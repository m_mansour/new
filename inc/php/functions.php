<?php

//send the welcome letter
function send_email($email){
		
	//set the root
	$root = $_SERVER['DOCUMENT_ROOT'];
	
	//format each email
	$body = file_get_contents($root.'/signup_template.html');
	$body_plain_txt = file_get_contents($root.'/signup_template.txt');

	$mail = new PHPMailer();
	$mail->IsSMTP(); // telling the class to use SMTP
	$mail->SMTPDebug = false;
	$mail->do_debug = 0;
	$mail->SMTPAuth   = true;                  // enable SMTP authentication
	$mail->Host       = "ssl://smtp.gmail.com";      // sets GMAIL as the SMTP server
	$mail->Port       = 465;                   // set the SMTP port for the GMAIL server
	$mail->Username   = "info@coterique.com";  // GMAIL username
	$mail->Password   = "cotcotcot";            // GMAIL password
	$mail->SetFrom('info@coterique.com', 'Coterique');
	$mail->Subject    = "Item sold";
	$mail->AltBody    = $body_plain_txt; // optional, comment out and test
	$mail->MsgHTML($body);
	$mail->AddAddress($email, $email);
	
	$result = $mail->Send();
	
	return $result;
	
}

//cleanup the errors
function show_errors($action){

	$error = false;

	if(!empty($action['result'])){
	
		$error = "<ul class=\"alert $action[result]\">"."\n";

		if(is_array($action['text'])){
	
			//loop out each error
			foreach($action['text'] as $text){
			
				$error .= "<li><p>$text</p></li>"."\n";
			
			}	
		
		}else{
		
			//single error
			$error .= "<li><p>$action[text]</p></li>";
		
		}
		
		$error .= "</ul>"."\n";
		
	}

	return $error;

}