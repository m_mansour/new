<?php
// Heading
$_['heading_title']    = 'تقرير القسائم';

// Column
$_['column_name']      = 'اسم القسيمة';
$_['column_code']      = 'الرمز';
$_['column_orders']    = 'الطلبات';
$_['column_total']     = 'الغجمالي';
$_['column_action']    = 'الغجراء';

// Entry
$_['entry_date_start'] = 'تاريخ البدء';
$_['entry_date_end']   = 'تاريخ الانتهاء';
?>