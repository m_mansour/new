<?php
// Heading
$_['heading_title']     = 'تقرير عمولة نظام الترويج';

// Column
$_['column_affiliate']  = 'اسم المروج';
$_['column_email']      = 'البريد الإلكتروني';
$_['column_status']     = 'الحالة';
$_['column_commission'] = 'العمولة';
$_['column_orders']     = 'رقم الطلب';
$_['column_total']      = 'المجموع';
$_['column_action']     = 'الغجراء';

// Entry
$_['entry_date_start']  = 'تاريخ البدء:';
$_['entry_date_end']    = 'تاريخ الانتهاء:';
?>