<?php
// Heading
$_['heading_title']     = 'تقرير الشحن';

// Text
$_['text_year']         = 'السنوات';
$_['text_month']        = 'الشهور';
$_['text_week']         = 'الأسابيع';
$_['text_day']          = 'الايام';
$_['text_all_status']   = 'كل الحالات';

// Column
$_['column_date_start'] = 'تاريخ البدء';
$_['column_date_end']   = 'تاريخ الانتهاء';
$_['column_title']      = 'مسمى الشحن';
$_['column_orders']     = 'رقم الطلبات';
$_['column_total']      = 'الإجمالي';

// Entry
$_['entry_date_start']  = 'تاريخ البدء';
$_['entry_date_end']    = 'تاريخ الانتهاء';
$_['entry_group']       = 'مجموعة بي:';
$_['entry_status']      = 'حالة الطلب:';
?>