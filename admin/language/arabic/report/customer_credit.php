<?php
// Heading
$_['heading_title']         = 'تقرير رصيد العميل';

// Column
$_['column_customer']       = 'اسم العميل';
$_['column_email']          = 'البريد الإلكتروني';
$_['column_customer_group'] = 'مجموعة العملاء';
$_['column_status']         = 'الحالة';
$_['column_total']          = 'المجموع';
$_['column_action']         = 'الغجراء';

// Entry
$_['entry_date_start']     = 'تاريخ البدء';
$_['entry_date_end']       = 'تاريخ الانتهاء';
?>