<?php
// Heading
$_['heading_title']  = 'تقرير مشاهدة المنتجات';

// Text
$_['text_success']   = 'تم اعادة تعيين مشاهدة المنتجات بنجاح!';

// Column
$_['column_name']    = 'اسم المنتج';
$_['column_model']   = 'الموديل';
$_['column_viewed']  = 'شوهدت';
$_['column_percent'] = 'النسبة المئوية';
?>