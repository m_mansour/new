<?php
// Heading
$_['heading_title']     = 'تقرير شراء المنتجات';

// Text
$_['text_all_status']   = 'كل الحالات';

// Column
$_['column_date_start'] = 'تاريخ البدء';
$_['column_date_end']   = 'تاريخ الانتهاء';
$_['column_name']       = 'اسم المنتج';
$_['column_model']      = 'الموديل';
$_['column_quantity']   = 'العدد';
$_['column_total']      = 'المجموع';

// Entry
$_['entry_date_start']  = 'تاريخ البدء';
$_['entry_date_end']    = 'تاريخ الانتهاء';
$_['entry_status']      = 'حالة الطلب:';
?>