<?php
// Heading
$_['heading_title']    = 'النسخة الاحتياطية';

// Text
$_['text_backup']      = 'تحميل النسخة الاحتياطية';
$_['text_success']     = 'لقد نجحت في استيراد قاعدة البيانات بنجاح!';

// Entry
$_['entry_restore']    = 'استعادة النسخة الاحتياطية:';
$_['entry_backup']     = 'الجداول:';

// Error
$_['error_permission'] = 'لا يوجد لديك صلاحيات تعديل النسخة الاحتياطية!';
$_['error_empty']      = 'الملف الذي تم رفعة فارغ!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>