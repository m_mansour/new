<?php
// Heading
$_['heading_title']                = 'خدمة الطرود المتحدة';

// Text
$_['text_shipping']                = 'الشحن';
$_['text_success']                 = 'تم تعديل خدمة الطرود المتحدة لشحن الأمريكي بنجاح!';
$_['text_regular_daily_pickup']    = 'الشحن اليومي المعتاد';
$_['text_daily_pickup']            = 'الشحن اليومي';
$_['text_customer_counter']        = 'عداد العملاء';
$_['text_one_time_pickup']         = 'الشحن لمرة واحدة';
$_['text_on_call_air_pickup']      = 'شحن اتصال جوي واحد';
$_['text_letter_center']           = 'مركز الرسائل';
$_['text_air_service_center']      = 'مركز خدمة الطياران';
$_['text_suggested_retail_rates']  = 'اقترح أسعار التجزئة (يو بي إس المخزون)';
$_['text_package']                 = 'الطرد';
$_['text_ups_letter']              = 'UPS رسالة';
$_['text_ups_tube']                = 'UPS أنبوب';
$_['text_ups_pak']                 = 'UPS طرد';
$_['text_ups_express_box']         = 'UPS صندوق اكسبريس';
$_['text_ups_25kg_box']            = 'UPS صندوق 25 كيلو غرام';
$_['text_ups_10kg_box']            = 'UPS صندوق 10 كيلو غرام';
$_['text_us']                      = 'أمريكي الأصل';
$_['text_ca']                      = 'كندي الأصل';
$_['text_eu']                      = 'اتحاد الأوروبي الأصل';
$_['text_pr']                      = 'بورتوريكو  الأصل';
$_['text_mx']                      = 'مكسيكي الأصل';
$_['text_other']                   = 'جميع الأصول الأخرى';
$_['text_test']                    = 'تجربة';
$_['text_production']              = 'الإنتاج';	
$_['text_residential']             = 'سكني';
$_['text_commercial']              = 'تجاري';
$_['text_next_day_air']            = 'الطياران في اليوم التالي';
$_['text_2nd_day_air']             = 'لطياران في اليوم الثاني';
$_['text_ground']                  = 'البريد البري';
$_['text_3_day_select']            = 'اختيار 3 أيام';
$_['text_next_day_air_saver']      = 'الطياران في اليوم التالي المدخر';
$_['text_next_day_air_early_am']   = 'الطياران في اليوم التالي صباحا';
$_['text_2nd_day_air_am']          = 'الطياران في اليوم الثاني صباحا';
$_['text_saver']                   = 'البريد المدخر';
$_['text_worldwide_express']       = 'الشحن السريع حول العالم';
$_['text_worldwide_expedited']     = 'الشحن المستعجل حول العالم';
$_['text_standard']                = 'البريد العادي';
$_['text_worldwide_express_plus']  = 'الشحن السريع المميز حول العالم';
$_['text_express']                 = 'البريد السريع';
$_['text_expedited']               = 'البريد المستعجل';
$_['text_express_early_am']        = 'الشحن السريع في الصباح الباكر.';
$_['text_express_plus']            = 'الشحن السريع';
$_['text_today_standard']          = 'الشحن العادي اليوم';
$_['text_today_dedicated_courier'] = 'الشحن لساعي البريد اليوم';
$_['text_today_intercity']         = 'الشحن بين المدن اليوم';
$_['text_today_express']           = 'الشحن السريع اليوم';
$_['text_today_express_saver']     = 'الشحن السريع المدخر اليوم';

// Entry
$_['entry_key']                    = 'مفتاح الدخول:<span class="help">Enter the XML rates access key assigned to you by UPS.</span>';
$_['entry_username']               = 'اسم المستخدم:<span class="help">Enter your UPS Services account username.</span>';
$_['entry_password']               = 'كلمة المرور:<span class="help">Enter your UPS Services account password.</span>';
$_['entry_pickup']                 = 'طريقة الشحن:<span class="help">How do you give packages to UPS (only used when origin is US)?</span>';
$_['entry_packaging']              = 'نوع الشحنة:<span class="help">What kind of packaging do you use?</span>';
$_['entry_classification']         = 'رمز العميل السري:<span class="help">01 - If you are billing to a UPS account and have a daily UPS pickup, 03 - If you do not have a UPS account or you are billing to a UPS account but do not have a daily pickup, 04 - If you are shipping from a retail outlet (only used when origin is US)</span>';
$_['entry_origin']                 = 'رمز الشحن الأصلي:<span class="help">What origin point should be used (this setting affects only what UPS product names are shown to the user)</span>';
$_['entry_city']                   = 'المدينة الأصلية:<span class="help">Enter the name of the origin city.</span>';
$_['entry_state']                  = 'المنطقة أو المحافظة الأصلية:<span class="help">Enter the two-letter code for your origin state/province.</span>';
$_['entry_country']                = 'الدولة الأصلية:<span class="help">Enter the two-letter code for your origin country.</span>';
$_['entry_postcode']               = 'الرمز البريدي الأصلي:<span class="help">Enter your origin zip/postalcode.</span>';
$_['entry_test']                   = 'اختبار أو وضع الإنتاج:<span class="help">Use this module in Test or Production mode?</span>';
$_['entry_quote_type']             = 'نوع السعر:<span class="help">Quote for Residential or Commercial Delivery.</span>';
$_['entry_service']                = 'الخدمات:<span class="help">Select the UPS services to be offered.</span>';
$_['entry_insurance']              = 'تفعيل التأمين:<span class="help">Enables insurance with product total as the value</span>';
$_['entry_display_weight']         = 'عرض وزن الشحنة:<br /><span class="help">Do you want to display the shipping weight? (e.g. Delivery Weight : 2.7674 Kg\'s)</span>';
$_['entry_measurement_code']       = 'رمز القياس:<br /><span class="help">Make sure you match the measurement class with the measurement code.</span>';
$_['entry_measurement_class']      = 'درجة القياس:<span class="help">Set to centimeters or inches.</span>';
$_['entry_dimension']			         = 'الأبعاد (L x W x H):';
$_['entry_weight_code']            = 'رمز الوزن:<br /><span class="help">Allowed kgs or lbs. Make sure you match the weight class with the UPS accepted weight code.</span>';
$_['entry_weight_class']           = 'درجة الوزن:<span class="help">Set to kilograms or pounds.</span>';
$_['entry_tax']                    = 'نظام الضرائب:';
$_['entry_geo_zone']               = 'المنطقة الجغرافية:';
$_['entry_status']                 = 'الحالة:';
$_['entry_sort_order']             = 'ترتيب العرض:';

// Error
$_['error_permission']             = 'لا يوجد لديك صلاحيات التعديل على خدمة الطرود المتحدة لشحن الامريكي!';
$_['error_key']                    = 'مفتاح الدخول مطلوب!';
$_['error_username']               = 'اسم المستخدم مطلوب!';
$_['error_password']               = 'كلمة المرور مطلوبة!';
$_['error_city']                   = 'المدينة الأصلية مطلوبة!';
$_['error_state']                  = 'المنطقة أو المحافظة الأصلية مطلوبة!';
$_['error_country']                = 'الدولة الأصلية مطلوبة!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>