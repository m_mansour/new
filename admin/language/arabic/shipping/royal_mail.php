<?php
// Heading 
$_['heading_title']             = 'Royal Mail';

// Text
$_['text_shipping']             = 'الشحن';
$_['text_success']              = 'تم تعديل Royal Mail بنجاح!';
$_['text_1st_class_standard']   = 'الدرجة الأولى العادية للبريد';
$_['text_1st_class_recorded']   = 'الدرجة الأولى المسجلة للبريد';
$_['text_2nd_class_standard']   = 'الدرجة الثانية العادية للبريد';
$_['text_2nd_class_recorded']   = 'الدرجة الثانية المسجلة للبريد';
$_['text_standard_parcels']     = 'معيار الطرود';
$_['text_airmail']              = 'البريد الجوي';
$_['text_international_signed'] = 'التوقيع الدولي';
$_['text_airsure']              = 'التأكيد الجوي';
$_['text_surface']              = 'البريد البري';

// Entry
$_['entry_service']             = 'الخدمات:';
$_['entry_display_weight']      = 'عرض وزن الإرسال:<br /><span class="help"> هل تريد عرض وزن الشحن؟ (على سبيل المثال وزن الإرسال: 2.7674 كيلو غرام)</span>';
$_['entry_display_insurance']   = 'عرض التأمين:<br /><span class="help"> هل تريد عرض تأمين الشحن؟ (على سبيل المثال التامين حتى 500 جنيه إسترليني)</span>';
$_['entry_display_time']        = 'عرض وقت الإرسال:<br /><span class="help"> هل تريد عرض وقت الشحن؟ (على سبيل المثال يشحن في غضون 3 إلى 5 أيام)</span>';
$_['entry_weight_class']        = 'نظام الوزن:';
$_['entry_tax']                 = 'نظام الضرائب:';
$_['entry_geo_zone']            = 'المنطقة الجغرافية:';
$_['entry_status']              = 'الحالة:';
$_['entry_sort_order']          = 'ترتيب العرض:';

// Error
$_['error_permission']          = 'لا يوجد لديك صلاحيات التعديل على Royal Mail!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>