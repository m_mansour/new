<?php
// Heading
$_['heading_title']    = 'Weight Based شحن';

// Text
$_['text_shipping']    = 'الشحنات';
$_['text_success']     = 'تم تعديل شحن Weight Based بنجاح!';

// Entry
$_['entry_rate']       = 'التسعيرة:<br /><span class="help">مثال : 5:10.00,7:12.00 الوزن: تكلفة, الوزن: تكلفة, لأخ..</span>';
$_['entry_tax']        = 'نظام الضرائب:';
$_['entry_geo_zone']   = 'المنطقة الجغرافية:';
$_['entry_status']     = 'الحالة:';
$_['entry_sort_order'] = 'ترتيب العرض:';

// Error
$_['error_permission'] = 'لا يوجد لديك صلاحيات التعديل على شحن Weight Based!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>