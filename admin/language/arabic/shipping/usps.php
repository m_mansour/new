<?php
// Heading
$_['heading_title']         = 'دائرة بريد الولايات المتحدة';

// Text
$_['text_shipping']         = 'الشحن';
$_['text_success']          = 'تم تعديل دائرة بريد الولايات المتحدة بنجاح!';
$_['text_domestic_0']       = 'الدرجة الأولى';
$_['text_domestic_1']       = 'البريد الأولي';
$_['text_domestic_2']       = 'البريد السريع مستلم الطرد';
$_['text_domestic_3']       = 'البريد السريع إلى العنوان';
$_['text_domestic_4']       = 'الطرود البريدية';
$_['text_domestic_5']       = 'أمر طباعة ملزم';
$_['text_domestic_6']       = 'البريد الإعلاني';
$_['text_domestic_7']       = 'المكتبة';
$_['text_domestic_12']      = 'الدرجة الأولى البريد المختوم';
$_['text_domestic_13']      = 'البريد المميز سعر ثابت لطرد';
$_['text_domestic_16']      = 'البريد الأولي سعر ثابت لطرد';
$_['text_domestic_17']      = 'البريد الأولي العادي سعر ثابت لصندوق';
$_['text_domestic_18']      = 'البريد الأولي المفاتيح والمعرف';
$_['text_domestic_19']      = 'الدرجة الأولى';
$_['text_domestic_22']      = 'البريد الأولي سعر ثابت لصندوق الكبير';
$_['text_domestic_23']      = 'البريد السريع الأحد / أيام العطل';
$_['text_domestic_25']      = 'البريد السريع سعر ثابت للمغلف الأحد / أيام العطل';
$_['text_domestic_27']      = 'البريد السريع سعر ثابت للمغلف موقوف لتسليم';
$_['text_domestic_28']      = 'البريد الأولي سعر ثابت صغير لصندوق';
$_['text_international_1']  = 'البريد السريع الدولي';
$_['text_international_2']  = 'البريد الأولي الدولي';
$_['text_international_4']  = 'ضمان البريد السريع الدولي ( بوثيقة أو بدون وثيقة)';
$_['text_international_5']  = 'ضمان البريد السريع الدولي الوثيقة المستخدمة';
$_['text_international_6']  = 'ضمان البريد السريع الدولي وثيقة غير مستطيلة الشكل';
$_['text_international_7']  = 'ضمان البريد السريع الدولي ليسه وثيقة غير مستطيلة';
$_['text_international_8']  = 'البريد الأولي سعر ثابت للمغلف';
$_['text_international_9']  = 'البريد الأولي سعر ثابت للصندوق';
$_['text_international_10'] = 'البريد السريع الدولي سعر ثابت للمغلف';
$_['text_international_11'] = 'البريد الأولي سعر ثابت للصندوق الكبير';
$_['text_international_12'] = 'ضمان البريد السريع الدولي للمغلف';
$_['text_international_13'] = 'البريد الدولي الدرجة الأولى للرسائل';
$_['text_international_14'] = 'البريد الدولي الدرجة الأولى للمغلفات';
$_['text_international_15'] = 'البريد الدولي الدرجة الأولى للطرود';
$_['text_international_16'] = 'البريد الأولي سعر ثابت للصندوق الصغير';
$_['text_international_21'] = 'بطاقات بريدية';
$_['text_regular']          = 'عادي';
$_['text_large']            = 'ضخم';
$_['text_rectangular']      = 'مستطيلة';
$_['text_non_rectangular']  = 'غير مستطيلة';
$_['text_variable']         = 'المتغير';
$_['text_letter']           = 'خطاب';
$_['text_parcel']           = 'طرد';

// Entry
$_['entry_user_id']         = 'رقم المستخدم:';
$_['entry_postcode']        = 'الرمز البريدي:';
$_['entry_domestic']        = 'الخدمات المنزلية:';
$_['entry_international']   = 'الخدمات الدولية:';
$_['entry_size']            = 'الحجم:';
$_['entry_container']       = 'الحاوية:';
$_['entry_machinable']      = 'الآلية:';
$_['entry_firstclass']      = 'نوع سعر الدرجة الأولى المحلي:<br/><span class="help">الدرجة الأولى تنطبق على طرد 13oz أو اقل.عند التعطيل، يتم إرجاع إلى الأقساط الموحدة.</span>';
$_['entry_dimension']       = 'الأبعاد (L x W x H):';
$_['entry_girth']           = 'المقاس:';
$_['entry_display_time']    = 'عرض وقت الشحن:<br /><span class="help">هل تريد عرض وقت الشحن? (مثلا. الشحن من 3 إلى 5 أيام)</span>';
$_['entry_display_weight']  = 'عرض وزن الشحن:<br /><span class="help">هل تريد عرض وزن الشحن? (مثلا. وزن الشحن : 2.7674 كيلو غرام)</span>';
$_['entry_weight_class']    = 'نظام الوزن:<br /><span class="help">تعيين لجنيه.</span>';
$_['entry_tax']             = 'نظام الضرائب:';
$_['entry_geo_zone']        = 'المنطقة الجغرافية:';
$_['entry_status']          = 'الحالة:';
$_['entry_sort_order']      = 'ترتيب العرض:';
$_['entry_debug']					  = 'وضع التصحيح:<br/><span class="help">معلومات إضافية إلى سجل النظام.</span>';

// Error
$_['error_permission']      = 'لا يوجد لديك صلاحيات التعديل على دائرة بريد الولايات المتحدة!';
$_['error_user_id']         = 'رقم المستخدم مطلوب!';
$_['error_postcode']        = 'الرمز البريدي مطلوب!';
$_['error_width']          	= 'العرض مطلوب!';
$_['error_length']        	= 'الطول مطلوب!';
$_['error_height']        	= 'الارتفاع مطلوب!';
$_['error_girth']          	= 'المقاس مطلوب!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>