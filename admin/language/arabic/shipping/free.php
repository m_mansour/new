<?php
// Heading
$_['heading_title']    = 'الشحن المجاني';

// Text
$_['text_shipping']    = 'الشحن';
$_['text_success']     = 'تم تعديل الشحن المجاني بنجاح!';

// Entry
$_['entry_total']      = 'الإجمالي:<br /><span class="help">المبلغ الإجمالي مطلوب قبل أن يتم تفعيل الشحن المجاني للعميل.</span>';
$_['entry_geo_zone']   = 'المنطقة الجغرافية:';
$_['entry_status']     = 'الحالة:';
$_['entry_sort_order'] = 'ترتيب العرض:';

// Error
$_['error_permission'] = 'لا يوجد لديك صلاحيات التعديل على الشحن المجاني!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>