<?php 
// Heading
$_['heading_title']    = 'لكل منتج';

// Text
$_['text_shipping']    = 'الشحن';
$_['text_success']     = 'تم تعديل سعر الشحن لكل منتج بنجاح!';

// Entry
$_['entry_cost']       = 'التكلفة:';
$_['entry_tax']        = 'نظام الضرائب:';
$_['entry_geo_zone']   = 'المنطقة الجغرافية:';
$_['entry_status']     = 'الحالة:';
$_['entry_sort_order'] = 'ترتيب العرض:';

// Error
$_['error_permission'] = 'لا يوجد لديك صلاحيات التعديل على سعر الشحن لكل منتج!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>