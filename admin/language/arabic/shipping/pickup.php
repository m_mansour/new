<?php
// Heading
$_['heading_title']           = 'التسليم من المحل';

// Text 
$_['text_shipping']           = 'الشحن';
$_['text_success']            = 'تم تعديل التسليم من المحل بنجاح!';

// Entry
$_['entry_geo_zone']          = 'المنطقة الجغرافية:';
$_['entry_status']            = 'الحالة:';
$_['entry_sort_order']        = 'ترتيب العرض:';

// Error
$_['error_permission']        = 'لا يوجد لديك صلاحيات التعديل على التسليم من المحل!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>