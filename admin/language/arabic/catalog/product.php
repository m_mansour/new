<?php
// Heading
$_['heading_title']          = 'المنتجات'; 

// Text  
$_['text_success']           = 'تم تعديل المنتجات بنجاح';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'الافتراضي';
$_['text_image_manager']     = 'إدارة الصور';
$_['text_option']            = 'الخيارات';
$_['text_option_value']      = 'خيارات القيمة';
$_['text_percent']           = 'النسبة المئوية';
$_['text_amount']            = 'مبلغ ثابت';

// Column
$_['column_name']            = 'اسم المنتج';
$_['column_model']           = 'الموديل';
$_['column_image']           = 'الصورة';
$_['column_price']           = 'السعر';
$_['column_quantity']        = 'الكمية';
$_['column_status']          = 'الحالة';
$_['column_action']          = 'الإجراء';

// Entry
$_['entry_name']             = 'اسم المنتج:';
$_['entry_meta_keywords'] 	 = 'الكلمات الدلالية:';
$_['entry_meta_description'] = 'وصف الميتا تاق:';
$_['entry_description']      = 'الوصف:';
$_['entry_store']            = 'المتاجر:';
$_['entry_keyword']          = 'SEO كلمات:';
$_['entry_model']            = 'الموديل:';
$_['entry_sku']              = 'الوحدة المتوفرة:';
$_['entry_upc']              = 'يو بي سي:';
$_['entry_location']         = 'الموقع:';
$_['entry_manufacturer']     = 'الشركة المصنعة:';
$_['entry_shipping']         = 'يتطلب الشحن:'; 
$_['entry_date_available']   = 'تاريخ التوفر:';
$_['entry_quantity']         = 'الكمية:';
$_['entry_minimum']          = 'الحد الأدنى من الكمية:<br/><span class="help">أجبار كمية معينة للطلب</span>';
$_['entry_stock_status']     = 'حالة توفر المنتج:<br/><span class="help">تظهر عند انتهاء كمية المنتج من المخزون</span>';
$_['entry_tax_class']        = 'فئة الضريبة:';
$_['entry_price']            = 'السعر:';
$_['entry_points']           = 'النقاط:<br/><span class="help">عدد النقاط اللازمة لشراء هذا المنتج. إذا كنت تريد عدم شراء هذا المنتج بنقاط ضع صفر في الحقل.</span>';
$_['entry_option_points']    = 'النقاط:';
$_['entry_subtract']         = 'عرض الكمية المتوفرة:';
$_['entry_weight_class']     = 'فئة الوزن:';
$_['entry_weight']           = 'الوزن:';
$_['entry_length']           = 'فئة الطول:';
$_['entry_dimension']        = 'الأبعاد (L x W x H):';
$_['entry_image']            = 'الصورة:<br /><span class="help">اضغط على الصورة لتغييرها.</span>';
$_['entry_customer_group']   = 'مجموعة العملاء:';
$_['entry_date_start']       = 'تاريخ البدء:';
$_['entry_date_end']         = 'تاريخ الانتهاء:';
$_['entry_priority']         = 'الأولوية:';
$_['entry_attribute']        = 'السمات:';
$_['entry_attribute_group']  = 'مجموعة السمات:';
$_['entry_text']             = 'الضريبة:';
$_['entry_option']           = 'الخيارات:';
$_['entry_option_value']     = 'خيارات القيمة:';
$_['entry_required']         = 'مطلوب:';
$_['entry_status']           = 'الحالة:';
$_['entry_sort_order']       = 'ترتيب العرض:';
$_['entry_category']         = 'الأقسام:';
$_['entry_download']         = 'التحميل:';
$_['entry_related']          = 'منتجات ذات صلة:';
$_['entry_tags']          	 = 'وسم المنتج:<br /><span class="help">فصل الجمل بفاصلة</span>';
$_['entry_reward']           = 'نقاط المكافآت:';
$_['entry_layout']           = 'التصميم الجانبي:';

// Error
$_['error_warning']          = 'لم يتم إدخال البيانات المطلوبة. يرجى التحقق من الحقول ربما توجد أخطاء!';
$_['error_permission']       = 'لا يوجد لديك صلاحيات تعديل المنتج!';
$_['error_name']             = 'اسم المنتج يجب أن يكون أكبر من 3 وأقل من 255 حرفا!';
$_['error_model']            = 'موديل المنتج يجب أن يكون أكبر من 3 وأقل من 24 حرفا!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>