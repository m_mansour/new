<?php
// Heading
$_['heading_title']       = 'خيارات الحقول';

// Text
$_['text_success']        = 'تم تعديل الخيارات بنجاح!';
$_['text_choose']         = 'خيار';
$_['text_select']         = 'تحديد';
$_['text_radio']          = 'زر اختيار دائري';
$_['text_checkbox']       = 'زر اختيار مربع';
$_['text_input']          = 'حقل';
$_['text_text']           = 'صندوق نص بسطر وحيد';
$_['text_textarea']       = 'صندوق نص متعدد الأسطر';
$_['text_file']           = 'ملف';
$_['text_date']           = 'تاريخ';
$_['text_datetime']       = 'التاريخ والوقت';
$_['text_time']           = 'الوقت';

// Column
$_['column_name']         = 'اسم الخيار';
$_['column_sort_order']   = 'ترتيب العرض';
$_['column_action']       = 'الإجراء';

// Entry
$_['entry_name']         = 'اسم الخيار:';
$_['entry_type']         = 'نوع الحقل:';
$_['entry_value']        = 'اسم قيمة الخيار:';
$_['entry_sort_order']   = 'ترتيب العرض:';

// Error
$_['error_permission']   = 'لا يوجد لديك صلاحيات تعديل خيارات الحقول!';
$_['error_name']         = 'اسم الخيار يجب أن يكون ما بين 3 حتى 128 حرفا!';
$_['error_type']         = 'قيمة الخيار مطلوبة!';
$_['error_option_value'] = 'اسم قيمة الخيار يجب أن يكون ما بين 3 حتى 128 حرفا!';
$_['error_product']      = 'لا يمكن حذف الخيار لان قد تم تعيينه على المنتجات التالية %s!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>