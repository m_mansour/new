<?php
// Heading
$_['heading_title']      = 'الماركات';

// Text
$_['text_success']       = 'تم تعديل الماركات بنجاح';
$_['text_default']       = 'الافتراضي';
$_['text_image_manager'] = 'إدارة الصور';
$_['text_percent']       = 'النسبة المئوية';
$_['text_amount']        = 'مبلغ ثابت';

// Column
$_['column_name']        = 'اسم الماركة';
$_['column_sort_order']  = 'ترتيب العرض';
$_['column_action']      = 'الإجراء';

// Entry
$_['entry_name']         = 'اسم الماركة:';
$_['entry_store']        = 'المتاجر:';
$_['entry_keyword']      = 'SEO كلمات:';
$_['entry_image']        = 'الصورة:';
$_['entry_sort_order']   = 'ترتيب العرض:';
$_['entry_type']         = 'النوع:';

// Error
$_['error_permission']   = 'لا يوجد لديك صلاحيات تعديل الماركات!';
$_['error_name']         = 'اسم الماركة يجب أن يكون أكبر من 3 وأقل من 64 حرفا!';
$_['error_product']      = 'لا يمكن حذف الماركة يوجد منتجات مرتبطة بها!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>