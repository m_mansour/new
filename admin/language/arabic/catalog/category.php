<?php
// Heading
$_['heading_title']          = 'الأقسام';

// Text
$_['text_success']           = 'تم تعديل القسم بنجاح';
$_['text_default']           = 'الافتراضي';
$_['text_image_manager']     = 'إدارة الصور';

// Column
$_['column_name']            = 'اسم القسم';
$_['column_sort_order']      = 'عدد الطلبات';
$_['column_action']          = 'الإجراء';

// Entry
$_['entry_name']             = 'اسم القسم:';
$_['entry_meta_keywords'] 	 = 'الكلمات الدلالية:';
$_['entry_meta_description'] = 'وصف الميتا تاق:';
$_['entry_description']      = 'وصف القسم:';
$_['entry_parent']           = 'الأقسام الرئيسية:';
$_['entry_store']            = 'المتاجر:';
$_['entry_keyword']          = 'SEO كلمات::<br/><span class="help">يجب أن يكون فريد عالميا.</span>';
$_['entry_image']                = 'الصورة:';
$_['entry_top']              = 'للأعلى:<br/><span class="help">العرض في القائمة العلوية. فقط يعمل مع الأقسام الرئيسية.</span>';
$_['entry_column']           = 'الأعمدة:<br/><span class="help">عدد الأعمدة يتم استخدام 3 أعمدة.فقط يعمل مع أقسام القائمة العلوية.</span>';
$_['entry_sort_order']       = 'ترتيب العرض:';
$_['entry_status']           = 'حالة القسم:';
$_['entry_layout']           = 'التصميم الجانبي:';

// Error 
$_['error_warning']          = 'لم يتم إدخال البيانات المطلوبة. يرجى التحقق من الحقول ربما توجد أخطاء!';
$_['error_permission']       = ' لا يوجد لديك صلاحيات تعديل الأقسام!';
$_['error_name']             = 'اسم القسم يجب أن يكون أكبر من 2 وأقل من 32 حرفا!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>