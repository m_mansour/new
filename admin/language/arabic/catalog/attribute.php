<?php
// Heading
$_['heading_title']          = 'السمات';

// Text
$_['text_success']           = 'تم تعديل السمات بنجاح!';

// Column
$_['column_name']            = 'اسم السمة';
$_['column_attribute_group'] = 'مجموعة السمة';
$_['column_sort_order']      = 'ترتيب العرض';
$_['column_action']          = 'الإجراء';

// Entry
$_['entry_name']            = 'اسم السمة:';
$_['entry_attribute_group'] = 'مجموعة السمة:';
$_['entry_sort_order']      = 'ترتيب العرض:';

// Error
$_['error_permission']      = ' لا يوجد لديك تصريح تعديل السمات!';
$_['error_name']            = 'اسم السمة يجب أن يكون ما بين 2 حتى 32 حرفا!';
$_['error_product']         = 'لا يمكن حذف السمة لان قد تم تعيين على المنتجات التالية %s!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>