<?php
// Heading
$_['heading_title']     = 'التعليقات';

// Text
$_['text_success']      = 'تم تعديل التعليق بنجاح!';

// Column
$_['column_product']    = 'المنتج';
$_['column_author']     = 'الكاتب';
$_['column_rating']     = 'التقييم';
$_['column_status']     = 'الحالة';
$_['column_date_added'] = 'تاريخ الإضافة';
$_['column_action']     = 'الإجراء';

// Entry
$_['entry_product']     = 'المنتج:';
$_['entry_author']      = 'الكاتب:';
$_['entry_rating']      = 'التقييم:';
$_['entry_status']      = 'الحالة:';
$_['entry_text']        = 'النص:';
$_['entry_good']        = 'جيد';
$_['entry_bad']         = 'سيء';

// Error
$_['error_permission']  = 'لا يوجد لديك صلاحيات تعديل التعليقات!';
$_['error_product']     = 'المنتج مطلوب!';
$_['error_author']      = 'اسم الكاتب يجب أن يكون أكبر من 3 وأقل من 64 حرفا!';
$_['error_text']        = 'نص التعليق يجب أن يكون أكبر من 25 وأقل من 1000 حرفا!';
$_['error_rating']      = 'التقييم مطلوب!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>