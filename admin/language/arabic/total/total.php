<?php
// Heading
$_['heading_title']    = 'المجموع';

// Text
$_['text_total']       = 'نظام الطلبات';
$_['text_success']     = 'تم تعديل إجمالي المجموع بنجاح!';

// Entry
$_['entry_status']     = 'الحالة:';
$_['entry_sort_order'] = 'ترتيب العرض:';

// Error
$_['error_permission'] = 'لا يوجد لديك صلاحيات التعديل على إجمالي المجموع!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>