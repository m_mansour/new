<?php
// Heading
$_['heading_title']    = 'رسوم التوصيل';

// Text
$_['text_total']       = 'نظام الطلبات';
$_['text_success']     = 'تم تعديل رسوم التوصيل بنجاح!';

// Entry
$_['entry_total']      = 'إجمالي الطلبات:';
$_['entry_fee']        = 'الرسوم:';
$_['entry_tax']        = 'نظام الضرائب:';
$_['entry_status']     = 'الحالة:';
$_['entry_sort_order'] = 'ترتيب العرض:';

// Error
$_['error_permission'] = 'لا يوجد لديك صلاحيات التعديل على رسوم التوصيل!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>