<?php
// Heading
$_['heading_title']    = 'أدنى رسوم الطلبات';

// Text
$_['text_total']       = 'نظام الطلبات';
$_['text_success']     = 'تم تعديل أدنى رسوم الطلبات بنجاح!';

// Entry
$_['entry_total']      = 'إجمالي الطلب:';
$_['entry_fee']        = 'الرسوم:';
$_['entry_tax']        = 'نظام الضرائب:';
$_['entry_status']     = 'الحالة:';
$_['entry_sort_order'] = 'ترتيب العرض:';

// Error
$_['error_permission'] = 'لا يوجد لديك صلاحيات التعديل على أدنى رسوم الطلبات!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>