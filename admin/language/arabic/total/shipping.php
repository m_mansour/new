<?php
// Heading
$_['heading_title']    = 'الشحن';

// Text
$_['text_total']       = 'نظام الطلبات';
$_['text_success']     = 'تتم تعديل إجمالي الشحن بنجاح!';

// Entry
$_['entry_estimator']  = 'مقدار الشحن:';
$_['entry_status']     = 'الحالة:';
$_['entry_sort_order'] = 'ترتيب العرض:';

// Error
$_['error_permission'] = 'لا يوجد لديك صلاحيات التعديل على إجمالي الشحن!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>