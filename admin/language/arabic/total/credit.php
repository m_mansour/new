<?php
// Heading
$_['heading_title']    = 'رصيد المتجر';

// Text
$_['text_total']       = 'نظام الطلبات';
$_['text_success']     = 'تم تعديل إجمالي رصيد المتجر بنجاح!';

// Entry
$_['entry_status']     = 'الحالة:';
$_['entry_sort_order'] = 'ترتيب العرض:';

// Error
$_['error_permission'] = 'لا توجد لديك صلاحيات التعديل على إجمالي رصيد المتجر!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>