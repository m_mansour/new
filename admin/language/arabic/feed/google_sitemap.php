<?php
// Heading
$_['heading_title']    = 'Google Sitemap';

// Text 
$_['text_feed']        = 'تغذية المنتجات';
$_['text_success']     = 'تم تعديل تغذية Google Sitemap بنجاح!';

// Entry
$_['entry_status']     = 'الحالة:';
$_['entry_data_feed']  = 'رابط تغذية البيانات:';

// Error
$_['error_permission'] = 'لا يوجد لديك صلاحيات التعديل على تغذية Google Sitemap!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>