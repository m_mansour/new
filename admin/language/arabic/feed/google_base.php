<?php
// Heading
$_['heading_title']    = 'Google Base موقع';

// Text   
$_['text_feed']        = 'تغذية المنتجات';
$_['text_success']     = 'تم تعديل تغذية Google Base بنجاح!';

// Entry
$_['entry_status']     = 'الحالة:';
$_['entry_data_feed']  = 'رابط حقل البيانات:';

// Error
$_['error_permission'] = 'لا يوجد لديك صلاحيات التعديل على تغذية Google Base!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>