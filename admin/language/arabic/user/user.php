<?php
// Heading
$_['heading_title']     = 'الأعضاء';

// Text
$_['text_success']      = 'تم تعديل بيانات العضو بنجاح!';

// Column
$_['column_username']   = 'اسم المستخدم';
$_['column_status']     = 'الحالة';
$_['column_date_added'] = 'تاريخ الإضافة';
$_['column_action']     = 'الإجراء';

// Entry
$_['entry_username']    = 'اسم المستخدم:';
$_['entry_password']    = 'كلمة المرور:';
$_['entry_confirm']     = 'تأكيد كلمة المرور:';
$_['entry_firstname']   = 'الاسم:';
$_['entry_lastname']    = 'اسم العائلة:';
$_['entry_email']       = 'البريد الإلكتروني:';
$_['entry_user_group']  = 'مجموعة الأعضاء:';
$_['entry_status']      = 'الحالة:';
$_['entry_captcha']     = 'أدخل الرمز في الحقل أدناه:';

// Error
$_['error_permission']  = 'لا يوجد لديك صلاحيات التعديل على الأعضاء!';
$_['error_account']     = 'لا يمكنك حذف الحساب الخاص بك!';
$_['error_username']    = 'اسم المستخدم يجب أن يكون أكبر من 3 وأقل من 20 حرفا!';
$_['error_password']    = 'كلمة المرور يجب أن تكون أكبر من 3 وأقل من 20 حرفا!';
$_['error_confirm']     = 'كلمة المرور وتأكيد كلمة المرور لا تتطابقان!';
$_['error_firstname']   = 'الاسم يجب أن يكون أكبر من 3 وأقل من 32 حرفا!';
$_['error_lastname']    = 'اسم العائلة يجب أن يكون أكبر من 3 وأقل من 32 حرفا!';
$_['error_captcha']     = 'الرمز المدخل غير صحيح يرجى إعادة إدخال الرمز من جديد!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>