<?php
// Heading
$_['heading_title']    = 'مجموعة الأعضاء';

// Text
$_['text_success']     = 'تم تعديل مجموعة الأعضاء بنجاح!';

// Column
$_['column_name']      = 'اسم مجموعة الأعضاء';
$_['column_action']    = 'الإجراء';

// Entry
$_['entry_name']       = 'اسم مجموعة الأعضاء:';
$_['entry_access']     = 'صلاحيات الدخول:';
$_['entry_modify']     = 'صلاحيات التعديل:';

// Error
$_['error_permission'] = 'لا يوجد لديك صلاحيات التعديل على مجموعة الأعضاء!';
$_['error_name']       = 'اسم مجموعة الأعضاء يجب أن يكون أكبر من 3 وأقل من 64 حرفا!';
$_['error_user']       = 'مجموعة الأعضاء هذه لا يمكن حذفها لأنه قد تم تعيينها إلى  %s أعضاء!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>