<?php
// Locale
$_['code']                    = 'ar';
$_['direction']               = 'rtl';
$_['date_format_short']       = 'd/m/Y';
$_['date_format_long']        = 'l dS F Y';
$_['time_format']             = 'h:i:s A';
$_['decimal_point']           = '.';
$_['thousand_point']          = ',';

// Text
$_['text_yes']                = 'نعم';
$_['text_no']                 = 'لا';
$_['text_enabled']            = 'تفعيل';
$_['text_disabled']           = 'تعطيل';
$_['text_none']               = ' --- لا يوجد --- ';
$_['text_select']             = ' --- يرجى الاختيار --- ';
$_['text_select_all']         = 'تحديد الكل';
$_['text_unselect_all']       = 'إلغاء تحديد الكل';
$_['text_all_zones']          = 'جميع المناطق';
$_['text_default']            = ' <b>(الافتراضي)</b>';
$_['text_close']              = 'إغلاق';
$_['text_pagination']         = 'عرض {start} إلى {end} من {total} ({pages} صفحات)';
$_['text_no_results']         = 'لا يوجد نتائج!';
$_['text_separator']          = ' &gt; ';
$_['text_edit']               = 'تحرير';
$_['text_view']               = 'عرض';
$_['text_home']               = 'الرئيسية';

// Button
$_['button_insert']           = 'إضافة';
$_['button_delete']           = 'حذف';
$_['button_save']             = 'حفظ';
$_['button_cancel']           = 'إلغاء';
$_['button_clear']            = 'حذف السجلات';
$_['button_close']            = 'إغلاق';
$_['button_filter']           = 'تنفيذ';
$_['button_send']             = 'إرسال';
$_['button_edit']             = 'تحرير';
$_['button_copy']             = 'نسخ';
$_['button_back']             = 'للخلف';
$_['button_remove']           = 'إزالة';
$_['button_backup']           = 'تصدير';
$_['button_restore']          = 'استعادة';
$_['button_invoice']          = 'طباعة الفاتورة';
$_['button_add_address']      = 'إضافة عنوان';
$_['button_add_attribute']    = 'إضافة سما';
$_['button_add_banner']       = 'إضافة بنر';
$_['button_add_product']      = 'إضافة منتج';
$_['button_add_option']       = 'إضافة خيار';
$_['button_add_option_value'] = 'إضافة قيمة جديدة';
$_['button_add_discount']     = 'إضافة خصم';
$_['button_add_special']      = 'إضافة عرض مميز';
$_['button_add_image']        = 'إضافة صورة';
$_['button_add_geo_zone']     = 'إضافة منطقة جغرافية';
$_['button_add_rate']         = 'إضافة معدل الضرائب';
$_['button_add_history']      = 'إضافة سجل الطلب';
$_['button_add_transaction']  = 'إضافة تحويل';
$_['button_add_total']        = 'إضافة الإجمالي';
$_['button_add_reward']       = 'إضافة نقاط مكافآت';
$_['button_add_route']        = 'إضافة مسار';
$_['button_add_module']       = 'إضافة موديل';
$_['button_add_link']         = 'إضافة رابط';
$_['button_approve']          = 'الموافقة';
$_['button_reset']            = 'إعادة التعين';

// Tab
$_['tab_address']             = 'العنوان';
$_['tab_admin']               = 'الإدارة';
$_['tab_attribute']           = 'السمات';
$_['tab_coupon_history']      = 'سجل قسائم التخفيض';
$_['tab_data']                = 'البيانات';
$_['tab_design']              = 'التصميم';
$_['tab_discount']            = 'تخفيض';
$_['tab_general']             = 'عام';
$_['tab_ip']                  = ' الاي بي';
$_['tab_links']               = 'الروابط';
$_['tab_image']               = 'خيارات الشعار';
$_['tab_option']              = 'خيارات عامة';
$_['tab_server']              = 'خيارات الخادم';
$_['tab_store']               = 'خيارات المتجر';
$_['tab_special']             = 'المميزة';
$_['tab_local']               = 'خيارات محلية';
$_['tab_mail']                = 'خيارات البريد';
$_['tab_module']              = 'الموديل';
$_['tab_order']               = 'تفاصيل الطلب';
$_['tab_order_history']       = 'سجل الطلب';
$_['tab_payment']             = 'عنوان الدفع';
$_['tab_product']             = 'المنتجات';
$_['tab_return']              = 'تفاصيل الإرجاع';
$_['tab_return_history']      = 'سجل الإرجاع';
$_['tab_reward']              = 'نقاط المكافآت';
$_['tab_shipping']            = 'عنوان الشحن';
$_['tab_total']               = 'الإجمالي';
$_['tab_transaction']         = 'التحويلات';
$_['tab_voucher_history']     = 'سجل قسائم الإهداءات';

// Error
$_['error_upload_1']          = 'الملف الذي تم رفعة يتجاوز التوجيه upload_max_filesize في ملف php.ini!';
$_['error_upload_2']          = 'الملف الذي تم رفعة يتجاوز التوجيه MAX_FILE_SIZE الذي تم تحديده في نموذج HTML!';
$_['error_upload_3']          = 'الملف الذي تم رفعه، رفع جزء فقط!';
$_['error_upload_4']          = 'لم يتم تحميل أي ملف!';
$_['error_upload_6']          = 'المجلد المؤقت مفقود!';
$_['error_upload_7']          = 'فشل إنشاء الملف في القرص!';
$_['error_upload_8']          = 'رفع الملف توقف بسبب المدة!';
$_['error_upload_999']        = 'لا يتوفر رمز للخطاء!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>