<?php
// Heading
$_['heading_title']  = 'لم يتم العثور على الصفحة!';

// Text
$_['text_not_found'] = 'الصفحة التي تبحث عنها لم يتم العثور عليها! يرجى الاتصال بمسؤول المتجر إذا كانت المشكلة مستمرة.';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>