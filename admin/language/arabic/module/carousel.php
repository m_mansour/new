<?php
// Heading
$_['heading_title']       = 'شريط التمرير';

// Text
$_['text_module']         = 'الإضافات';
$_['text_success']        = 'تم تعديل شريط التمرير بنجاح!';
$_['text_content_top']    = 'أعلى';
$_['text_content_bottom'] = 'أسفل';
$_['text_column_left']    = 'يمين';
$_['text_column_right']   = 'يسار';

// Entry
$_['entry_banner']        = 'البنر:';
$_['entry_limit']         = 'الحد:';
$_['entry_scroll']        = 'التمرير:';
$_['entry_image']         = 'الصورة (W x H):';
$_['entry_layout']        = 'التصميم:';
$_['entry_position']      = 'الوضع:';
$_['entry_status']        = 'الحالة:';
$_['entry_sort_order']    = 'ترتيب العرض:';

// Error
$_['error_permission']    = 'لا توجد لديك صلاحيات التعديل على إضافة شريط التمرير!';
$_['error_image']         = 'ارتفاع وطول الصورة مطلوب!';
?>