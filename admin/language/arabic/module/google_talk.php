<?php
// Heading
$_['heading_title']       = 'قوقل تولك';

// Text
$_['text_module']         = 'الإضافات';
$_['text_success']        = 'تم تعديل إضافة قوقل تولك بنجاح!';
$_['text_content_top']    = 'أعلى';
$_['text_content_bottom'] = 'أسفل';
$_['text_column_left']    = 'يمين';
$_['text_column_right']   = 'يسار';

// Entry
$_['entry_code']          = 'الرمز:<br /><span class="help">اذهب إلى <a onclick="window.open(\'http://www.google.com/talk/service/badge/New\');"><u>وإنشاء صندوق محادثة قوقل تولك</u></a> انسخ والصق الرمز الذي حصلت عليه في الحقل.</span>';
$_['entry_layout']        = 'التصميم:';
$_['entry_position']      = 'الوضع:';
$_['entry_status']        = 'الحالة:';
$_['entry_sort_order']    = 'ترتيب العرض:';

// Error
$_['error_permission']    = 'لا توجد لديك صلاحيات التعديل على إضافة قوقل تولك!';;
$_['error_code']          = 'الرمز مطلوب';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>