<?php
// Heading
$_['heading_title']       = 'البنرات';

// Text
$_['text_module']         = 'الإضافات';
$_['text_success']        = 'تم تعديل إضافة البنرات بنجاح!';
$_['text_content_top']    = 'أعلى';
$_['text_content_bottom'] = 'أسفل';
$_['text_column_left']    = 'يمين';
$_['text_column_right']   = 'يسار';

// Entry
$_['entry_banner']        = 'البنر:';
$_['entry_dimension']     = 'الأبعاد (W x H):';
$_['entry_layout']        = 'التصميم:';
$_['entry_position']      = 'الوضع:';
$_['entry_status']        = 'الحالة:';
$_['entry_sort_order']    = 'ترتيب العرض:';

// Error
$_['error_permission']    = 'لا يوجد لديك صلاحيات التعديل على إضافة البنرات!';
$_['error_dimension']     = 'الارتفاع والطول مطلوب!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>