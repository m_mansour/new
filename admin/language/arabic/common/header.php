<?php
// Heading
$_['heading_title']                    = 'لوحة تحكم الإدارة';

// Text 
$_['text_affiliate']                   = 'نظام الترويج';
$_['text_attribute']                   = 'السمات';
$_['text_attribute_group']             = 'مجموعة السمات';
$_['text_backup']                      = 'النسخة الاحتياطية';
$_['text_banner']                      = 'البنرات';
$_['text_catalog']                     = 'خيارات المتجر';
$_['text_category']                    = 'الأقسام';
$_['text_confirm']			               = 'هل أنت متأكد من إجراء هذه العملية?';
$_['text_country']                     = 'الدولة'; 
$_['text_coupon']                      = 'القسائم';
$_['text_currency']                    = 'العملة';
$_['text_customer']                    = 'العملاء';
$_['text_customer_group']              = 'مجموعة العملاء';
$_['text_dashboard']                   = 'الصفحة الرئيسية';
$_['text_design']                      = 'التصميم';
$_['text_download']                    = 'التحميل';
$_['text_error_log']                   = 'سجل الأخطاء';
$_['text_extension']                   = 'خيارات الملحقات';
$_['text_feed']                        = 'تغذية المنتجات';
$_['text_front']                       = 'واجهة المتجر';
$_['text_geo_zone']                    = 'المنطقة الجغرافية';
$_['text_help']                        = 'المساعدة';
$_['text_information']                 = 'معلومات';
$_['text_language']                    = 'اللغة';
$_['text_layout']                      = 'التصميم';
$_['text_localisation']                = 'الإعدادات الرئيسية';
$_['text_logged']                      = 'لقد تم تسجيل دخولك باسم <span>%s</span>';
$_['text_logout']                      = 'تسجيل الخروج';
$_['text_contact']                     = 'النشرة البريدية';
$_['text_manufacturer']                = 'الماركات';
$_['text_module']                      = 'الإضافات';
$_['text_option']                      = 'الخيارات';
$_['text_order']                       = 'قائمة الطلبات';
$_['text_order_status']                = 'حالة الطلب';
$_['text_opencart']                    = 'الصفحة الرئيسية';
$_['text_payment']                     = 'نظام الدفع';
$_['text_product']                     = 'المنتجات';
$_['text_reports']                     = 'خيارات التقرير';
$_['text_report_sale_order']           = 'الطلبات';
$_['text_report_sale_tax']             = 'الضريبة';
$_['text_report_sale_shipping']        = 'الشحن';
$_['text_report_sale_return']          = 'الإرجاع';
$_['text_report_sale_coupon']          = 'القسائم';
$_['text_report_product_viewed']       = 'المشاهدة';
$_['text_report_product_purchased']    = 'المشتريات';
$_['text_report_customer_order']       = 'الطلبات';
$_['text_report_customer_reward']      = 'نقاط المكافأت';
$_['text_report_customer_credit']      = 'الرصيد';
$_['text_report_affiliate_commission'] = 'العمولة';
$_['text_review']                      = 'التعليقات';
$_['text_return']                      = 'المنتجات المرجعة';
$_['text_return_action']               = 'إجراء الإرجاع';
$_['text_return_reason']               = 'سبب الإرجاع';
$_['text_return_status']               = 'حالة الإرجاع';
$_['text_sale']                        = 'خيارات المبيعات';
$_['text_shipping']                    = 'الشحنات';
$_['text_setting']                     = 'الإعدادات العامة';
$_['text_stock_status']                = 'حالة توفر المنتج';
$_['text_support']                     = 'منتدى الدعم الفني';
$_['text_system']                      = 'إعدادات النظام';
$_['text_tax_class']                   = 'نظام الضرائب';
$_['text_total']                       = 'نظام الطلبات';
$_['text_user']                        = 'الأعضاء';
$_['text_documentation']               = 'المستندات';
$_['text_users']                       = 'خيارات الأعضاء';
$_['text_user_group']                  = 'مجموعة الأعضاء';
$_['text_voucher']                     = 'قسائم الإهداءات';
$_['text_voucher_theme']               = 'تصميم القسائم';
$_['text_weight_class']                = 'نظام الوزن';
$_['text_length_class']                = 'نظام الطول';
$_['text_zone']                        = 'المنطقة';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>