<?php
// Heading
$_['heading_title']                 = 'الصفة الرئيسية';

// Text
$_['text_overview']                 = 'نظرة عامة';
$_['text_statistics']               = 'إحصائيات';
$_['text_latest_10_orders']         = 'أخر 10 طلبات';
$_['text_total_sale']               = 'إجمالي المبيعات:';
$_['text_total_sale_year']          = 'إجمالي المبيعات هذه السنة:';
$_['text_total_order']              = 'إجمالي الطلبات:';
$_['text_total_customer']           = 'عدد العملاء:';
$_['text_total_customer_approval']  = 'عملاء ينتظرون التفعيل:';
$_['text_total_review_approval']    = 'تقييمات تنتظر التفعيل:';
$_['text_total_affiliate']          = 'عدد المنتسبين لنظام الترويج:';
$_['text_total_affiliate_approval'] = 'منتسبون لنظام الترويج ينتظرون التفعيل:';
$_['text_day']                      = 'اليوم';
$_['text_week']                     = 'هذا الأسبوع';
$_['text_month']                    = 'هذا الشهر';
$_['text_year']                     = 'هذه السنة';
$_['text_order']                    = 'إجمالي الطلبات';
$_['text_customer']                 = 'إجمالي العملاء';

// Column 
$_['column_order']                  = 'رقم الطلب';
$_['column_customer']               = 'العميل';
$_['column_status']                 = 'الحالة';
$_['column_date_added']             = 'تاريخ الإضافة';
$_['column_total']                  = 'المجموع';
$_['column_firstname']              = 'الاسم';
$_['column_lastname']               = 'اسم العائلة';
$_['column_action']                 = 'الإجراء';

// Entry
$_['entry_range']                   = 'اختر المدى:';

// Error
$_['error_install']                 = 'لم تقوم بحذف مجلد التثبيت يرجى حذفه للأهمية!';
$_['error_image']                   = 'مسار الصورة %s غير قابل للكتابة!';
$_['error_image_cache']             = 'مسار كاش الصورة %s غير قابل للكتابة!';
$_['error_cache']                   = 'مسار الكاش %s غير قابل للكتابة!';
$_['error_download']                = 'مسار التحميل %s غير قابل للكتابة!';
$_['error_logs']                    = 'مسار السجلات %s غير قابل للكتابة!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>