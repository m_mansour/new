<?php
// Heading
$_['heading_title']     = 'اللغة';  

// Text
$_['text_success']      = 'تم تعديل بيانات اللغة بنجاح!'; 

// Column
$_['column_name']       = 'اسم اللغة';
$_['column_code']       = 'الرمز';
$_['column_sort_order'] = 'ترتيب العرض';
$_['column_action']     = 'الخيارات';

// Entry
$_['entry_name']        = 'اسم اللغة:';
$_['entry_code']        = 'الرمز:<br /><span class="help">لا تقم بالتغيير إذا كانت هذه هي اللغة الافتراضية الخاصة بك.</span>';
$_['entry_locale']      = 'ترميز اللغة:<br /><span class="help">مثال: en_US.UTF-8,en_US,en-gb,english</span>';
$_['entry_image']       = 'الصورة:<br /><span class="help">مثال: gb.png</span>';
$_['entry_directory']   = 'اسم مجلد اللغة:<br /><span class="help">اسم مجلد اللغة (الخيار حساس)</span>';
$_['entry_filename']    = 'اسم الملف:<br /><span class="help">اللغة الرئيسية اسم الملف دون تمديد</span>';
$_['entry_status']      = 'الحالة:<br /><span class="help">إخفاء أو إظهار في قائمة اللغة</span>';
$_['entry_sort_order']  = 'ترتيب العرض:';

// Error
$_['error_permission']  = 'لا توجد لديك صلاحيات التعديل على بيانات اللغة!';
$_['error_name']        = 'اسم اللغة يجب أن يكون أكبر من 3 وأقل من 32 حرفا!';
$_['error_code']        = 'رمز اللغة يجب أن يحتوي على 2 أحرف!';
$_['error_locale']      = 'ترميز اللغة مطلوب!';
$_['error_image']       = 'اسم الصورة يجب أن يكون أكبر من 3 وأقل من 64 حرفا!';
$_['error_directory']   = 'اسم مجلد اللغة مطلوب!';
$_['error_filename']    = 'اسم الملف يجب أن يكون أكبر من 3 وأقل من 64 حرفا!';
$_['error_default']     = 'هذه اللغة لا يمكن حذفها لأنه قد تم تعينها اللغة الافتراضية للمتجر!';
$_['error_admin']       = 'هذه اللغة لا يمكن حذفها لأنه قد تم تعينها لغة المسؤول!';
$_['error_store']       = 'لا يمكن حذف اللغة لأنها معينا افتراضية لي %s متاجر!';
$_['error_order']       = 'هذه اللغة لا يمكن حذفها لأنه قد تم تعينها لي  %s طلبات!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>