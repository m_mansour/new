<?php
// Heading
$_['heading_title']      = 'SagePay';

// Text 
$_['text_payment']       = 'نظام الدفع'; 
$_['text_success']       = 'تم تعديل بيانات حساب SagePay بنجاح!';
$_['text_sagepay']       = '<a onclick="window.open(\'https://support.sagepay.com/apply/default.aspx?PartnerID=E511AF91-E4A0-42DE-80B0-09C981A3FB61\');"><img src="view/image/payment/sagepay.png" alt="SagePay" title="SagePay" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_sim']           = 'محاكاة';
$_['text_test']          = 'تجربة';
$_['text_live']          = 'مباشر';
$_['text_payment']       = 'الدفع';
$_['text_defered']       = 'الاختلاف';
$_['text_authenticate']  = 'التوثيق';

// Entry
$_['entry_vendor']       = 'معرف التاجر:';
$_['entry_password']     = 'كلمة المرور:';
$_['entry_test']         = 'الوضع التجريبي:';
$_['entry_transaction']  = 'طريقة التحويل:';
$_['entry_total']        = 'الإجمالي:<br /><span class="help">لتقدم لشراء يجب وصول المبلغ الإجمالي للطلبات وبعد ذلك سوف تكون وسيلة الدفع متاحة.</span>';
$_['entry_order_status'] = 'حالة الطلب:';
$_['entry_geo_zone']     = 'المنطقة الجغرافية:';
$_['entry_status']       = 'الحالة:';
$_['entry_sort_order']   = 'ترتيب العرض:';

// Error
$_['error_permission']   = 'لا يوجد لديك صلاحيات التعديل على SagePay!';
$_['error_vendor']       = 'معرف التاجر مطلوب!';
$_['error_password']     = 'كلمة المرور مطلوبة!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>