<?php
// Heading
$_['heading_title']      = 'توشيك أوت';

// Text 
$_['text_payment']       = 'نظام الدفع';
$_['text_success']       = 'تم تعديل بيانات حساب توشيك أوت بنجاح!';

// Entry
$_['entry_account']      = 'معرف حساب توشيك أوت:';
$_['entry_secret']       = 'الكلمة السرية:<br /><span class="help">كلمة السر للتأكيد التحويل مع ( يجب أن تكون نفس التي في صفحة حساب التاجر).</span>';
$_['entry_test']         = 'الوضع التجريبي:';
$_['entry_total']        = 'الإجمالي:<br /><span class="help">لتقدم لشراء يجب وصول المبلغ الإجمالي للطلبات وبعد ذلك سوف تكون وسيلة الدفع متاحة.</span>';
$_['entry_order_status'] = 'حالة الطلب:';
$_['entry_geo_zone']     = 'المنطقة الجغرافية:';
$_['entry_status']       = 'الحالة:';
$_['entry_sort_order']   = 'ترتيب العرض:';

// Error
$_['error_permission']   = 'لا يوجد لديك صلاحيات التعديل على توشيك أوت!';
$_['error_account']      = 'رقم الحساب مطلوب!';
$_['error_secret']       = 'الكلمة السرية مطلوبة!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>