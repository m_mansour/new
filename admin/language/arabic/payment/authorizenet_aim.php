<?php
// Heading
$_['heading_title']      = 'Authorize.Net (AIM)';

// Text 
$_['text_payment']       = 'نظام الدفع';
$_['text_success']       = 'تم تعديل بيانات حساب Authorize.Net (AIM) بنجاح!';
$_['text_test']          = 'تجربة';
$_['text_live']          = 'مباشر';
$_['text_authorization'] = 'تفويض';
$_['text_capture']       = 'امسك';

// Entry
$_['entry_login']        = 'معرف تسجيل الدخول:';
$_['entry_key']          = 'مفتاح التحويل:';
$_['entry_hash']         = 'MD5 تشفير:';
$_['entry_server']       = 'خادم التحويل:';
$_['entry_mode']         = 'وضع التحويل:';
$_['entry_method']       = 'طريقة التحويل:';
$_['entry_total']        = 'الإجمالي:<br /><span class="help">لتقدم لشراء يجب وصول المبلغ الإجمالي للطلبات وبعد ذلك سوف تكون وسيلة الدفع متاحة.</span>';
$_['entry_order_status'] = 'حالة الطلب:';
$_['entry_geo_zone']     = 'المنطقة الجغرافية:';
$_['entry_status']       = 'الحالة:';
$_['entry_sort_order']   = 'ترتيب العرض:';

// Error 
$_['error_permission']   = 'لا يوجد لديك صلاحيات التعديل على Authorize.Net (AIM)!';
$_['error_login']        = 'معرف تسجيل الدخول مطلوب!';
$_['error_key']          = 'مفتاح التحويل مطلوب!';
$_['error_hash']         = 'تشفير MD5 مطلوب!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>