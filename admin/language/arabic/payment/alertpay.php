<?php
// Heading
$_['heading_title']      = 'AlertPay';

// Text 
$_['text_payment']       = 'نظام الدفع';
$_['text_success']       = 'تم تعديل بيانات حساب AlertPay بنجاح!';
      
// Entry
$_['entry_merchant']     = 'معرف التاجر:';
$_['entry_security']     = 'الرمز الأمني:';
$_['entry_callback']     = 'رابط AlertPay:<br /><span class="help">يجب ضبط هذه في لوحة تحكم AlertPay. سوف تحتاج أيضا إلى التحقق من "IPN" انه مفعل.</span>';
$_['entry_total']        = 'الإجمالي:<br /><span class="help">لتقدم لشراء يجب وصول المبلغ الإجمالي لطلبات وبعد ذلك سوف تكون وسيلة الدفع متاحة.</span>';
$_['entry_order_status'] = 'حالة الطلب:';
$_['entry_geo_zone']     = 'المنطقة الجغرافية:';
$_['entry_status']       = 'الحالة:';
$_['entry_sort_order']   = 'ترتيب العرض:';

// Error
$_['error_permission']   = 'لا يوجد لديك صلاحيات التعديل على AlertPay!';
$_['error_merchant']     = 'معرف التاجر مطلوب!';
$_['error_security']     = 'الرمز الأمني مطلوب!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>