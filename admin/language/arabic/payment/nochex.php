<?php
// Heading
$_['heading_title']      = 'NOCHEX';

// Text 
$_['text_payment']       = 'نظام الدفع';
$_['text_success']       = 'تم تعديل بيانات حساب NOCHEX بنجاح!';
$_['text_nochex']	       = '<a onclick="window.open(\'https://secure.nochex.com/apply/merchant_info.aspx?partner_id=172198798\');"><img src="view/image/payment/nochex.png" alt="NOCHEX" title="NOCHEX" style="border: 1px solid #EEEEEE;" /><br /><small>تسجيل حساب جديد</small></a>';
$_['text_seller']        = 'تاجر / حساب شخصي';
$_['text_merchant']      = 'حساب التاجر';
      
// Entry
$_['entry_email']        = 'البريد الالكتروني:';
$_['entry_account']      = 'نوع الحساب:';
$_['entry_merchant']     = 'معرف التاجر:';
$_['entry_template']     = 'قالب كلمة المرور:';
$_['entry_test']         = 'تجربة:';
$_['entry_total']        = 'الإجمالي:<br /><span class="help">لتقدم لشراء يجب وصول المبلغ الإجمالي للطلبات وبعد ذلك سوف تكون وسيلة الدفع متاحة.</span>';
$_['entry_order_status'] = 'حالة الطلب:';
$_['entry_geo_zone']     = 'المنطقة الجغرافية:';
$_['entry_status']       = 'الحالة:';
$_['entry_sort_order']   = 'ترتيب العرض:';

// Error
$_['error_permission']   = 'لا يوجد لديك صلاحيات التعديل على NOCHEX!';
$_['error_email']        = 'البريد الالكتروني مطلوب';
$_['error_merchant']     = 'معرف التاجر مطلوب!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>