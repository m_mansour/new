<?php
// Heading
$_['heading_title']      = 'بوابة بي اس اي';

// Text 
$_['text_payment']       = 'نظام الدفع'; 
$_['text_success']       = 'تم تعديل بيانات حساب بوابة بي اس اي بنجاح!';

// Entry
$_['entry_merchant']     = 'معرف التاجر:';
$_['entry_password']     = 'كلمة المرور:';
$_['entry_gateway']      = 'ربط البوابة:';
$_['entry_test']         = 'الوضع التجريبي:';
$_['entry_total']        = 'الإجمالي:<br /><span class="help">لتقدم لشراء يجب وصول المبلغ الاجمالي لطلبات وبعد ذلك سوف تكون وسيلة الدفع متاحة.</span>';
$_['entry_order_status'] = 'حالة الطلب:';
$_['entry_geo_zone']     = 'المنطقة الجغرافية:';
$_['entry_status']       = 'الحالة:';
$_['entry_sort_order']   = 'ترتيب العرض:';

// Error
$_['error_permission']   = 'لا يوجد لديك صلاحيات التعديل على بوابة بي اس اي!';
$_['error_merchant']     = 'معرف التاجر مطلوب!';
$_['error_password']     = 'كلمة المرور مطلوبة!';
$_['error_gateway']      = 'رابط البوابة مطلوب!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>