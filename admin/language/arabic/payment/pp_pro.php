<?php
// Heading
$_['heading_title']      = 'باي بال موقع الدفع المميز';

// Text 
$_['text_payment']       = 'نظام الدفع';
$_['text_success']       = 'تم تعديل بيانات حساب باي بال موقع الدفع المميز بنجاح!';
$_['text_pp_pro']        = '<a onclick="window.open(\'https://www.paypal.com/uk/mrb/pal=W9TBB5DTD6QJW\');"><img src="view/image/payment/paypal.png" alt="PayPal Website Payment Pro" title="PayPal Website Payment Pro" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization'] = 'الترخيص';
$_['text_sale']          = 'بيع';

// Entry
$_['entry_username']     = 'اسم المستخدم API:';
$_['entry_password']     = 'كلمة مرور API:';
$_['entry_signature']    = 'توقيع API:';
$_['entry_test']         = 'الوضع التجريبي:<br /><span class="help">استخدم المباشر أو التجريبي من خدمة بوابة (صندوق الارسال) لتنفيذ التحويلات?</span>';
$_['entry_transaction']  = 'طريقة التحويل:';
$_['entry_total']        = 'الإجمالي:<br /><span class="help">لتقدم لشراء يجب وصول المبلغ الإجمالي للطلبات وبعد ذلك سوف تكون وسيلة الدفع متاحة.</span>';
$_['entry_order_status'] = 'حالة الطلب:';
$_['entry_geo_zone']     = 'المنطقة الجغرافية:';
$_['entry_status']       = 'الحالة:';
$_['entry_sort_order']   = 'ترتيب العرض:';

// Error
$_['error_permission']   = 'لا يوجد لديك صلاحيات التعديل على باي بال موقع الدفع المميز!';
$_['error_username']     = 'اسم المستخدم API مطلوب!'; 
$_['error_password']     = 'كلمة مرور API مطلوبة!'; 
$_['error_signature']    = 'توقيع API مطلوب!'; 

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>