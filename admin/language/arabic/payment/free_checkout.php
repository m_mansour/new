<?php
// Heading
$_['heading_title']      = 'التقدم لشراء مجانا';

// Text
$_['text_payment']       = 'نظام الدفع';
$_['text_success']       = 'تم تعديل التقدم لشراء بنجاح!';

// Entry
$_['entry_order_status'] = 'حالة الطلب:';
$_['entry_status']       = 'الحالة:';
$_['entry_sort_order']   = 'ترتيب العرض:';

// Error
$_['error_permission']   = 'لا يوجد لديك صلاحيات التعديل على التقدم لشراء!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>