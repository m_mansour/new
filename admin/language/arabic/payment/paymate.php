<?php
// Heading
$_['heading_title']      = 'Paymate';

// Text 
$_['text_payment']       = 'نظام الدفع';
$_['text_success']       = 'تم تعديل بيانات حساب Paymate بنجاح!';

// Entry
$_['entry_username']     = 'اسم المستخدم Paymate:';
$_['entry_total']        = 'الإجمالي:<br /><span class="help">لتقدم لشراء يجب وصول المبلغ الإجمالي للطلبات وبعد ذلك سوف تكون وسيلة الدفع متاحة.</span>';
$_['entry_order_status'] = 'حالة الطلب:';
$_['entry_geo_zone']     = 'المنطقة الجغرافية:';
$_['entry_status']       = 'الحالة:';
$_['entry_sort_order']   = 'ترتيب العرض:';

// Error
$_['error_permission']   = 'لا يوجد لديك صلاحيات التعديل على Paymate!';
$_['error_username']     = 'اسم المستخدم Paymate مطلوب!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>