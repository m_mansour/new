<?php
// Heading
$_['heading_title']      = 'قوقل شيك أوت';

// Text
$_['text_payment']       = 'نظام الدفع';
$_['text_success']       = 'تم تعديل بيانات حساب قوقل شيك أوت بنجاح!';

// Entry
$_['entry_merchant_id']  = 'معرف التاجر:';
$_['entry_merchant_key'] = 'مفتاح التاجر:';
$_['entry_currency']     = 'العملة:';
$_['entry_test']         = 'الوضع التجريبي:';
$_['entry_status']       = 'الحالة:';

// Error
$_['error_permission']   = 'لا يوجد لديك صلاحيات التعديل على قوقل شيك أوت!';
$_['error_merchant_id']  = 'معرف التاجر مطلوب!';
$_['error_merchant_key'] = 'مفتاح التاجر مطلوب!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>