<?php
// Heading					
$_['heading_title']		        = 'موني بوكرز';
							
// Text 					
$_['text_payment']		        = 'نظام الدفع';
$_['text_success']		        = 'تم تعديل بيانات موني بوكرز بنجاح!';
$_['text_moneybookers']	      = '<a onclick="window.open(\'https://www.moneybookers.com/app/?rid=10111486\');"><img src="view/image/payment/moneybookers.png" alt="Moneybookers" title="Moneybookers" style="border: 1px solid #EEEEEE;" /></a>';
	
// Entry					
$_['entry_email']		          = 'البريد الالكتروني:';
$_['entry_secret']		        = 'المفتاح السري:';
$_['entry_total']             = 'الإجمالي:<br /><span class="help">لتقدم لشراء يجب وصول المبلغ الإجمالي للطلبات وبعد ذلك سوف تكون وسيلة الدفع متاحة.</span>';
$_['entry_order_status']      = 'حالة الطلب:';
$_['entry_pending_status']    = 'حالة الطلب المعلق:';
$_['entry_canceled_status']   = 'حالة الطلب الملغي:';
$_['entry_failed_status']     = 'حالة الطلب الغير صحيح:';
$_['entry_chargeback_status'] = 'حالة الطلب استعادة المبلغ:';
$_['entry_geo_zone']          = 'المنطقة الجغرافية:';
$_['entry_status']            = 'الحالة:';
$_['entry_sort_order']        = 'ترتيب العرض:';

// Error					
$_['error_permission']	      = 'لا يوجد لديك صلاحيات التعديل على موني بوكرز!'; 
$_['error_email']		          = 'البريد الالكتروني مطلوب!';
$_['error_secret']		        = 'المفتاح السري مطلوب!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>