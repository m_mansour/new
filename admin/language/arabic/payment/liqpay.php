<?php
// Heading
$_['heading_title']      = 'LIQPAY';

// Text 
$_['text_payment']       = 'نظام الدفع';
$_['text_success']       = 'تم تعديل بيانات حساب LIQPAY بنجاح!';   
$_['text_pay']           = 'LIQPAY';
$_['text_card']          = 'البطاقة الائتمانية';

// Entry
$_['entry_merchant']     = 'معرف التاجر:';
$_['entry_signature']    = 'التوقيع:';
$_['entry_type']         = 'النوع:';
$_['entry_total']        = 'الإجمالي:<br /><span class="help">لتقدم لشراء يجب وصول المبلغ الإجمالي للطلبات وبعد ذلك سوف تكون وسيلة الدفع متاحة.</span>';
$_['entry_order_status'] = 'حالة الطلب:';
$_['entry_geo_zone']     = 'المنطقة الجغرافية:';
$_['entry_status']       = 'الحالة:';
$_['entry_sort_order']   = 'ترتيب العرض:';

// Error
$_['error_permission']   = 'لا يوجد لديك صلاحيات التعديل على LIQPAY!';
$_['error_merchant']     = 'معرف التاجر مطلوب!';
$_['error_signature']    = 'التوقيع مطلوب!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>