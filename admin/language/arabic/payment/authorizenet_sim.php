<?php
// Heading
$_['heading_title']         = 'Authorize.Net (SIM)';

// Text 
$_['text_payment']          = 'نظام الدفع';
$_['text_success']          = 'تم تعديل بيانات حساب Authorize.Net (SIM) بنجاح!';

// Entry
$_['entry_merchant']        = 'معرف التاجر:';
$_['entry_key']             = 'مفتاح التحويل:';
$_['entry_callback']        = 'رابط الإجابة الحقيقية:<br /><span class="help">يرجى تسجيل الدخول وضبط الإعدادات في <a href="https://secure.authorize.net" target="_blank" class="txtLink">https://secure.authorize.net</a>.</span>';
$_['entry_test']            = 'الوضع التجريبي:';
$_['entry_total']           = 'الإجمالي:<br /><span class="help">لتقدم لشراء يجب وصول المبلغ الإجمالي للطلبات وبعد ذلك سوف تكون وسيلة الدفع متاحة.</span>';
$_['entry_order_status']    = 'حالة الطلب:';
$_['entry_geo_zone']        = 'المنطقة الجغرافية:';
$_['entry_status']          = 'الحالة:';
$_['entry_sort_order']      = 'ترتيب العرض:';

// Error 
$_['error_permission']      = 'لا يوجد لديك صلاحيات التعديل على Authorize.Net (SIM)!';
$_['error_merchant']        = 'معرف التاجر مطلوب!';
$_['error_key']             = 'مفتاح التحويل مطلوب!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>