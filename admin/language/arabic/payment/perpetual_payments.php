<?php
// Heading
$_['heading_title']      = 'Perpetual Payments';

// Text 
$_['text_payment']        = 'نظام الدفع';
$_['text_success']        = 'تم تعديل بيانات حساب Perpetual Payments بنجاح!';

// Entry
$_['entry_auth_id']       = 'معرف الترخيص:';
$_['entry_auth_pass']     = 'كلمة مرور الترخيص:';
$_['entry_test']          = 'الوضع التجريبي:<br /><span class="help">استخدم مباشرة أو تجريبي لتحويل؟</span>';
$_['entry_total']         = 'الإجمالي:<br /><span class="help">لتقدم لشراء يجب وصول المبلغ الإجمالي للطلبات وبعد ذلك سوف تكون وسيلة الدفع متاحة.</span>';
$_['entry_order_status']  = 'حالة الطلب:';
$_['entry_geo_zone']      = 'المنطقة الجغرافية:';
$_['entry_status']        = 'الحالة:';
$_['entry_sort_order']    = 'ترتيب العرض:';

// Error
$_['error_permission']    = 'لا يوجد لديك صلاحيات التعديل على Perpetual Payments!';
$_['error_auth_id']       = 'معرف الترخيص مطلوب!'; 
$_['error_auth_pass']     = 'كلمة مرور الترخيص مطلوبة!'; 

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>