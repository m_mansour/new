<?php
// Heading
$_['heading_title']      = 'WorldPay';

// Text 
$_['text_payment']       = 'نظام الدفع';
$_['text_success']       = 'تم تعديل بيانات حساب WorldPay بنجاح!';
$_['text_successful']    = 'فعال (دائما مقبول)';
$_['text_declined']      = 'فعال (دائما مرفوض)';
$_['text_off']           = 'معطل';
      
// Entry
$_['entry_merchant']     = 'معرف التاجر:';
$_['entry_password']     = 'كلمة مرور الدفع:<br /><span class="help">يجب إعدادها من لوحة تحكم WorldPay.</span>';
$_['entry_callback']     = 'رابط الإجابة الحقيقية:<br /><span class="help">يجب ضبط هذه في لوحة تحكم WorldPay. سوف تحتاج أيضا إلى التحقق من استجابة التسوق انه مفعل.</span>';
$_['entry_test']         = 'الوضع التجريبي:';
$_['entry_total']        = 'الإجمالي:<br /><span class="help">لتقدم لشراء يجب وصول المبلغ الإجمالي للطلبات وبعد ذلك سوف تكون وسيلة الدفع متاحة.</span>';
$_['entry_order_status'] = 'حالة الطلب:';
$_['entry_geo_zone']     = 'المنطقة الجغرافية:';
$_['entry_status']       = 'الحالة:';
$_['entry_sort_order']   = 'ترتيب العرض:';

// Error
$_['error_permission']   = 'لا يوجد لديك صلاحيات التعديل على WorldPay!';
$_['error_merchant']     = 'معرف التاجر مطلوب!';
$_['error_password']     = 'كلمة المرور مطلوبة!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>