<?php
// Heading
$_['heading_title']      = 'SagePay (US)';

// Text 
$_['text_payment']       = 'نظام الدفع'; 
$_['text_success']       = 'تم تعديل بيانات حساب SagePay (US) بنجاح!';

// Entry
$_['entry_merchant_id']  = 'معرب التاجر:';
$_['entry_merchant_key'] = 'مفتاح التاجر:';
$_['entry_total']        = 'الإجمالي:<br /><span class="help">لتقدم لشراء يجب وصول المبلغ الإجمالي للطلبات وبعد ذلك سوف تكون وسيلة الدفع متاحة.</span>';
$_['entry_order_status'] = 'حالة الطلب:';
$_['entry_geo_zone']     = 'المنطقة الجغرافية:';
$_['entry_status']       = 'الحالة:';
$_['entry_sort_order']   = 'ترتيب العرض:';

// Error
$_['error_permission']   = 'لا يوجد لديك صلاحيات التعديل على SagePay (US)!';
$_['error_merchant_id']  = 'معرف التاجر مطلوب!';
$_['error_merchant_key'] = 'مفتاح التاجر مطلوب!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>