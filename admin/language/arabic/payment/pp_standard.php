<?php
// Heading
$_['heading_title']                  = 'باي بال';

// Text 
$_['text_payment']                   = 'نظام الدفع';
$_['text_success']                   = 'تم تعديل بيانات حساب الباي بال بنجاح!';
$_['text_pp_standard']               = '<a onclick="window.open(\'https://www.paypal.com/uk/mrb/pal=W9TBB5DTD6QJW\');"><img src="view/image/payment/paypal.png" alt="PayPal" title="PayPal" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization']             = 'ترخيص';
$_['text_sale']                      = 'بيع';

// Entry
$_['entry_email']                    = 'البريد الالكتروني:';
$_['entry_test']                     = 'تجربة الوضع:';
$_['entry_transaction']              = 'طريقة التحويل:';
$_['entry_pdt_token']				         = 'بيانات الدفع رمز النقل:<br/><span class="help">يستخدم بيانات الدفع رمز النقل لمزيد من الأمن والموثوقية. لمعرفة كيفية تمكين بيانات الدفع رمز النقل <a href="https://cms.paypal.com/us/cgi-bin/?&cmd=_render-content&content_ID=developer/howto_html_paymentdatatransfer" alt="">من هنا</a></span>';
$_['entry_debug']					           = 'وضع التصحيح:<br/><span class="help">معلومات إضافية إلى سجل النظام.</span>';
$_['entry_total']                    = 'الإجمالي:<br /><span class="help">لتقدم لشراء يجب وصول المبلغ الإجمالي للطلبات وبعد ذلك سوف تكون وسيلة الدفع متاحة.</span>';
$_['entry_canceled_reversal_status'] = 'حالة إلغاء عكسية:';
$_['entry_completed_status']         = 'حالة الاكتمال:';
$_['entry_denied_status']			       = 'حالة الرفض:';
$_['entry_expired_status']			     = 'حالة الانتهاء:';
$_['entry_failed_status']			       = 'حالة الفشل:';
$_['entry_pending_status']			     = 'حالة الانتظار:';
$_['entry_processed_status']		     = 'حالة المعالجة:';
$_['entry_refunded_status']			     = 'حالة الاسترداد:';
$_['entry_reversed_status']			     = 'حالة عكسية:';
$_['entry_voided_status']		         = 'حالة الإلغاء:';
$_['entry_geo_zone']                 = 'المنطقة الجغرافية:';
$_['entry_status']                   = 'الحالة:';
$_['entry_sort_order']               = 'ترتيب العرض:';

// Error
$_['error_permission']               = 'لا يوجد لديك صلاحيات التعديل على الباي بال!';
$_['error_email']                    = 'البريد الالكتروني مطلوب!'; 

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>