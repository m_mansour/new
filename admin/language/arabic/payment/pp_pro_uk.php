<?php
// Heading
$_['heading_title']      = 'باي بال موقع الدفع المميز (UK)';

// Text 
$_['text_payment']       = 'نظام الدفع';
$_['text_success']       = 'تم تعديل بيانات حساب باي بال موقع الدفع المميز (UK) بنجاح!';
$_['text_pp_pro_uk']     = '<a onclick="window.open(\'https://www.paypal.com/uk/mrb/pal=W9TBB5DTD6QJW\');"><img src="view/image/payment/paypal.png" alt="PayPal Website Payment Pro (UK)" title="PayPal Website Payment Pro (UK)" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization'] = 'الترخيص';
$_['text_sale']          = 'بيع';

// Entry
$_['entry_vendor']       = 'معرف التاجر:<br /><span class="help">معرف التاجر الخاص بك الذي قمت بإنشائه عند التسجيل في موقع باي بال موقع الدفع المميز.</span>';
$_['entry_user']         = 'اسم المستخدم:<br /><span class="help">إذا قمت بإضافة مستخدم أخر أو أكثر في حسابك. هذه المعرف للمستخدم مخول لعمل تحويلات. ولكن، إذا لم تكن قد قمت بإنشاء مستخدمين إضافيين على حسابك. المستخدم له نفس التصريح لمعرف التاجر.</span>';
$_['entry_password']     = 'كلمة المرور:<br /><span class="help">كلمة المرور التي من 6 إلى 23 حرفا التي قمت بأنشائها عند عمل الحساب الخاص بك.</span>';
$_['entry_partner']      = 'الشريك:<br /><span class="help">المعرف الذي تم إعطائك إياه من قبل الموزع المعتمد لباي بال الذي قام بتسجيلك SDK إذا كنت قد اشتريت الحساب مباشرة من باي بال، استخدم باي بال UK.</span>';
$_['entry_test']         = 'الوضع التجريبي:<br /><span class="help">استخدم المباشر أو التجريبي من خدمة بوابة (صندوق الإرسال) لتنفيذ التحويلات?</span>';
$_['entry_transaction']  = 'طرقة التحويل:';
$_['entry_total']        = 'الإجمالي:<br /><span class="help">لتقدم لشراء يجب وصول المبلغ الاجمالي لطلبات وبعد ذلك سوف تكون وسيلة الدفع متاحة.</span>';
$_['entry_order_status'] = 'حالة الطلب:';
$_['entry_geo_zone']     = 'المنطقة الجغرافية:';
$_['entry_status']       = 'الحالة:';
$_['entry_sort_order']   = 'ترتيب العرض:';

// Error
$_['error_permission']   = 'لا يوجد لديك صلاحيات التعديل على باي بال موقع الدفع المميز (UK)!';
$_['error_vendor']       = 'معرف التاجر مطلوب!'; 
$_['error_user']         = 'اسم المستخدم مطلوب!'; 
$_['error_password']     = 'كلمة المرور مطلوبة!'; 
$_['error_partner']      = 'الشريك مطلوب!'; 

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>