<?php
// Heading  
$_['heading_title']     = 'قسائم الإهداءات';

// Text
$_['text_send']         = 'إرسال';
$_['text_success']      = 'تم تعديل قسائم الإهداءات بنجاح!';
$_['text_sent']         = 'تم إرسال قسيمة الإهداءات بنجاح!';
$_['text_wait']         = 'يرجى الانتظار!';

// Column
$_['column_name']       = 'اسم القسيمة';
$_['column_code']       = 'الرمز';
$_['column_from']       = 'من';
$_['column_to']         = 'إلى';
$_['column_amount']     = 'المبلغ';
$_['column_theme']      = 'الشكل';
$_['column_date_added'] = 'تاريخ الإضافة';
$_['column_status']     = 'الحالة';
$_['column_order_id']   = 'رقم الطلب';
$_['column_customer']   = 'العميل';
$_['column_date_added'] = 'تاريخ الإضافة';
$_['column_action']     = 'الحالة';

// Entry
$_['entry_code']        = 'الرمز:<br /><span class="help">الرمز هو الذي يدخله العميل لتنشيط القسيمة.</span>';
$_['entry_from_name']   = 'الاسم:';
$_['entry_from_email']  = 'بريد المرسل:';
$_['entry_to_name']     = 'اسم المتلقي:';
$_['entry_to_email']    = 'بريد المتلقي:';
$_['entry_message']     = 'الرسالة:';
$_['entry_amount']      = 'المبلغ:';
$_['entry_theme']       = 'الشكل:';
$_['entry_status']      = 'الحالة:';

// Error
$_['error_permission']  = 'لا توجد لديك صلاحيات التعديل على قسائم الإهداءات!';
$_['error_code']        = 'الرمز يجب أن يكون ما بين 3 حتى 10 حرفا!';
$_['error_to_name']     = 'اسم المتلقي يجب أن يكون ما بين 1 حتى 64 حرفا!';
$_['error_from_name']   = 'الاسم يجب أن يكون ما بين 1 حتى 64 حرفا!';
$_['error_email']       = 'يبدو أن البريد الالكتروني غير صحيح!';
$_['error_amount']      = 'يجب أن يكون المبلغ أكبر من أو يساوي 1!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>