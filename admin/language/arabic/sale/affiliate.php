<?php
// Heading
$_['heading_title']             = 'نظام الترويج';

// Text
$_['text_success']              = 'تم تعديل نظام الترويج بنجاح!';
$_['text_approved']             = 'تم الموافق على %s حساب!';
$_['text_wait']                 = 'يرجى الانتظار!';
$_['text_balance']              = 'الرصيد:';
$_['text_cheque']               = 'شيك';
$_['text_paypal']               = 'باي بال';
$_['text_bank']                 = 'تحويل بنكي';

// Column
$_['column_name']               = 'اسم المروج';
$_['column_email']              = 'البريد الإلكتروني';
$_['column_code']               = 'رمز التتبع';
$_['column_balance']            = 'الرصيد';
$_['column_status']             = 'الحالة';
$_['column_approved']           = 'فعال';
$_['column_date_added']         = 'تاريخ الإضافة';
$_['column_description']        = 'الوصف';
$_['column_amount']             = 'المبلغ';
$_['column_action']             = 'الإجراء';

// Entry
$_['entry_firstname']           = 'الاسم:';
$_['entry_lastname']            = 'اسم العائلة:';
$_['entry_email']               = 'البريد الإلكتروني:';
$_['entry_telephone']           = 'رقم الهاتف:';
$_['entry_fax']                 = 'رقم الفكس:';
$_['entry_status']              = 'الحالة:';
$_['entry_password']            = 'كلمة المرور:';
$_['entry_confirm']             = 'تأكيد كلمة المرور:';
$_['entry_company']             = 'الشركة:';
$_['entry_address_1']           = 'العنوان الأول:';
$_['entry_address_2']           = 'العنوان الثاني:';
$_['entry_city']                = 'المدينة:';
$_['entry_postcode']            = 'صندوق البريد:';
$_['entry_country']             = 'الدولة:';
$_['entry_zone']                = 'المنطقة أو المحافظة:';
$_['entry_code']                = 'كود التتبع:<span class="help">رمز التتبع الذي سيتم استخدامه لتتبع الدعوات.</span>';
$_['entry_commission']          = 'العمولة (%):<span class="help">النسبة المئوية التي سوف يحصل عليها المروج على كل طلبيه.</span>';
$_['entry_tax']                 = 'رقم الضريبة:';
$_['entry_payment']             = 'طريقة الدفع:';
$_['entry_cheque']              = 'اسم المدفوع له على الشيك:';
$_['entry_paypal']              = 'البريد الالكتروني لباي بال:';
$_['entry_bank_name']           = 'اسم البنك:';
$_['entry_bank_branch_number']  = 'ABA/BSB رقم (رقم الفرع):';
$_['entry_bank_swift_code']     = 'رمز السويفت:';
$_['entry_bank_account_name']   = 'اسم صاحب الحساب:';
$_['entry_bank_account_number'] = 'رقم الحساب:';
$_['entry_amount']              = 'المبلغ:';
$_['entry_description']         = 'الوصف:';

// Error
$_['error_permission']          = 'لا توجد لديك صلاحيات التعديل على نظام الترويج!';
$_['error_firstname']           = 'الاسم الأول يجب أن يكون أكثر من 3 وأقل من 32 حرفا!';
$_['error_lastname']            = 'اسم العائلة يجب أن يكون أكثر من 3 وأقل من 32 حرفا!';
$_['error_email']               = 'البريد الإلكتروني غير صحيح يرجى إعادة كتابته من جديد!';
$_['error_telephone']           = 'رقم الهاتف يجب أن يكون أكثر من 3 وأقل من 32 رقما!';
$_['error_password']            = 'كلمة المرور يجب أن تكون أكثر من 3 وأقل من 20 حرفا!';
$_['error_confirm']             = 'لم تتطابق كلمة المرور الرجاء إعادة كتبته مرة أخرى!';
$_['error_address_1']           = 'العنوان يجب أن يكون أكثر من 3 وأقل من 128 حرفا!';
$_['error_city']                = 'اسم المدينة يجب أن يكون أكثر من 3 وأقل من 128 حرفا!';
$_['error_postcode']            = 'يجب ان يكون صندوق البريد اكثر من 2 وأقل من 10 حرفا لهذه الدولة!';
$_['error_country']             = 'يرجي اختيار الدولة!';
$_['error_zone']                = 'يرجي اختيار المنطقة أو المحافظة!';
$_['error_code']                = 'رمز التتبع مطلوب!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>