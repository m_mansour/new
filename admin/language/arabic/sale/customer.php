<?php
// Heading
$_['heading_title']         = 'العملاء';

// Text
$_['text_login']            = 'تسجيل الدخول';
$_['text_success']          = 'تم تعديل بيانات العميل بنجاح';
$_['text_approved']         = 'تم تفعيل %s حساب!';
$_['text_wait']             = 'يرجى الانتظار!';
$_['text_balance']          = 'الرصيد:';

// Column
$_['column_name']           = 'اسم العميل';
$_['column_email']          = 'البريد الالكتروني';
$_['column_customer_group'] = 'مجموعة العملاء';
$_['column_status']         = 'الحالة';
$_['column_approved']       = 'التفعيل';
$_['column_date_added']     = 'تاريخ الإضافة';
$_['column_description']    = 'الوصف';
$_['column_amount']         = 'المبلغ';
$_['column_points']         = 'النقاط';
$_['column_ip']             = 'الاي بي';
$_['column_total']          = 'إجمالي الحسابات';
$_['column_action']         = 'الإجراء';

// Entry
$_['entry_firstname']       = 'الاسم:';
$_['entry_lastname']        = 'اسم العائلة:';
$_['entry_email']           = 'البريد الالكتروني:';
$_['entry_telephone']       = 'الهاتف:';
$_['entry_fax']             = 'الفاكس:';
$_['entry_newsletter']      = 'النشرة البريدية:';
$_['entry_customer_group']  = 'مجموعة العملاء:';
$_['entry_status']          = 'الحالة:';
$_['entry_password']        = 'كلمة المرور:';
$_['entry_confirm']         = 'تأكيد كلمة المرور:';
$_['entry_company']         = 'الشركة:';
$_['entry_address_1']       = 'العنوان الأول:';
$_['entry_address_2']       = 'العنوان الثاني:';
$_['entry_city']            = 'المدينة:';
$_['entry_postcode']        = 'الرمز البريدي:';
$_['entry_country']         = 'الدولة:';
$_['entry_zone']            = 'المنطقة أو المحافظة:';
$_['entry_default']         = 'العنوان الافتراضي:';
$_['entry_amount']          = 'المبلغ:';
$_['entry_points']          = 'النقاط:';
$_['entry_description']     = 'الوصف:';

// Error
$_['error_warning']         = 'يرجى التحقق من الحقول جيداً!';
$_['error_permission']      = 'لا يوجد لديك صلاحيات التعديل على العملاء';
$_['error_firstname']       = 'الاسم يجب أن يكون أكبر من 3 وأقل من 32 حرفا!';
$_['error_lastname']        = 'اسم العائلة يجب أن يكون أكبر من 3 وأقل من 32 حرفا!';
$_['error_email']           = 'يبدو أن البريد الالكتروني غير صحيح!';
$_['error_telephone']       = 'الهاتف يجب أن يكون أكبر من 3 وأقل من 32 رقما!';
$_['error_password']        = 'كلمة المرور يجب أن تكون أكبر من 3 وأقل من 20 حرفا!';
$_['error_confirm']         = 'كلمة المرور وتأكيد كلمة المرور لا يتطابقان!';
$_['error_address_1']       = 'العنوان يجب أن يكون أكبر من 3 وأقل من 128 حرفا!';
$_['error_city']            = 'اسم المدينة يجب أن يكون أكبر من 3 وأقل من 128 حرفا!';
$_['error_postcode']        = 'يجب أن يكون عنوان صندوق البريد من 2 إلى 10 أرقام لهذا البلد !';
$_['error_country']         = 'يرجى تحديد البلد!';
$_['error_zone']            = 'يرجى تحديد المنطقة أو المحافظة!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>