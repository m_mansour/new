<?php
// Heading
$_['heading_title']           = 'الطلبات';

// Text
$_['text_success']            = 'تم تعديل الطلب بنجاح!';
$_['text_order_id']           = 'رقم الطلب:';
$_['text_invoice_no']         = 'رقم الفاتورة:';
$_['text_invoice_date']       = 'تاريخ الفاتورة:';
$_['text_store_name']         = 'اسم المتجر:';
$_['text_store_url']          = 'رابط المتجر:';
$_['text_customer']           = 'العميل:';
$_['text_customer_group']     = 'مجموعة العملاء:';
$_['text_email']              = 'البريد الإلكتروني:';
$_['text_ip']                 = 'الاي بي:';
$_['text_telephone']          = 'رقم الهاتف';
$_['text_fax']                = 'رقم الفاكس';
$_['text_shipping_method']    = 'طريقة الشحن:';
$_['text_payment_method']     = 'طريقة الدفع:';
$_['text_total']              = 'الإجمالي:';
$_['text_reward']             = 'نقاط المكافآت:';
$_['text_order_status']       = 'حالة الطلب:';
$_['text_comment']            = 'التعليق:';
$_['text_affiliate']          = 'نظام الترويج:';
$_['text_commission']         = 'العمولة:';
$_['text_date_added']         = 'تاريخ الإضافة:';
$_['text_date_modified']      = 'تاريخ التعديل:';
$_['text_firstname']          = 'الاسم:';
$_['text_lastname']           = 'اسم العائمة:';
$_['text_company']            = 'الشركة:';
$_['entry_address_1']         = 'العنوان الأول:';
$_['entry_address_2']         = 'العنوان الثاني:';
$_['entry_postcode']          = 'صندوق البريد:';
$_['entry_city']              = 'المدينة:';
$_['entry_zone']              = 'المنطقة أو المحافظة:';
$_['text_zone_code']          = 'رمز المنطقة أو المحافظة:';
$_['text_country']            = 'الدولة:';
$_['text_download']           = 'طلب تحميلات';
$_['text_invoice']            = 'الفاتورة';
$_['text_to']                 = 'إلى';
$_['text_ship_to']            = 'الشحن إلى (إذا كان العنوان مختلف)';
$_['text_missing_orders']     = 'طلبات مفقودة';
$_['text_default']            = 'الافتراضي';
$_['text_wait']               = 'يرجى الانتظار!';
$_['text_reward_add']         = 'إضافة نقاط مكافآت';
$_['text_reward_added']       = 'أضيفت نقاط المكافآت!';
$_['text_reward_remove']      = 'إزالة نقاط المكافآت';
$_['text_reward_removed']     = 'أزيلت نقاط المكافآت!';
$_['text_commission_add']     = 'إضافة عمولة';
$_['text_commission_added']   = 'أضيفت العمولة!';
$_['text_commission_remove']  = 'إزالة العمولة';
$_['text_commission_removed'] = 'أزيلت العمولة!';
$_['text_credit_add']         = 'إضافة رصيد';
$_['text_credit_added']       = 'أضيفه الرصيد للحساب!';
$_['text_credit_remove']      = 'إزالة الرصيد';
$_['text_credit_removed']     = 'أزيله الرصيد من الحساب!';

// Column
$_['column_order_id']         = 'رقم الطلب';
$_['column_customer']         = 'العميل';
$_['column_status']           = 'الحالة';
$_['column_date_added']       = 'تاريخ الإضافة';
$_['column_date_modified']    = 'تاريخ التعديل';
$_['column_total']            = 'الإجمالي';
$_['column_product']          = 'المنتج';
$_['column_model']            = 'الموديل';
$_['column_quantity']         = 'العدد';
$_['column_price']            = 'سعر الوحدة';
$_['column_download']         = 'اسم ملف التحميل';
$_['column_filename']         = 'اسم الملف';
$_['column_remaining']        = 'المتبقي';
$_['column_comment']          = 'التعليق';
$_['column_notify']           = 'تبليغ العميل';
$_['column_action']           = 'الإجراء';

// Entry
$_['entry_store']             = 'المتجر:';
$_['entry_customer']          = 'العميل:';
$_['entry_firstname']         = 'الاسم:';
$_['entry_lastname']          = 'اسم العائلة:';
$_['entry_email']             = 'البريد الالكتروني:';
$_['entry_telephone']         = 'رقم الهاتف:';
$_['entry_fax']               = 'رقم الفاكس:';
$_['entry_address']           = 'اختر العنوان:';
$_['entry_company']           = 'الشركة:';
$_['entry_address_1']         = 'العنوان الأول:';
$_['entry_address_2']         = 'العنوان الثاني:';
$_['entry_city']              = 'المدينة:';
$_['entry_postcode']          = 'صندوق البريد:';
$_['entry_country']           = 'الدولة:';
$_['entry_zone']              = 'المنطقة أو المحافظة:';
$_['entry_zone_code']         = 'رمز المنطقة:';
$_['entry_product']           = 'المنتج:';
$_['entry_model']             = 'الموديل:';
$_['entry_quantity']          = 'العدد:';
$_['entry_price']             = 'سعر الوحدة:';
$_['entry_affiliate']         = 'نظام الترويج:';
$_['entry_order_status']      = 'حالة الطلب:';
$_['entry_notify']            = 'تنبيه العميل:';
$_['entry_comment']           = 'التعليق:';
$_['entry_shipping']          = 'طرية الشحن:';
$_['entry_payment']           = 'طريقة الدفع:';

// Error
$_['error_permission']        = 'لا يوجد لديك تصريح لتعديل الطلبات!';
$_['error_firstname']         = 'الاسم يجب أن يكون أكثر من 3 وأقل من 32 حرفا!';
$_['error_lastname']          = 'اسم العائلة يجب أن يكون أكثر من 3 وأقل من 32 حرفا!';
$_['error_email']             = 'البريد الإلكتروني غير صحيح يرجى إعادة كتابته من جديد!';
$_['error_telephone']         = 'رقم الهاتف يجب أن يكون أكثر من 3 وأقل من 32 رقما!';
$_['error_password']          = 'كلمة المرور يجب أن تكون أكثر من 3 وأقل من 20 حرفا!';
$_['error_confirm']           = 'لم تتطابق كلمة المرور الرجاء إعادة كتبته مرة أخرى!';
$_['error_address_1']         = 'العنوان يجب أن يكون أكثر من 3 وأقل من 128 حرفا!';
$_['error_city']              = 'اسم المدينة يجب أن يكون أكثر من 3 وأقل من 128 حرفا!';
$_['error_postcode']          = 'يجب أن يكون عنوان صندوق البريد من 2 إلى 10 أرقام لهذا البلد !';
$_['error_country']           = 'يرجي اختيار الدولة!';
$_['error_zone']              = 'يرجي اختيار المنطقة أو المحافظة!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>