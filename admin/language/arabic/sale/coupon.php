<?php
// Heading  
$_['heading_title']       = 'إعدادات القسائم';

// Text
$_['text_success']        = 'تم تعديل القسائم بنجاح!';
$_['text_percent']        = 'نسبة مئوية';
$_['text_amount']         = 'المبلغ الثابت';

// Column
$_['column_name']         = 'اسم القسيمة';
$_['column_code']         = 'الرمز';
$_['column_discount']     = 'الخصم';
$_['column_date_start']   = 'تاريخ البدء';
$_['column_date_end']     = 'تاريخ الانتهاء';
$_['column_status']       = 'الحالة';
$_['column_order_id']     = 'رقم الطلب';
$_['column_customer']     = 'العميل';
$_['column_amount']       = 'المبلغ';
$_['column_date_added']   = 'تاريخ الإضافة';
$_['column_action']       = 'الإجراء';

// Entry
$_['entry_name']          = 'اسم القسيمة:';
$_['entry_code']          = 'الرمز:<br /><span class="help">الرمز الذي يدخله العميل للحصول على الخصم</span>';
$_['entry_type']          = 'النوع:<br /><span class="help">نسبة مئوية أو مبلغ ثابت</span>';
$_['entry_discount']      = 'الخصم:';
$_['entry_logged']        = 'تسجيل دخول العملاء:<br /><span class="help">يجب على العميل تسجيل الدخول لاستخدام القسيمة.</span>';
$_['entry_shipping']      = 'شحن مجاني:';
$_['entry_total']         = 'المبلغ الإجمالي:<br /><span class="help">المبلغ الإجمالي الذي يجب الوصول إلية قبل أن تكون القسيمة صالحة للاستخدام.</span>';
$_['entry_product']       = 'المنتجات:<br /><span class="help">اختيار المنتج الذي سوف يتم تطبيق القسيمة علية. عدم تحديد منتج سوف تطبق القسيمة على سلة المشتريات.</span>';
$_['entry_date_start']    = 'تاريخ البدء:';
$_['entry_date_end']      = 'تاريخ الانتهاء:';
$_['entry_uses_total']    = 'يستخدم لكل قسيمة:<br /><span class="help">الحد الأقصى لعدد مرات استخدام القسيمة من قبل أي عميل. يترك فارغا لعدم التحديد</span>';
$_['entry_uses_customer'] = 'يستخدم لكل عميل:<br /><span class="help">الحد الأقصى لعدد مرات استخدام القسيمة من قبل عميل واحد. يترك فارغا لعدم التحديد</span>';
$_['entry_status']        = 'الحالة:';

// Error
$_['error_permission']    = 'لا يوجد لديك تصريح تعديل القسائم!';
$_['error_name']          = 'اسم القسيمة يجب أن يكون أكبر من 3 وأقل من 64 حرفا!';
$_['error_code']          = 'الرمز يجب أن يكون أكبر من 3 وأقل من 10 حرفا!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>