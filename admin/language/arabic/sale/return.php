<?php
// Heading
$_['heading_title']        = 'إرجاع المنتجات';

// Text
$_['text_opened']          = 'مفتوح';
$_['text_unopened']        = 'غير مفتوح';
$_['text_success']         = 'تم تعديل إرجاع المنتجات بنجاح!';
$_['text_wait']            = 'يرجى الانتظار!';

// Text
$_['text_return_id']       = 'رقم الإرجاع:';
$_['text_order_id']        = 'رقم الطلب:';
$_['text_date_ordered']    = 'تاريخ الطلب:';
$_['text_customer']        = 'العميل:';
$_['text_email']           = 'البريد الإلكتروني:';
$_['text_telephone']       = 'رقم الهاتف:';
$_['text_return_status']   = 'حالة الإرجاع:';
$_['text_comment']         = 'التعليق:';
$_['text_date_added']      = 'تاريخ الإضافة:';
$_['text_date_modified']   = 'تاريخ التعديل:';

// Column
$_['column_return_id']     = 'رقم الإرجاع';
$_['column_order_id']      = 'رقم الطلب';
$_['column_customer']      = 'العميل';
$_['column_quantity']      = 'الكمية';
$_['column_status']        = 'الحالة';
$_['column_date_added']    = 'تاريخ الإضافة';
$_['column_date_modified'] = 'تاريخ التعديل';
$_['column_product']       = 'المنتج';
$_['column_model']         = 'الموديل';
$_['column_reason']        = 'سبب الإرجاع';
$_['column_opened']        = 'مفتوح';
$_['column_notify']        = 'تنبيه العميل';
$_['column_comment']       = 'التعليق';
$_['column_action']        = 'الإجراء';

// Entry
$_['entry_customer']       = 'العميل:';
$_['entry_order_id']       = 'رقم الطلب:';
$_['entry_date_ordered']   = 'تاريخ الطلب:';
$_['entry_firstname']      = 'الاسم:';
$_['entry_lastname']       = 'اسم العائلة:';
$_['entry_email']          = 'البريد الإلكتروني:';
$_['entry_telephone']      = 'رقم الهاتف:';
$_['entry_product']        = 'المنتج:';
$_['entry_model']          = 'الموديل:';
$_['entry_quantity']       = 'الكمية:';
$_['entry_reason']         = 'سبب الإرجاع:';
$_['entry_opened']         = 'مفتوح:';
$_['entry_comment']        = 'التعليق:';
$_['entry_return_status']  = 'حالة الإرجاع:';
$_['entry_notify']         = 'تنبيه العميل:';
$_['entry_action']         = 'إجراء الإرجاع:';

// Error
$_['error_warning']        = 'يرجى التحقق من الحقول جيداً!';
$_['error_permission']     = 'لا توجد لديك صلاحيات التعديل على إرجاع المنتجات!';
$_['error_firstname']      = 'الاسم يجب أن يكون أكبر من 3 وأقل من 32 حرفا!';
$_['error_lastname']       = 'اسم العائلة يجب أن يكون أكبر من 3 وأقل من 32 حرفا!';
$_['error_email']          = 'يبدو أن البريد الالكتروني غير صحيح!';
$_['error_telephone']      = 'رقم الهاتف يجب أن يكون أكبر من 3 وأقل من 32 رقما!';
$_['error_password']       = 'كلمة المرور يجب أن تكون أكبر من 3 وأقل من 20 حرفا!';
$_['error_confirm']        = 'كلمة المرور وتأكيد كلمة المرور لا يتطابقان!';
$_['error_address_1']      = 'العنوان يجب أن يكون أكبر من 3 وأقل من 128 حرفا!';
$_['error_city']           = 'اسم المدينة يجب أن يكون أكبر من 3 وأقل من 128 حرفا!';
$_['error_postcode']       = 'يجب أن يكون عنوان صندوق البريد من 2 إلى 10 أرقام لهذا البلد !';
$_['error_country']        = 'يرجى تحديد الدولة!';
$_['error_zone']           = 'يرجى تحديد المنطقة أو المحافظة!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>