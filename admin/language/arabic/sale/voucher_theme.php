<?php
// Heading
$_['heading_title']      = 'شكل القسيمة';

// Text
$_['text_success']       = 'تم تعديل شكل القسيمة بنجاح!';
$_['text_image_manager'] = 'إدارة الصور';

// Column
$_['column_name']        = 'اسم شكل القسيمة';
$_['column_action']      = 'الإجراء';

// Entry
$_['entry_name']         = 'اسم شكل القسيمة:';
$_['entry_description']  = 'وصف شكل القسيمة:';
$_['entry_image']        = 'الصورة:';

// Error
$_['error_permission']   = 'لا توجد لديك صلاحيات التعديل على شكل القسيمة!';
$_['error_name']         = 'اسم شكل القسيمة يجب أن يكون ما بين 3 حتى 32 حرفا!';
$_['error_image']        = 'الصورة مطلوبة!';
$_['error_voucher']      = 'لا يمكن حذف شكل القسيمة لان قد تم تعيينه على القسائم التالية %s!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>