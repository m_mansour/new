<?php  
// Heading
$_['heading_title']        = 'النشرة البريدية';

// Text
$_['text_success']         = 'تم إرسال الرسالة بنجاح!';
$_['text_default']         = 'الافتراضي';
$_['text_newsletter']      = 'جميع المشتركين في النشرة الإخبارية';
$_['text_customer_all']    = 'كل العملاء';
$_['text_customer_group']  = 'مجموعة العملاء';
$_['text_customer']        = 'العملاء';
$_['text_affiliate_all']   = 'كل المروجين';
$_['text_affiliate']       = 'المروجين';
$_['text_product']         = 'المنتجات';

// Entry
$_['entry_store']          = 'من:';
$_['entry_to']             = 'إلى:';
$_['entry_customer_group'] = 'مجموعة العملاء:';
$_['entry_customer']       = 'العميل:';
$_['entry_affiliate']      = 'المروجين:';
$_['entry_product']        = 'المنتجات:<br /><span class="help">ترسل فقط للعملاء الذين طلب منتجات في القائمة.</span>';
$_['entry_subject']        = 'الموضوع:';
$_['entry_message']        = 'نص الرسالة:';

// Error
$_['error_permission']     = 'لا يوجد لديك الصلاحية إرسال النشرة البريدية!';
$_['error_subject']        = 'موضوع الرسالة مطلوب!';
$_['error_message']        = 'نص الرسالة مطلوب!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>