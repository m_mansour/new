<?php
// Heading
$_['heading_title']      = 'تغذية المنتجات';

// Text
$_['text_install']       = 'تثبيت';
$_['text_uninstall']     = 'إلغاء';

// Column
$_['column_name']        = 'اسم تغذية المنتج';
$_['column_status']      = 'الحالة';
$_['column_action']      = 'الإجراء';

// Error
$_['error_permission']   = 'لا يوجد لديك صلاحيات التعديل على تغذية المنتجات!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>