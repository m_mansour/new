<?php
// Heading
$_['heading_title']     = 'نظام الطلبات';

// Text
$_['text_install']      = 'تثبيت';
$_['text_uninstall']    = 'إلغاء';

// Column
$_['column_name']       = 'نظام الطلبات';
$_['column_status']     = 'الحالة';
$_['column_sort_order'] = 'ترتيب العرض';
$_['column_action']     = 'الخيارات';

// Error
$_['error_permission']  = 'لا يوجد لديك صلاحيات التعديل على نظام الطلبات!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>