<?php
// Heading
$_['heading_title']      = 'الإضافات';

// Text
$_['text_install']       = 'تثبيت';
$_['text_uninstall']     = 'إلغاء';

// Column
$_['column_name']        = 'اسم الإضافة';
$_['column_action']      = 'الإجراء';

// Error
$_['error_permission']   = 'لا يوجد لديك صلاحيات التعديل على الإضافات!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>