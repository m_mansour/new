<?php
// Heading
$_['heading_title']      = 'الشحن';

// Text
$_['text_install']       = 'تثبيت';
$_['text_uninstall']     = 'إلغاء';

// Column
$_['column_name']        = 'طريقة الشحن';
$_['column_status']      = 'الحالة';
$_['column_sort_order']  = 'ترتيب العرض';
$_['column_action']      = 'الإجراء';

// Error
$_['error_permission']   = 'لا يوجد لديك صلاحيات التعديل على الشحن!';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>