<?php
// Heading					
$_['heading_title']		      = 'PaymobCC';

// Text 					
$_['text_payment']		      = 'Payment';
$_['text_success']		      = 'Success: You have modified the PaymobCC details.';
$_['text_moneybookers']	      = '<img src="http://www.magentocommerce.com/magento-connect/media/catalog/product/cache/9/image/468x300/9df78eab33525d08d6e5fb8d27136e95/p/a/payfort_logo_2_1.png" alt="Payfort" title="Payfort" style="border: 1px solid #EEEEEE;" /></a>';
	
// Entry					
$_['entry_email']		      = 'E-Mail:';
$_['entry_secret']		      = 'Secret:';
$_['entry_total']             = 'Total:<br /><span class="help">The checkout total the order must reach before this payment method becomes active.</span>';
$_['entry_order_status']      = 'Order Status:';
$_['entry_pending_status']    = 'Pending Status :';
$_['entry_canceled_status']   = 'Canceled Status:';
$_['entry_failed_status']     = 'Failed Status:';
$_['entry_chargeback_status'] = 'Chargeback Status:';
$_['entry_geo_zone']          = 'Geo Zone:';
$_['entry_status']            = 'Status:';
$_['entry_sort_order']        = 'Sort Order:';

// Error					
$_['error_permission']	      = 'Warning: You do not have permission to modify PaymobCC!'; 
$_['error_email']		      = 'E-Mail Required!';
?>