<?php
// Heading
$_['heading_title']       = 'Static Banners';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module static banners!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';

// Entry
$_['entry_banner']        = 'Banner:';
$_['entry_dimension']     = 'Dimension (W x H) and Resize Type:';
$_['entry_layout']        = 'Layout:';
$_['entry_position']      = 'Position:';
$_['entry_status']        = 'Status:';
$_['entry_sort_order']    = 'Sort Order:';
$_['entry_show']           = 'Show:';
$_['text_show_random']    = 'Random image';
$_['text_show_all']       = 'All images (in order)';
$_['text_show_allrandom'] = 'All images (shuffle)';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module static banners!';
$_['error_dimension']     = 'Width &amp; Height dimensions required!';
?>