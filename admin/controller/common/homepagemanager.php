<?php   
class ControllerCommonHomepagemanager extends Controller {   
	public function index() {
    	$this->load->language('common/home');
	 
//		$this->document->setTitle($this->language->get('heading_title'));
		
    	$data['heading_title'] = $this->language->get('heading_title');

		
		$this->template = 'common/homepagemanager.tpl';

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

//		$this->children = array(
//			'common/header',
//			'common/footer'
//		);

		$this->response->setOutput($this->load->view('common/homepagemanager.tpl', $data));


  	}
 }
	
