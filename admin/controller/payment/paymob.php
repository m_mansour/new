<?php
class ControllerPaymentPaymob extends Controller {
	private $error = array(); 
	
	public function index() {
		$this->language->load('payment/paymob');
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
			
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('paymob', $this->request->post);				
			
			$this->session->data['success'] = $this->language->get('text_success');
		
			$this->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
		}
		
		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_all_zones'] = $this->language->get('text_all_zones');
				
		$this->data['entry_email'] = $this->language->get('entry_email');
		$this->data['entry_total'] = $this->language->get('entry_total');	
		$this->data['entry_order_status'] = $this->language->get('entry_order_status');	
		$this->data['entry_pending_status'] = $this->language->get('entry_pending_status');	
		$this->data['entry_canceled_status'] = $this->language->get('entry_canceled_status');	
		$this->data['entry_failed_status'] = $this->language->get('entry_failed_status');	
		$this->data['entry_chargeback_status'] = $this->language->get('entry_chargeback_status');	
		$this->data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_mb_id'] = $this->language->get('entry_mb_id');
		$this->data['entry_secret'] = $this->language->get('entry_secret');
		$this->data['entry_custnote'] = $this->language->get('entry_custnote');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
 		
		if (isset($this->error['email'])) {
			$this->data['error_email'] = $this->error['email'];
		} else {
			$this->data['error_email'] = '';
		}
		
  		$this->data['breadcrumbs'] = array();
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_payment'),
			'href'      => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'),      		
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('payment/paymob', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
				
		$this->data['action'] = $this->url->link('payment/paymob', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');
		
		if (isset($this->request->post['paymob_email'])) {
			$this->data['paymob_email'] = $this->request->post['paymob_email'];
		} else {
			$this->data['paymob_email'] = $this->config->get('paymob_email');
		}
		
		if (isset($this->request->post['paymob_secret'])) {
			$this->data['paymob_secret'] = $this->request->post['paymob_secret'];
		} else {
			$this->data['paymob_secret'] = $this->config->get('paymob_secret');
		}
		
		if (isset($this->request->post['paymob_total'])) {
			$this->data['paymob_total'] = $this->request->post['paymob_total'];
		} else {
			$this->data['paymob_total'] = $this->config->get('paymob_total'); 
		} 
				
		if (isset($this->request->post['paymob_order_status_id'])) {
			$this->data['paymob_order_status_id'] = $this->request->post['paymob_order_status_id'];
		} else {
			$this->data['paymob_order_status_id'] = $this->config->get('paymob_order_status_id'); 
		} 

		if (isset($this->request->post['paymob_paymentmade_status_id'])) {
			$this->data['paymob_paymentmade_status_id'] = $this->request->post['paymob_paymentmade_status_id'];
		} else {
			$this->data['paymob_paymentmade_status_id'] = $this->config->get('paymob_paymentmade_status_id'); 
		} 

		if (isset($this->request->post['paymob_pending_status_id'])) {
			$this->data['paymob_pending_status_id'] = $this->request->post['paymob_pending_status_id'];
		} else {
			$this->data['paymob_pending_status_id'] = $this->config->get('paymob_pending_status_id');
		}

		if (isset($this->request->post['paymob_canceled_status_id'])) {
			$this->data['paymob_canceled_status_id'] = $this->request->post['paymob_canceled_status_id'];
		} else {
			$this->data['paymob_canceled_status_id'] = $this->config->get('paymob_canceled_status_id');
		}

		if (isset($this->request->post['paymob_failed_status_id'])) {
			$this->data['paymob_failed_status_id'] = $this->request->post['paymob_failed_status_id'];
		} else {
			$this->data['paymob_failed_status_id'] = $this->config->get('paymob_failed_status_id');
		}

		if (isset($this->request->post['paymob_chargeback_status_id'])) {
			$this->data['paymob_chargeback_status_id'] = $this->request->post['paymob_chargeback_status_id'];
		} else {
			$this->data['paymob_chargeback_status_id'] = $this->config->get('paymob_chargeback_status_id');
		}
		
		$this->load->model('localisation/order_status');
		
		$this->data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		
		if (isset($this->request->post['paymob_geo_zone_id'])) {
			$this->data['paymob_geo_zone_id'] = $this->request->post['paymob_geo_zone_id'];
		} else {
			$this->data['paymob_geo_zone_id'] = $this->config->get('paymob_geo_zone_id'); 
		} 	
		
		$this->load->model('localisation/geo_zone');
										
		$this->data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();
		
		if (isset($this->request->post['paymob_status'])) {
			$this->data['paymob_status'] = $this->request->post['paymob_status'];
		} else {
			$this->data['paymob_status'] = $this->config->get('paymob_status');
		}
		
		if (isset($this->request->post['paymob_sort_order'])) {
			$this->data['paymob_sort_order'] = $this->request->post['paymob_sort_order'];
		} else {
			$this->data['paymob_sort_order'] = $this->config->get('paymob_sort_order');
		}
		
		if (isset($this->request->post['paymob_rid'])) {
			$this->data['paymob_rid'] = $this->request->post['paymob_rid'];
		} else {
			$this->data['paymob_rid'] = $this->config->get('paymob_rid');
		}
		
		if (isset($this->request->post['paymob_custnote'])) {
			$this->data['paymob_custnote'] = $this->request->post['paymob_custnote'];
		} else {
			$this->data['paymob_custnote'] = $this->config->get('paymob_custnote');
		}

		$this->template = 'payment/paymob.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'payment/paymob')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->request->post['paymob_email']) {
			$this->error['email'] = $this->language->get('error_email');
		}
				
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>