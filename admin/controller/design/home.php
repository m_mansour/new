<?php   
class ControllerDesignHome extends Controller {   
	public function index() {
    	//$this->load->language('common/home');
		$this->document->setTitle($this->language->get('heading_title'));
    	$this->data['heading_title'] = $this->language->get('heading_title');
		$this->template = 'design/home.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
  	}
 }
	
