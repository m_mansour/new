<?php
class ControllerVendorsVendors extends Controller {
	public function index() {     

		$this->document->setTitle("Vendors");
		$data['heading_title'] = "Vendors";

		
		$this->load->model('vendors/vendors');
		$this->load->model('localisation/country');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		$url = '';


		$filter_data = array(

			'start'           => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'           => $this->config->get('config_limit_admin')
		);

		$vendors_from_model = $this->model_vendors_vendors->getVendors($filter_data);
		foreach($vendors_from_model as $vendor){
			$data['vendors'][] = array(
				'vendor_id' => $vendor['vendor_id'],
				'vendor_name' => $vendor['vendor_name'],
				'vendor_email' => $vendor['vendor_email'],
				'vendor_location' => $vendor['vendor_location'],
				'vendor_location_name' => $this->model_localisation_country->getCountry($vendor['vendor_location'])['name'],

				'edit_link' => $this->url->link('vendors/vendors/edit', 'token=' . $this->session->data['token'] . '&vendor_id=' .$vendor['vendor_id'] , 'SSL')
				);
		}
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_add'] = $this->language->get('button_add');
		$data['insert_url'] = $this->url->link('vendors/vendors/add', 'token=' . $this->session->data['token'], 'SSL');
        $data['delete_url'] = $this->url->link('vendors/vendors/delete', '&token=' . $this->session->data['token'] . $url, 'SSL');
		
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Vendors',
			'href' => $this->url->link('vendors/vendors', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);


		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}



		$vendor_total = $this->model_vendors_vendors->getTotalVendors();


		$pagination = new Pagination();
		$pagination->total = $vendor_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('vendors/vendors', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($vendor_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($vendor_total - $this->config->get('config_limit_admin'))) ? $vendor_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $vendor_total, ceil($vendor_total / $this->config->get('config_limit_admin')));


		//$this->template = 'vendors/vendors.tpl';

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('vendors/vendors.tpl', $data));
	}

	public function add(){
		$this->document->setTitle("Add A Vendor");

		$data['heading_title'] = "Add A Vendor";

		$this->load->model('localisation/country');
				$data['countries'] = $this->model_localisation_country->getCountries();
//		print_r( $data['countries'] );

		$this->load->model('vendors/vendors');
		if (($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateInsert() ) ){
            $url = '';
			$this->model_vendors_vendors->addVendor($this->request->post);
            $this->response->redirect($this->url->link('vendors/vendors', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$url= '';

			$data['action'] = $this->url->link('vendors/vendors/add', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Vendors',
			'href' => $this->url->link('vendors/vendors', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		$data['button_cancel']= "Cancel";
		$data['button_save']= "Save";

		$data['cancel'] = $this->url->link('vendors/vendors', 'token=' . $this->session->data['token'] . $url, 'SSL');


		//$this->template = 'vendors/vendors_add.tpl';

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('vendors/vendors_add.tpl', $data));
	}
	public function edit(){
		$this->document->setTitle("Edit A Vendor");
		$data['heading_title'] = "Edit A Vendor";


		$this->load->model('vendors/vendors');
		$this->load->model('localisation/country');
				$data['countries'] = $this->model_localisation_country->getCountries();
		if(!empty($this->request->get['vendor_id']))
		{
			$vendor= $this->model_vendors_vendors->getVendor($this->request->get['vendor_id']);
			$data['vendor_id'] = $this->request->get['vendor_id'];
			$data['vendor_name'] = $vendor['vendor_name'];
			$data['vendor_email'] = $vendor['vendor_email'];

			$data['vendor_location'] = $vendor['vendor_location'];
		}
		$url= '';

		if (!isset($this->request->get['vendor_id'])) {
			$data['action'] = $this->url->link('vendors/vendors/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$data['action'] = $this->url->link('vendors/vendors/edit', 'token=' . $this->session->data['token'] . '&vendor_id=' . $this->request->get['vendor_id'] . $url, 'SSL');
		}
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Vendors',
			'href' => $this->url->link('vendors/vendors', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		$data['button_cancel']= "Cancel";
		$data['button_save']= "Save";

		$data['cancel'] = $this->url->link('vendors/vendors', 'token=' . $this->session->data['token'] . $url, 'SSL');


		if (($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateInsert() ) ){
            $url= '';
			$this->model_vendors_vendors->editVendor($this->request->post);
            $this->response->redirect($this->url->link('vendors/vendors', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
//		$data['delete_url'] = $this->url->link('vendors/vendors/delete', 'vendor_id='.$this->request->get['vendor_id']. '&token=' . $this->session->data['token'] . $url, 'SSL');
		//$this->template = 'vendors/vendors_edit.tpl';

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('vendors/vendors_edit.tpl', $data));
	}
	public function delete(){

		$this->load->model('vendors/vendors');


		if (isset($this->request->post['selected'])) {
			foreach ($this->request->post['selected'] as $vendor_id) {
				$this->model_vendors_vendors->deleteVendor($vendor_id);

			}

			"Vendors deleted successfully";

			$this->session->data['success'] = "Vendors deleted successfully";

			$url ='';
			$this->response->redirect($this->url->link('vendors/vendors', 'token=' . $this->session->data['token'] . $url, 'SSL'));


		}

	}
	private function validateInsert(){
		return true;
	}
	
}
?>