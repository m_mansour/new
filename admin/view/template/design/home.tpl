<?php echo $header; ?>
<div id="content">
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/review.png" alt="" /> Add A Vendor</h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button">Save</a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
          <tr>
            <td>Name</td>
            <td><input type="text" name="name" value="<?php echo $name; ?>" />
          </tr>
          <tr>
            <td>E-Mail</td>
            <td><input type="text"  name="email" value="<?php echo $email; ?>" />
          </tr>
          <tr>
            <td>Password</td>
            <td><input type="password"  name="password" value="<?php echo $password; ?>" />
          </tr>
        </table>
      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?>