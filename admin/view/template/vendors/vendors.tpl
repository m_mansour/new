<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">

    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right"><a href="<?php echo $insert_url; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('are you sure?') ? $('#form-vendor').submit() : false;"><i class="fa fa-trash-o"></i></button>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>

    <div class="container-fluid">
            <?php if ($error_warning) { ?>
            <div class="warning"><?php echo $error_warning; ?></div>
            <?php } ?>
            <?php if ($success) { ?>
            <div class="success"><?php echo $success; ?></div>
            <?php } ?>
	        <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Vendors</h3>
                    <div class="buttons">
                        <a href="<?php echo $insert_url; ?>" class="button">Add A Vendor</a>
                    </div>
                </div>

                 <div class="panel-body">
            <form action="<?php echo $delete_url; ?>" method="post" enctype="multipart/form-data" id="form-vendor">
		        <div class="table-responsive">
		        	<table class="table table-bordered table-hover">
				<thead>
					<tr>
                        <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>

                        <td class="left">Vendor ID</td>
						<td class="left">Vendor Name</td>
						<td class="left">Vendor Email</td>
						<td class="left">Vendor Location</td>
						<td class="left">Modify</td>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($vendors as $vendor) { ?>

					<tr>
                        <td class="text-center"><?php if (in_array($vendor['vendor_id'], $selected)) { ?>
                                <input type="checkbox" name="selected[]" value="<?php echo $vendor['vendor_id']; ?>" checked="checked" />
                            <?php } else { ?>
                                <input type="checkbox" name="selected[]" value="<?php echo $vendor['vendor_id']; ?>" />
                            <?php } ?>
                        </td>
						<td><?php echo $vendor['vendor_id'] ;?> </td>
						<td><?php echo $vendor['vendor_name'];?> </td>
						<td><a href="mailto:<?php echo $vendor['vendor_email']; ?>"><?php echo $vendor['vendor_email']; ?></a></td>
						<td><?php echo $vendor['vendor_location_name']; ?></td>
						<td><a href="<?php echo $vendor['edit_link'];?>">Edit</a></td>
						<?php } ?>
					</tr>
				</tbody>
			</table>


		        </div>
            </form>
            <div class="row">
                <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                <div class="col-sm-6 text-right"><?php echo $results; ?></div>
            </div>
        </div>
             </div>
    </div>

	</div>
</div> 
<?php echo $footer; ?>