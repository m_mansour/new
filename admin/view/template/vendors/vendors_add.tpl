<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">

    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-vendor" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>

    <div class="container-fluid">

          <?php if ($error_warning) { ?>
          <div class="warning"><?php echo $error_warning; ?></div>
          <?php } ?>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><img src="view/image/review.png" alt="" /> Add A Vendor</h3>
            </div>
            <div class="panel-body">
              <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-vendor" class="form-horizontal">


                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-name">Name</label>
                            <div class="col-sm-10">
                                <input type="text" name="name"  placeholder="" id="input-name" class="form-control" />

                            </div>
                        </div>
                        <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-email">Email</label>
                                <div class="col-sm-10">
                                    <input type="text" name="email"  placeholder="" id="input-email" class="form-control" />

                                </div>
                          </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-pass">Password</label>
                            <div class="col-sm-10">
                                <input type="password" name="password"  placeholder="" id="input-pass" class="form-control" />

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="input-location">Location</label>
                            <div class="col-sm-10">
                                <select data-placeholder="Choose a Country..." name="location" id="input-location" >
                                    <?php foreach($countries as $country){?>
                                        <option value="<?php echo $country['country_id'];?>"><?php echo $country['name'];?></option>
                                    <?php }?>

                                </select>
                            </div>
                        </div>

              </form>
            </div>
          </div>
    </div>
</div>
<?php echo $footer; ?>