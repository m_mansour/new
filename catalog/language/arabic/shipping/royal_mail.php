<?php
// Text
$_['text_title']                = 'Royal Mail';
$_['text_weight']               = 'الوزن:';
$_['text_insurance']            = 'التامين حتى:';
$_['text_eta']                  = 'الوقت المقدر:';
$_['text_1st_class_standard']   = 'الدرجة الأولى العادية للبريد';
$_['text_1st_class_recorded']   = 'الدرجة الأولى المسجلة للبريد';
$_['text_2nd_class_standard']   = 'الدرجة الثانية العادية للبريد';
$_['text_2nd_class_recorded']   = 'الدرجة الثانية المسجلة للبريد';
$_['text_standard_parcels']     = 'معيار الطرود';
$_['text_airmail']              = 'البريد الجوي';
$_['text_international_signed'] = 'التوقيع الدولي';
$_['text_airsure']              = 'التأكيد الجوي';
$_['text_surface']              = 'البري';

// ####################### Translation By AlfnyHost Team ###########################
// ####################### Website: WWW.alfnyhost.com ##############################
// ####################### E-mail: support@alfnyhost.com ###########################
?>