<?php
// Heading 
$_['heading_title'] = 'Shopping Bag';

// Text
$_['text_items']    = '(%s) - %s';
$_['text_empty']    = 'Unfortunately, your shopping bag is empty.';
$_['text_cart']     = 'View Cart';
$_['text_checkout'] = 'Proceed to checkout';
?>