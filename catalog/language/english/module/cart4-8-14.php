<?php
// Heading 
$_['heading_title'] = 'CART';

// Text
$_['text_items']    = '(%s) - %s';
$_['text_empty']    = 'Your shopping cart is empty';
$_['text_cart']     = 'View Cart';
$_['text_checkout'] = 'Checkout';
?>