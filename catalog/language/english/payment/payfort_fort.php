<?php

// Text
$_['text_title']                 = 'Credit Card';
$_['text_wait']                  = 'Please wait';
$_['text_credit_card']           = 'Credit Card details';
$_['text_loading']               = 'Loading';
$_['text_sadad']                 = 'سداد';
$_['text_naps']                  = 'Tabs';
$_['text_payment_failed']        = 'Error: the payment process fails, please check the payment details and try again, Payfort message: ';
$_['text_payment_canceled']      = 'you have canceled the payment, please try agian.';
$_['text_p_success']             = 'Success';
$_['text_save_credit_card_note'] = 'Save Card Info';
$_['heading_success_title']      = 'The payment process Has completed successfully';

$_['text_success_customer']   = '<p>ORDER COMPLETE</p><p>We’ve sent you an email with the confirmation. Thank you for shopping at Coterique.</p>';
$_['text_success_guest']      = '<p>ORDER COMPLETE</p><p>We’ve sent you an email with the confirmation. Thank you for shopping at Coterique.</p>';
$_['text_general_error']      = 'System error, please try again later';
$_['text_error_card_decline'] = 'The card was rejected, please use another card';
$_['text_secure_connection']  = 'Creating a secure connection...';
$_['error_connection']        = 'Could not connect to PayFort. Please contact the shop\'s administrator for assistance or choose a different payment method.';

