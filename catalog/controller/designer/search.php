<?php
class ControllerDesignerSearch extends Controller {
	public function index() {

		$this->load->model('catalog/category');

		$this->load->model('tool/image');

		if (isset($this->request->get['search'])) {
			$search = $this->request->get['search'];
		} else {
			$search = '';
		}



		if (isset($this->request->get['description'])) {
			$description = $this->request->get['description'];
		} else {
			$description = '';
		}

		if (isset($this->request->get['category_id'])) {
			$category_id = $this->request->get['category_id'];
		} else {
			$category_id = 0;
		}

		if (isset($this->request->get['search'])) {
			$this->document->setTitle('Search - ' . $this->request->get['search']);
		} elseif (isset($this->request->get['tag'])) {
			$this->document->setTitle($this->language->get('heading_title') .  ' - ' . $this->language->get('heading_tag') . $this->request->get['tag']);
		} else {
			$this->document->setTitle($this->language->get('heading_title'));
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$url = '';

		if (isset($this->request->get['search'])) {
			$url .= '&search=' . urlencode(html_entity_decode($this->request->get['search'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['description'])) {
			$url .= '&description=' . $this->request->get['description'];
		}

		if (isset($this->request->get['category_id'])) {
			$url .= '&category_id=' . $this->request->get['category_id'];
		}


		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('product/search', $url)
		);

		if (isset($this->request->get['search'])) {
			$data['heading_title'] = $this->language->get('heading_title') .  ' - ' . $this->request->get['search'];
		} else {
			$data['heading_title'] = $this->language->get('heading_title');
		}



		$this->load->model('catalog/category');


		$data['categories'] = array();

		if (isset($this->request->get['search'])) {
			
			//$product_total = $this->model_catalog_product->getTotalProducts($filter_data);

			//$results = $this->model_catalog_product->getProducts($filter_data);
			$results = $this->model_catalog_category->getDesigners($search);

			foreach ($results as $result) {


				$data['categories'][] = array(
					'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '_' . $result['category_id'] . $url),
					'category_id' => $result['category_id'],
					'name'        => $result['name'],
					'p_thumb' => $result['p_image'],
					'thumb' => $result['image'],
					'country_id'=> $result['country_id'],
					'zone_id'=> $result['zone_id']
				);
			}

			$this->load->model('localisation/country');
			$data['countries'] = $this->model_localisation_country->getCountries();
			$this->load->model('localisation/zone');
			$data['zones'] = $this->model_localisation_zone->getZones();

			$url = '';

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . urlencode(html_entity_decode($this->request->get['search'], ENT_QUOTES, 'UTF-8'));
			}


			$url = '';

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . urlencode(html_entity_decode($this->request->get['search'], ENT_QUOTES, 'UTF-8'));
			}



			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}





			$url = '';

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . urlencode(html_entity_decode($this->request->get['search'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . urlencode(html_entity_decode($this->request->get['tag'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}






		}

		$data['search'] = $search;
		$data['description'] = $description;
		$data['category_id'] = $category_id;

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/designer/search.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/designer/search.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/product/search.tpl', $data));
		}
	}
}
