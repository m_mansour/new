<?php
class ControllerAmbassadorListing extends Controller {

 public function index(){
     $this->document->setTitle("CotiGirls");

     if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/ambassador/listing.tpl')) {
         $this->template = $this->config->get('config_template') . '/template/ambassador/listing.tpl';
     } else {
         $this->template = 'default/template/ambassador/listing.tpl';
     }


     $this->load->model('catalog/category');
     $this->load->model('localisation/country');
     $this->load->model('localisation/zone');
     $data['countries'] = $this->model_localisation_country->getCountries();
     $data['zones'] = $this->model_localisation_zone->getZones();
     //var_dump($data['zones']);

     $data['categories'] = array();

     $categories=$this->model_catalog_category->getCategories(478);
     //var_dump($categories);
     //exit;

     $this->load->model('tool/image');
     //$category_info = $this->model_catalog_category->getCategory($path_id);


     if (isset($this->request->get['path'])) {
         $url = '';

         if (isset($this->request->get['sort'])) {
             $url .= '&sort=' . $this->request->get['sort'];
         }

         if (isset($this->request->get['order'])) {
             $url .= '&order=' . $this->request->get['order'];
         }

         if (isset($this->request->get['limit'])) {
             $url .= '&limit=' . $this->request->get['limit'];
         }

         $path = '';

         $parts = explode('_', (string)$this->request->get['path']);

         $category_id = (int)array_pop($parts);

         foreach ($parts as $path_id) {
             if (!$path) {
                 $path = (int)$path_id;
             } else {
                 $path .= '_' . (int)$path_id;
             }

             $category_info = $this->model_catalog_category->getCategory($path_id);


             if ($category_info) {
                 $data['breadcrumbs'][] = array(
                     'text' => $category_info['name'],
                     'href' => $this->url->link('product/category', 'path=' . $path . $url)
                 );
             }
         }
     } else {
         $category_id = 0;
     }
     if ($category_info['p_image']) {
         $ee = $this->model_tool_image->resize($category_info['p_image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
     } else {
         $data['p_thumb'] = '';
     }

$results = $this->model_catalog_category->getCategories(478);



     //$image = empty($category['image']) ? 'no_image.jpg' : $category['image'];
     //$thumb = $this->model_tool_image->resize($image, 100, 100);



     foreach ($results as $result) {
         $filter_data = array(
             'filter_category_id'  => $result['category_id'],
             'filter_sub_category' => false
         );
         $url = '';

         $data['categories'][] = array(
             'href' => $this->url->link('product/category', 'path=_' . $result['category_id'] . $url),
             'category_id' => $result['category_id'],
             'name'        => $result['name'],
             'job_title'        => $result['job_title'],
             'insta_url'        => $result['insta_url'],
             't_url'        => $result['t_url'],
             'p_url'        => $result['p_url'],
             'w_url'        => $result['w_url'],
             'p_thumb' => $result['p_image'],
             'country_id'=> $result['country_id'],
             'zone_id'=> $result['zone_id']

         );
       //  $data['zones'] = $this->model_localisation_zone->getZonesByCountryId($result['country_id']);


        // $country_info = $this->model_localisation_country->getCountry($result['country_id']);
        // $data['country_name'] =$country_info['name'];
       // var_dump($categories['country_name']);
       //  exit;
       //  $zone_info = $this->model_localisation_zone->getZone($result['zone_id']);
         //$data['zone_name'] =$zone_info['name'];
     }








     $data['column_left'] = $this->load->controller('common/column_left');
     $data['column_right'] = $this->load->controller('common/column_right');
     $data['content_top'] = $this->load->controller('common/content_top');
     $data['content_bottom'] = $this->load->controller('common/content_bottom');
     $data['footer'] = $this->load->controller('common/footer');
     $data['header'] = $this->load->controller('common/header');





     // call the "View" to render the output

     $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/ambassador/listing.tpl', $data));

 }
}