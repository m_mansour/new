<?php
class ControllerPaymentWallety extends Controller {
	protected function index() {
		
		$this->data['button_confirm'] = $this->language->get('button_confirm');
 
		$this->data['action'] = 'https://www.wallety.com/checkout/checkout';
 
		$this->load->model('checkout/order');
 
		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
		
		if ($order_info) {
			
			$this->data['merchantid'] = '200';
			$this->data['orderid'] = $this->session->data['order_id'];
			//$this->data['browserbackurl'] = $this->url->link('payment/wallety/callback');
			$this->data['browserbackurl'] = 'http://demo.coterique.com//mypay_callback.php';
			$this->data['orderamount'] = $order_info['total'] * 100;
 
			$cert = '4C948068C0F03E32A0113F3E29022562';
 
			$strSource = $cert . $this->data['orderamount'] . 'wallety' . $this->data['merchantid'] . $this->data['orderid'] .
			$this->data['browserbackurl'];
		
			$signature = strtoupper(md5($strSource));
			$this->data['signature'] = $signature;
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/wallety.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/payment/wallety.tpl';
			} else {
				$this->template = 'default/template/payment/wallety.tpl';
			}	
			
			$this->render();
		}
	}
	
	public function callback() {
		$this->load->language('payment/wallety');
		
		
		
		if (isset($this->request->get['merchinvno'])) {
			$order_id = $this->request->get['merchinvno'];
		} else {
			$order_id = 0;
			//$this->redirect($this->url->link('common/home'));
		}
		
		$this->load->model('checkout/order');
				
		$order_info = $this->model_checkout_order->getOrder($order_id);	 	
		
		if ($order_info) {
			$error = '';
			
			if (!isset($this->request->get['response'])) {
				$error = $this->language->get('text_no_reponse');
			} elseif ($this->request->get['response'] != 0) {
				$error = $this->language->get('text_declined');
			}
		} else {
			$error = $this->language->get('text_unable');
		}	

		if ($error) {
			$this->data['breadcrumbs'] = array();

			$this->data['breadcrumbs'][] = array(
				'href'      => $this->url->link('common/home'),
				'text'      => $this->language->get('text_home'),
				'separator' => false
			);
			$this->data['breadcrumbs'][] = array(
				'href'      => $this->url->link('checkout/cart'),
				'text'      => $this->language->get('text_basket'),
				'separator' => $this->language->get('text_separator')
			);
			$this->data['breadcrumbs'][] = array(
				'href'      => $this->url->link('checkout/checkout', '', 'SSL'),
				'text'      => $this->language->get('text_checkout'),
				'separator' => $this->language->get('text_separator')
			);
			$this->data['breadcrumbs'][] = array(
				'href'      => $this->url->link('checkout/success'),
				'text'      => $this->language->get('text_failed'),
				'separator' => $this->language->get('text_separator')
			);		
			$this->data['heading_title'] = $this->language->get('text_failed');
			$this->data['text_message'] = sprintf($this->language->get('text_failed_message'), $error, $this->url->link('information/contact'));
			$this->data['button_continue'] = $this->language->get('button_continue');
			$this->data['continue'] = $this->url->link('common/home');
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/success.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/common/success.tpl';
			} else {
				$this->template = 'default/template/common/success.tpl';
			}
			
			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'
			);
			$this->response->setOutput($this->render());
		} else {
			$this->model_checkout_order->confirm($order_id, 'Complete');
			$this->redirect($this->url->link('checkout/success'));			
		}		
	}
	/*
	public function callback() {
	
		if (isset($this->request->post['response'])) {
			$response_id = $this->request->post['response'];
		} else {
			$response_id = 1;
		}
		
		$this->load->model('checkout/order');
		
		if (isset($this->request->post['merchinvno'])) {
			$order_id = $this->request->post['merchinvno'];
		} else {
			$order_id = 0;
		}
		
		$order_info = $this->model_checkout_order->getOrder($order_id);

		if ($order_info) {
			$this->model_checkout_order->confirm($order_id, 'Complete');//$this->config->get('wallety_order_status_id'));
			
			if ($response_id != 0) {
				$this->model_checkout_order->update($order_id, 'Failed', '', true);
			}
		}
	}*/
}
?>