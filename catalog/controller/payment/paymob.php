<?php
class ControllerPaymentPaymob extends Controller {
	protected function index() {
		$this->load->model('checkout/order');
		
		$this->language->load('payment/moneybookers');
		
    	$this->data['button_confirm'] = "Confirm";
        
		$this->data['action'] = 'https://www.moneybookers.com/app/payment.pl?p=OpenCart';

		$this->data['pay_to_email'] = $this->config->get('paymob_email');
		$this->data['platform'] = 'opencart';
		$this->data['description'] = $this->config->get('config_name');
		$this->data['transaction_id'] = $this->session->data['order_id'];
        $this->data['return_url'] = $this->url->link('checkout/success');
		$this->data['cancel_url'] = $this->url->link('checkout/checkout', '', 'SSL');
		$this->data['status_url'] = $this->url->link('payment/moneybookers/callback');
		$this->data['language'] = $this->session->data['language'];		
		$this->data['logo'] = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');
		
        $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
			
		$this->data['pay_from_email'] = $order_info['email'];
		$this->data['firstname'] = $order_info['payment_firstname'];
        $this->data['lastname'] = $order_info['payment_lastname'];
        $this->data['address'] = $order_info['payment_address_1'];
        $this->data['address2'] = $order_info['payment_address_2'];
        $this->data['phone_number'] = $order_info['telephone'];
		$this->data['postal_code'] = $order_info['payment_postcode'];
        $this->data['city'] = $order_info['payment_city'];
        $this->data['state'] = $order_info['payment_zone'];
		$this->data['country'] = $order_info['payment_iso_code_3'];
		$this->data['amount'] = $this->currency->format(intval($order_info['total']), $order_info['currency_code'], $order_info['currency_value'], false);
        $this->data['currency'] = $order_info['currency_code'];
		
		$products = '';
		
		foreach ($this->cart->getProducts() as $product) {
    		$products .= $product['quantity'] . ' x ' . $product['name'] . ', ';
    	}		
		
		$this->data['detail1_text'] = $products;
		
		$this->data['order_id'] = $this->session->data['order_id'];
		
		$sha_passphrase = "cotcotcot1234AA!";
		$sha_string = "AMOUNT=" . $this->data['amount'] . $sha_passphrase . "CURRENCY=" . $this->data['currency'] . $sha_passphrase;
		$sha_string .= "LANGUAGE=" . $this->data['language'] . $sha_passphrase . "ORDERID=" . $this->data['order_id'] . $sha_passphrase;
		$sha_string .= "PSPID=" . $this->data['pay_to_email'] . $sha_passphrase;

		//echo $sha_string;
		$sha_string = sha1($sha_string);
		//echo $sha_string;

		$this->data['sha'] = $sha_string;
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/paymob.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/payment/paymob.tpl';
		} else {
			$this->template = 'default/template/payment/paymob.tpl';
		}	
		
		$this->render();
	}
	/*public function t(){
		$this->load->model('checkout/order');
		$this->model_checkout_order->update(318,2, '', true);


	}*/
	public function callback() {


		$sha_passphrase = "cotcotcot1234AA!";

		$this->load->model('checkout/order');

		$results =json_decode(file_get_contents('php://input'), true);//print_r($_POST, true);
		
		file_put_contents('/ebs1/pm_log.txt',print_r( $results, true ), FILE_APPEND);

		$order_id = $results['merchant_order_id'];
		$order_info = $this->model_checkout_order->getOrder($order_id);
		file_put_contents('/ebs1/pm_log.txt',$order_info, FILE_APPEND);
		if ($order_info) {
			//file_put_contents('/ebs1/pm_log.txt','6aaaaaaaab eeehhhh', FILE_APPEND);

			$this->model_checkout_order->confirm($order_id, $this->config->get('config_order_status_id'));
			$verified = true;
			if ($verified) {
				switch($results['payment_status']) {
					case 'PAID':
						$this->model_checkout_order->update($order_id, 1, '', true);
						break;
					case 'UNPAID':
						file_put_contents('/ebs1/pm_log.txt','henaaaa', FILE_APPEND);

						$this->model_checkout_order->update($order_id, 2, '', true);
						break;					
					case 'VOID':
						$this->model_checkout_order->update($order_id, 16 , '', true);
						break;
					case 'CANCELLED':
						$this->model_checkout_order->update($order_id, 7, '', true);
						break;	
				}
			} else {
				$this->log->write('md5sig returned (' . $hash . ') does not match generated (' . $hash_created . '). Verify Manually. Current order state: ' . $this->config->get('config_order_status_id'));
			}
		}else
		{
			//no orderecho ';
		}


		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode(array('ack'=>'ok')));
	}
}
?>