<?php
class ControllerPaymentPayfort extends Controller {
	protected function index() {
		$this->load->model('checkout/order');
		
		$this->language->load('payment/moneybookers');
		
    	$this->data['button_confirm'] = "Confirm";
        
		$this->data['action'] = 'https://www.moneybookers.com/app/payment.pl?p=OpenCart';

		$this->data['pay_to_email'] = $this->config->get('payfort_email');
		$this->data['platform'] = 'opencart';
		$this->data['description'] = $this->config->get('config_name');
		$this->data['transaction_id'] = $this->session->data['order_id'];
        $this->data['return_url'] = $this->url->link('checkout/success');
		$this->data['cancel_url'] = $this->url->link('checkout/checkout', '', 'SSL');
		$this->data['status_url'] = $this->url->link('payment/moneybookers/callback');
		$this->data['language'] = $this->session->data['language'];		
		$this->data['logo'] = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');
		
        $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
			
		$this->data['pay_from_email'] = $order_info['email'];
		$this->data['firstname'] = $order_info['payment_firstname'];
        $this->data['lastname'] = $order_info['payment_lastname'];
        $this->data['address'] = $order_info['payment_address_1'];
        $this->data['address2'] = $order_info['payment_address_2'];
        $this->data['phone_number'] = $order_info['telephone'];
		$this->data['postal_code'] = $order_info['payment_postcode'];
        $this->data['city'] = $order_info['payment_city'];
        $this->data['state'] = $order_info['payment_zone'];
		$this->data['country'] = $order_info['payment_iso_code_3'];
		$this->data['amount'] = $this->currency->format(intval($order_info['total'])*100, $order_info['currency_code'], $order_info['currency_value'], false);
        $this->data['currency'] = $order_info['currency_code'];
		
		$products = '';
		
		foreach ($this->cart->getProducts() as $product) {
    		$products .= $product['quantity'] . ' x ' . $product['name'] . ', ';
    	}		
		
		$this->data['detail1_text'] = $products;
		
		$this->data['order_id'] = $this->session->data['order_id'];
		
		$sha_passphrase = "cotcotcot1234AA!";
		$sha_string = "AMOUNT=" . $this->data['amount'] . $sha_passphrase . "CURRENCY=" . $this->data['currency'] . $sha_passphrase;
		$sha_string .= "LANGUAGE=" . $this->data['language'] . $sha_passphrase . "ORDERID=" . $this->data['order_id'] . $sha_passphrase;
		$sha_string .= "PSPID=" . $this->data['pay_to_email'] . $sha_passphrase;

		//echo $sha_string;
		$sha_string = sha1($sha_string);

		//echo $sha_string;

		$this->data['sha'] = $sha_string;
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/payfort.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/payment/payfort.tpl';
		} else {
			$this->template = 'default/template/payment/payfort.tpl';
		}	
		
		$this->render();
	}
	/*public function t(){
		$this->load->model('checkout/order');
		$this->model_checkout_order->update(318,2, '', true);


	}*/
	public function callback() {
		if (isset($this->request->post['orderID'])) {
			$order_id = $this->request->post['orderID'];
		} else {
			$order_id = 0;
		}
		print_r($this->request->post);
		$sha_passphrase = "cotcotcot1234AA!";

		$this->load->model('checkout/order');

		$order_info = $this->model_checkout_order->getOrder($order_id);
		$results = print_r($this->request->post, true);
		
		file_put_contents('/ebs1/pf_log.txt', $results, FILE_APPEND);
		$f = fopen('/ebs1/pf_log.txt', 'a');
		fwrite($f, $this->request->post['STATUS'] . '\n');
		fclose($f);
		if ($order_info) {
			//echo "here";
			$this->model_checkout_order->confirm($order_id, $this->config->get('config_order_status_id'));

			$verified = false;
			
			// md5sig validation
			$hash  = strtolower($this->request->post['SHASIGN']);
			$hash_created = "";
			if($this->request->post['NCERROR'] != "")
			{
				$hash_created = "NCERROR=" .$this->request->post['NCERROR'] . $sha_passphrase;
			}

			$hash_created .= "ORDERID=" .$order_id . $sha_passphrase;
			$hash_created .= "PAYID=" . $this->request->post['PAYID'] .$sha_passphrase;
			$hash_created .= "STATUS=" . $this->request->post['STATUS'] .$sha_passphrase;
			echo $hash_created . "\n";
			$hash_created  = sha1($hash_created);
			echo $hash_created;
			/*
			  $this->request->post['transaction_id'];
			$hash .= strtoupper(md5($this->config->get('moneybookers_secret')));
			$hash .= $this->request->post['mb_amount'];
			$hash .= $this->request->post['mb_currency'];
			$hash .= $this->request->post['status'];
			
			$md5hash = strtoupper(md5($hash));
			$md5sig = $this->request->post['md5sig'];
			*/
			//echo $hash_created . '\n' . $hash;
			if ($hash == $hash_created) {
				$verified = true;
			}
			echo $verified;
			if ($verified) {
				$this->log->write('Wohooo');
				switch($this->request->post['STATUS']) {
					case '2':
						$this->model_checkout_order->update($order_id, 8, '', true); //denied
						break;
				
					case '0':
					case '1':
					case '6':
					case '61':
					case '62':
					case '63':
					case '64':
					case '7':
					case '71':
					case '72':
					case '73':
					case '74':
					case '75':
					case '93':
						$this->model_checkout_order->update($order_id, $this->config->get('payfort_failed_status_id'), '', true);
						break;
					case '4':
					case '40':
					case '41':

						$this->model_checkout_order->update($order_id, $this->config->get('payfort_order_status_id'), '', true);
						break;					
					case '5':
					case '50':
					case '51':
					case '52':
					case '53':
					case '54':
					case '55':
					case '56':
					case '57':
					case '58':
					case '59':
						$this->model_checkout_order->update($order_id, $this->config->get('payfort_order_status_id'), '', true);
						$this->log->write('payfort active ayee');
						break;
					case '9':
					case '91':
					case '92':
					case '94':
					case '95':
					case '99':
						$this->model_checkout_order->update($order_id, $this->config->get('payfort_pending_status_id'), '', true);
						$this->log->write('payfort active ayee');
						break;	
					case '92':
						$this->model_checkout_order->update($order_id, 2, '', true); //processing
						$this->log->write('payfort active ayee');
						break;					
					case '8':
					case '81':
					case '82':
					case '83':
					case '84':
					case '85':
						$this->model_checkout_order->update($order_id, $this->config->get('payfort_chargeback_status_id'), '', true);
						break;
					default:
						$this->model_checkout_order->update($order_id, $this->config->get('payfort_failed_status_id'), '', true);
						break;
				}
			} else {
				$this->log->write('md5sig returned (' . $hash . ') does not match generated (' . $hash_created . '). Verify Manually. Current order state: ' . $this->config->get('config_order_status_id'));
			}
		}else
		{
			echo "no order info";
		}
	}
}
?>