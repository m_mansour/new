<?php
class ControllerProductLatest extends Controller 
{
  public function index()
  {
    $this->language->load('product/category');
    
    $this->load->model('catalog/category');
    
    $this->load->model('catalog/product');

    $this->load->model('tool/image'); 


    //give page departments & handle its filtering URLS
    //17: Accessories
    //25: Bags
    //18:Clothing
    $departments = array();
    $departments[]= array ('id' => '17', 'name' => 'Accessories');
    $departments[]= array ('id' => '18', 'name' => 'Clothing');
    $departments[]= array ('id' => '25', 'name' => 'Bags');
    $department_filters = array();
    if(isset($this->request->get['department_filter']))
    {
      $department_filter_query = $this->request->get['department_filter'];
      $department_filters = explode(';', $department_filter_query);
    }
    foreach($departments as $department)
    {
        $active=0;
        $new_department_filters = $department_filters;
        if(in_array($department['id'], $department_filters ) == FALSE )
        {
          $new_department_filters[] = $department['id'];
        }
        else
        {
          //print_r(array_diff($new_department_filters, array($department['id'])));
          $new_department_filters = array_merge(array_diff($new_department_filters, array($department['id'])));
          $active=1;
        }
        $new_department_filter_query = '';
        if(count( $new_department_filters) > 0) {
          $new_department_filter_query .= 'department_filter=' . implode(';', $new_department_filters);
        }
        if(isset($this->request->get['designer_filter'])){
          $new_department_filter_query .= '&designer_filter=' . $this->request->get['designer_filter']; 
        }
        if(isset($this->request->get['color_filter'])){
          $new_department_filter_query .= '&color_filter=' . $this->request->get['color_filter']; 
        }
         $this->data['departments'][] =array(
                'department_id' =>  $department['id'],
                'name' => $department['name'],
                'filter_query' => $new_department_filter_query,
                'active' => $active
              );
    }


    //give page designers & handle its filtering URLs
    $categories = $this->model_catalog_category->getCategories(20);
    $designer_filters = array();
    if(isset($this->request->get['designer_filter']))
    {
      $designer_filter_query = $this->request->get['designer_filter'];
      $designer_filters = explode(';', $designer_filter_query);

    }
    foreach($categories as $category)
    {
      $active=0;
      $new_designer_filters = $designer_filters;

      if(in_array($category['category_id'], $designer_filters) == FALSE )
      {
          $new_designer_filters[] = $category['category_id'];
      }
      else
      {
        //print_r(array_diff($new_designer_filters, array($category['category_id'])));
        $new_designer_filters= array_merge(array_diff($new_designer_filters, array($category['category_id'])));
        $active=1;
      }
        $new_designer_filter_query = '';
        if(count( $new_designer_filters) > 0) {
          $new_designer_filter_query .= 'designer_filter=' . implode(';', $new_designer_filters);
        }
      if(isset($this->request->get['department_filter'])){
        $new_designer_filter_query .= '&department_filter=' . $this->request->get['department_filter']; 
      }       
      if(isset($this->request->get['color_filter'])){
        $new_designer_filter_query .= '&color_filter=' . $this->request->get['color_filter']; 
      }      
      $this->data['designers'][] =array(
              'category_id' =>  $category['category_id'],
              'name' => $category['name'],
              'filter_query' => $new_designer_filter_query,
              'active' => $active
            );
    }
    /* Start color filter */
    $categories = $this->model_catalog_category->getOptionValues(13);
   // print_r($categories);
   $color_filters = array();
    if(isset($this->request->get['color_filter']))
    {
      $color_filter_query = $this->request->get['color_filter'];
      $color_filters = explode(';', $color_filter_query);
    }
    foreach($categories as $category)
    {
      $active=0;
      $new_color_filters = $color_filters;

      if(in_array($category['option_value_id'], $color_filters) == FALSE )
      {
          $new_color_filters[] = $category['option_value_id'];
      }
      else
      {
        //print_r(array_diff($new_designer_filters, array($category['category_id'])));
        $new_color_filters= array_merge(array_diff($new_color_filters, array($category['option_value_id'])) );
       // print_r($new_color_filters);
        $active=1;
      }
        $new_color_filter_query = '';
        if(!empty( $new_color_filters) ) {
          $new_color_filter_query .= 'color_filter=' . implode(';', $new_color_filters);
        }
      if(isset($this->request->get['department_filter'])){
        $new_color_filter_query .= '&department_filter=' . $this->request->get['department_filter']; 
      }       
      if(isset($this->request->get['designer_filter'])){
        $new_color_filter_query .= '&designer_filter=' . $this->request->get['designer_filter']; 
      }       
      $this->data['colors'][] =array(
              'color_id' =>  $category['option_value_id'],
              'name' => $category['name'],
              'filter_query' => $new_color_filter_query,
              'active' => $active
            );
    }
    $designer_filter = array();
    if(isset($this->request->get['designer_filter']) && $this->request->get['designer_filter'] != '' && $this->request->get['designer_filter'] != ';')
    {
      $designer_filter_query = $this->request->get['designer_filter'];
      $designer_filter_query=explode(';',$designer_filter_query);
      for($i=0; $i<count($designer_filter_query);$i++){
        if($designer_filter_query[$i]!='')
        {
          $designer_filter[] = $designer_filter_query[$i];
        }
      }

    }
    $department_filter = array();
    if(isset($this->request->get['department_filter']) && $this->request->get['department_filter'] != '' && $this->request->get['department_filter'] != ';')
    {
      $department_filter_query = $this->request->get['department_filter'];
      $department_filter_query=explode(';',$department_filter_query);
      for($i=0; $i<count($department_filter_query);$i++){
        if($department_filter_query[$i]!='')
        {
          $department_filter[] = $department_filter_query[$i];
        }
      }

    }
    $color_filter = array();
    if(isset($this->request->get['color_filter']) && $this->request->get['color_filter'] != '' && $this->request->get['color_filter'] != ';')
    {
      $color_filter_query = $this->request->get['color_filter'];
      $color_filter_query=explode(';',$color_filter_query);
      for($i=0; $i<count($color_filter_query);$i++){
        if($color_filter_query[$i]!='')
        {
          $color_filter[] = $color_filter_query[$i];
        }
      }

    }
    //print_r($color_filter);
    //print_r($designer_filter);
    $results = $this->model_catalog_product->getLatestProductsJustIn(25, $designer_filter, $department_filter, $color_filter);
    //print_r($results);
    foreach ($results as $result) {
        if ($result['image']) {
          $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
         // $image = $this->model_tool_image->resize($result['image'], 200, 220);
        } else {
          $image = false;
        }
        
        if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
          $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
        } else {
          $price = false;
        }
        
        if ((float)$result['special']) {
          $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
        } else {
          $special = false;
        } 
        
        if ($this->config->get('config_tax')) {
          $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
        } else {
          $tax = false;
        }       
        
        if ($this->config->get('config_review_status')) {
          $rating = (int)$result['rating'];
        } else {
          $rating = false;
        }
        $r = array();
      
        $k = $this->model_catalog_product->getProductImages($result['product_id']);
      
        foreach ($k as $p) {
          $r[] =  $this->model_tool_image->resize($p['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
        } 

        $this->data['products'][] = array(
          'product_id'  => $result['product_id'],
          'thumb'       => $image,
          'name'        => $result['name'],
          'model'       => $result['model'],
          'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, 100) . '..',
          'price'       => $price,
          'special'     => $special,
          'tax'         => $tax,
          'rating'      => $result['rating'],
          'reviews'     => sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
          'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id']),
          'additional_images' => $r
        );
      }

      if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/latest.tpl')) {
        $this->template = $this->config->get('config_template') . '/template/product/latest.tpl';
      } else {
        $this->template = 'default/template/product/latest.tpl';
      }
      //$this->template = 'default/template/product/latest.tpl';
     // echo "here";
      $this->children = array(
        'common/column_left',
        'common/column_right',
        'common/content_top',
        'common/content_bottom',
        'common/footer',
        'common/header'
      );
        
      $this->response->setOutput($this->render());  
  }
}