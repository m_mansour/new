<?php  
class ControllerModuleBannerlist extends Controller {
	protected function index($setting) {
		static $module = 0;
		
		$this->load->model('design/banner');
		$this->load->model('tool/image');
		
		$this->data['banners'] = array();
		
		$results = $this->model_design_banner->getBanner($setting['banner_id']);
		
		if($setting['show'] == 'random' || $setting['show'] == 'allrandom')
			shuffle($results);
		
		if($setting['show'] == 'random')
			$results = array_slice($results, 0, 1);
		//  echo 'whaaat';
		foreach ($results as $result) {
			if (file_exists(DIR_IMAGE . $result['image'])) {
				$this->data['banners'][] = array(
					'title' => $result['title'],
					'link'  => $result['link'],
					//'image' => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height'])
					'image' => 'http://coterique.com/image/'. $result['image']
				);
			}
		}
		
		$this->data['module'] = $module++;
				
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/bannerlist.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/bannerlist.tpl';
		} else {
			$this->template = 'default/template/module/bannerlist.tpl';
		}
		
		$this->render();
	}
}
?>