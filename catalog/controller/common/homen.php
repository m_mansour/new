<?php
class ControllerCommonHomen extends Controller {
	public function index() {

        $this->load->model('catalog/category');

        $this->load->model('catalog/product');

        if (isset($this->request->get['path'])) {
            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $path = '';

            $parts = explode('_', (string)$this->request->get['path']);

            $category_id = (int)array_pop($parts);

            foreach ($parts as $path_id) {
                if (!$path) {
                    $path = (int)$path_id;
                } else {
                    $path .= '_' . (int)$path_id;
                }

                $category_info = $this->model_catalog_category->getCategory($path_id);


                if ($category_info) {
                    $data['breadcrumbs'][] = array(
                        'text' => $category_info['name'],
                        'href' => $this->url->link('product/category', 'path=' . $path . $url)
                    );
                }
            }
        }
       var_dump($category_info);exit;
        $data['categories'] = array();

        $category_info= $this->model_catalog_category->getCategoryf(1);

        $category_infos = $this->model_catalog_category->getCategory($category_info['category_id']);


        $this->load->model('localisation/country');
        $country_info = $this->model_localisation_country->getCountry($category_infos['country_id']);

        $data['country_name'] =$country_info['name'];
        $this->load->model('localisation/zone');
        $zone_info = $this->model_localisation_zone->getZone($category_infos['zone_id']);
        $data['zone_name'] =$zone_info['name'];

        $data['heading_title'] = $category_info['name'];
        $data['url'] = $this->url->link('product/category', 'path=' . $category_info['category_id']);
        $data['description'] = html_entity_decode($category_info['description']);

    //  var_dump($categories);

       // $categories = $this->model_catalog_category->getCategories(0);
        foreach ($categories as $category) {


                $children_data = array();

                $children = $this->model_catalog_category->getCategories();


                // Level 1
                $data['categories'][] = array(
                    'name' => $category['name'],
                    'column' => $category['column'] ? $category['column'] : 1,
                    'href' => $this->url->link('product/category', 'path=' . $category['category_id'])
                );

        }

        //mansour
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

		if (isset($this->request->get['route'])) {
			$this->document->addLink(HTTP_SERVER, 'canonical');
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/homen.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/homen.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/common/homen.tpl', $data));
		}
	}
}