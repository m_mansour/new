<?php
class ControllerDiscoverListing extends Controller {

 public function index(){
     $this->document->setTitle("Discover");

     if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/discover/listing.tpl')) {
         $this->template = $this->config->get('config_template') . '/template/discover/listing.tpl';
     } else {
         $this->template = 'default/template/discover/listing.tpl';
     }


     $this->load->model('catalog/category');

     $data['categories'] = array();

     $categories=$this->model_catalog_category->getCategories(20);
     //var_dump($categories);
     //exit;

     $this->load->model('tool/image');
     //$category_info = $this->model_catalog_category->getCategory($path_id);


     if (isset($this->request->get['path'])) {
         $url = '';

         if (isset($this->request->get['sort'])) {
             $url .= '&sort=' . $this->request->get['sort'];
         }

         if (isset($this->request->get['order'])) {
             $url .= '&order=' . $this->request->get['order'];
         }

         if (isset($this->request->get['limit'])) {
             $url .= '&limit=' . $this->request->get['limit'];
         }

         $path = '';

         $parts = explode('_', (string)$this->request->get['path']);

         $category_id = (int)array_pop($parts);

         foreach ($parts as $path_id) {
             if (!$path) {
                 $path = (int)$path_id;
             } else {
                 $path .= '_' . (int)$path_id;
             }

             $category_info = $this->model_catalog_category->getCategory($path_id);


             if ($category_info) {
                 $data['breadcrumbs'][] = array(
                     'text' => $category_info['name'],
                     'href' => $this->url->link('product/category', 'path=' . $path . $url)
                 );
             }
         }
     } else {
         $category_id = 0;
     }
     if ($category_info['image']) {
         $ee = $this->model_tool_image->resize($category_info['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
     } else {
         $data['thumb'] = '';
     }

$results = $this->model_catalog_category->getCategories(20);



     //$image = empty($category['image']) ? 'no_image.jpg' : $category['image'];
     //$thumb = $this->model_tool_image->resize($image, 100, 100);



     foreach ($results as $result) {
         $filter_data = array(
             'filter_category_id'  => $result['category_id'],
             'filter_sub_category' => false
         );

         $data['categories'][] = array(
             'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '_' . $result['category_id'] . $url),
             'category_id' => $result['category_id'],
             'name'        => $result['name'],
             'thumb' => $result['image']
         );
     }




     $data['column_left'] = $this->load->controller('common/column_left');
     $data['column_right'] = $this->load->controller('common/column_right');
     $data['content_top'] = $this->load->controller('common/content_top');
     $data['content_bottom'] = $this->load->controller('common/content_bottom');
     $data['footer'] = $this->load->controller('common/footer');
     $data['header'] = $this->load->controller('common/header');





     // call the "View" to render the output

     $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/discover/listing.tpl', $data));

 }
}