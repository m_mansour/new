<?php echo $header; ?>
<?php if( $parent_id != 20){ ?>	
<?php //echo $column_left; ?><?php //echo $column_right; ?>
<!--<div id='column-left'>
  <div class="box">
    <div class="box-content" style="border-style: solid; border-width: 2px; border-color: ">
      <div class="box-category">
        <ul>
          <li><a href="#" class="active">DEPARTMENTS</a>
            <ul>
              <?php 
              //are we already filtered?

              foreach($departments as $department)
              {
                ?><li><a <?php if($department['active']) echo 'style="text-decoration: underline;"'; ?> href="http://coterique.com/index.php?route=product/latest&<?php echo $department['filter_query']; ?>"><?php echo $department['name']; ?></a></li>
              <?
              }
              ?>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </div>

  <div class="box">
    <div class="box-content" style="border-style: solid; border-width: 2px; border-color: ">
      <div class="box-category">
        <ul>
          <li><a href="#" class="active">DESIGNERS</a>
            <ul>
              <?php 
              //are we already filtered?

              foreach($designers as $designer)
              {
                ?><li><a <?php if($designer['active']) echo 'style="text-decoration: underline;"'; ?> href="http://coterique.com/index.php?route=product/latest&<?php echo $designer['filter_query']; ?>"><?php echo $designer['name']; ?></a></li>
              <?
              }
              ?>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </div>

  <div class="box">
    <div class="box-content" style="border-style: solid; border-width: 2px; border-color: ">
      <div class="box-category">
        <ul>
          <li><a href="#" class="active">COLORS</a>
            <ul>
              <?php 
              //are we already filtered?

              foreach($colors as $color)
              {
                ?><li><a <?php if($color['active']) echo 'style="text-decoration: underline;"'; ?> href="http://coterique.com/index.php?route=product/latest&<?php echo $color['filter_query']; ?>"><?php echo $color['name']; ?></a></li>
              <?
              }
              ?>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
-->
<div id="content"><?php echo $content_top; ?>
  <?php if ($thumb || $description) { ?>
  <div class="category-info">
    <?php if ($thumb) { ?>
    <div class="image"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" /></div>
    <?php } ?>
    <!--<?php if ($description) { ?>
    <?php echo $description; ?>
    <?php } ?>-->
  </div>
  <?php } ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <div class="product-grid"  >

  <?php foreach ($products as $product) { ?>
  <div>
    <?php if ($product['thumb']) { ?>
    <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" />

        <?php foreach($product['additional_images'] as $add_img) { ?>
      <img src="<?php echo $add_img; ?>" style="display:none;">
      <?php } ?>


    </a></div>
    <?php } ?>
    <div class="brand"><a href="<?php echo $product['href']; ?>"><?php echo $product['model']; ?></a></div>
    <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
    <div class="description"><?php echo $product['description']; ?></div>
    <?php if ($product['price']) { ?>
    <div class="price">
      <?php if (!$product['special']) { ?>
      <?php echo $product['price']; ?>
      <?php } else { ?>
      <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
      <?php } ?>
      <?php if ($product['tax']) { ?>
      <br />
      <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
      <?php } ?>
    </div>
    <?php } ?>
      <!--<div class="cart">
        <input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button" />
      </div>-->
    </div>
    <?php } ?>
  </div>
</div>


  <div class="pagination"><?php echo $pagination; ?></div>
  <?php } ?>
  <?php if (!$categories && !$products) { ?>
  <h1> &nbsp; </h1>
  <div class="content">
   <div class="left"><?php echo $text_empty; ?></div></div>
   <div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
  </div>
  <?php } ?>
  <?php echo $content_bottom; ?></div>
  


  <script src="http://cdn.jsdelivr.net/flexslider/2.1/jquery.flexslider-min.js" type="text/javascript"></script>
  <link rel="stylesheet" href="//cdn.jsdelivr.net/flexslider/2.1/flexslider.css">
  <script type="text/javascript">
  $(window).load(function() {
    $('.flexslider').flexslider({
      animation: "slide",
      animationLoop: false,
      itemWidth: 210,
      itemMargin: 5,
      minItems: 2,
      maxItems: 4
    });
  });
  </script>
  <style>
  .designer_banner{
   padding-top: 20px;
 }

 .designer_banner img{
   width: 100%;
   height: 160px;
 }
 </style>
 <div class="designer_banner"><img src="<?php echo $test_banner; ?>" alt="<?php echo $heading_title; ?>" /></div>
 <div class="product-filter"></div>
 <script>
  var last_time_out = 0;
  function show_hide_image(image_class, current_id, counter){
      i=0;
      while (i < counter){
        if(i==current_id)
        {
          
        }
        else{
          image_class.find('img').eq(i).fadeOut(500).hide()
        }
        i++;

        //current_id++;
  }
  image_class.find('img').eq(current_id).fadeIn(500).show()
  console.log('show image #' + i)
  last_time_out  = setTimeout( function(){
      show_hide_image(image_class, (++current_id % counter ) , counter);
       }, 1000) ;
}
$( ".image" ).hover(function() {
  var counter = 0;
  $(this).find('img').each(function(){
    counter++;
  });
  console.log(counter);
  current_id = 0;
  image_class = $(this);
  if(counter>1){

    last_time_out = setTimeout( function(){
      show_hide_image(image_class, current_id, counter);
       }, 1) ;
  }
}, function(){
  clearTimeout(last_time_out);
});


</script>

 <?php echo $footer; ?>
