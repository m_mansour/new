<!DOCTYPE html>

<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
  <meta charset="UTF-8" />
  <title><?php echo $title; ?></title>
  <base href="<?php echo $base; ?>" />
  <?php if ($description) { ?>
  <meta name="description" content="<?php echo $description; ?>" />
  <?php } ?>
  <?php if ($keywords) { ?>
  <meta name="keywords" content="<?php echo $keywords; ?>" />
  <?php } ?>
  <?php if ($icon) { ?>
  <link href="<?php echo $icon; ?>" rel="icon" />
  <?php } ?>
  <?php foreach ($links as $link) { ?>
  <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
  <?php } ?>
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/shop/stylesheet/stylesheet.css" />
  <?php foreach ($styles as $style) { ?>
  <link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
  <?php } ?>

  <!-- Facebook Conversion Code for FB -->
  <script type="text/javascript">
  var fb_param = {};
  fb_param.pixel_id = '6016532623537';
  fb_param.value = '0.01';
  fb_param.currency = 'USD';
  (function(){
  var fpw = document.createElement('script');
  fpw.async = true;
  fpw.src = '//connect.facebook.net/en_US/fp.js';
  var ref = document.getElementsByTagName('script')[0];
  ref.parentNode.insertBefore(fpw, ref);
  })();
  </script>
  <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6016532623537&amp;value=0.01&amp;currency=USD" /></noscript>
  <script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
  <script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
  <link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />
  <script type="text/javascript" src="catalog/view/javascript/jquery/ui/external/jquery.cookie.js"></script>
  <script type="text/javascript" src="catalog/view/javascript/jquery/colorbox/jquery.colorbox.js"></script>
  <link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/colorbox/colorbox.css" media="screen" />
  <script type="text/javascript" src="catalog/view/javascript/jquery/tabs.js"></script>
  <script type="text/javascript" src="catalog/view/javascript/common.js"></script>
  <!-- <script type="text/javascript" src="/fancybox/source/jquery.fancybox.js?v=2.1.5"></script> -->
  <script type="text/javascript" src="catalog/view/javascript/livechat.js"></script>
  <script type='text/javascript' src='catalog/view/javascript/jquery.jqzoom-core-pack.js'></script>  
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/shop/stylesheet/jquery.jqzoom.css">  
  <script>
    $(document).ready(function(){ 
      $('.image_product').jqzoom({
	        zoomType: 'standard',
            showEffect : 'fadein',  
            hideEffect: 'fadeout'});  
    });  
  </script>
  <?php foreach ($scripts as $script) { ?>
  <script type="text/javascript" src="<?php echo $script; ?>"></script>
  <?php } ?>
<!--[if IE 7]>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/shop/stylesheet/ie7.css" />
<![endif]-->
<!--[if lt IE 7]>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/shop/stylesheet/ie6.css" />
<script type="text/javascript" src="catalog/view/javascript/DD_belatedPNG_0.0.8a-min.js"></script>
<script type="text/javascript">
DD_belatedPNG.fix('#logo img');
</script>
<![endif]-->
<?php echo $google_analytics; ?>
</head>
<body>
  <div id="container">
    <div id="header">
      <?php if ($logo) { ?>
      <div id="logo"><a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></a><br><i>Curated fashion for the middle east</i></div>
      <?php } ?>
      <?php echo $language; ?>

      <div id="icons" style="display:none">
        <a href="https://www.facebook.com/pages/Coterique/387226174709700"><img src='http://coterique.com/catalog/view/theme/shop/image/icon_facebook.png' alt="facebook" height="17" width="17"></a>
        <a href="https://twitter.com/Coterique"><img src='http://coterique.com/catalog/view/theme/shop/image/icon_twitter.png' alt="twitter" height="17" width="17"></a>
        <a href="http://pinterest.com/coterique/"><img src='http://coterique.com/catalog/view/theme/shop/image/icon_pinterest.png' alt="pinterest" height="17" width="17"></a>
	<a href="http://coterique.tumblr.com/"><img src='http://coterique.com/catalog/view/theme/shop/image/icon_tumblr.png' alt="tumblr" height="17" width="17"></a>
	<a href="http://instagram.com/coterique"><img src='http://coterique.com/catalog/view/theme/shop/image/icon_instagram.png' alt="instagram" height="17" width="17"></a>

      </div>
<?php /*
      <div id="welcome">
        <?php if (!$logged) { ?>
         <a href="http://coterique.com">Women</a> / <a href="http://coterique.com/landing.html">Men</a>
        <?php } else { ?>
        <?php echo $text_logged; ?>
        <?php } ?>
      </div>
*/?>
      <!--<?php echo $currency; ?>-->

  <div id="search">
    <div class="button-search"></div>
    <?php if ($filter_name) { ?>
    <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" />
    <?php } else { ?>
    <input type="text" name="filter_name" value="<?php echo $text_search; ?>" onclick="this.value = '';" onkeydown="this.style.color = '#000000';" />
    <?php } ?>
  </div>
  
  <div name="topBar" style="position: fixed;width:100%;background: #ebe5dc;left: 0;z-index: 100;">
  <?php echo $cart; ?>
   </div>

<div style="float: right;margin-top: 46px;margin-right: 3px;">
<?php if (!$logged) { ?>
         <i>Welcome to Coterique, <a href="http://coterique.com/index.php?route=account/login">Sign in</a> or <a href="http://coterique.com/index.php?route=account/register">Register</a> </i>
        <?php } else { ?>
        <?php echo $text_logged; ?>
        <?php } ?>
      </div>

  


</div>
<?php if ($categories) { ?>
<div id="menu">
  <ul>
	<li><a href="http://coterique.com/index.php?route=product/latest" >JUST IN</a></li>
   <!--<li><a href="<?php echo $home; ?>"><?php echo $text_home; ?></a></li>-->
   <?php foreach ($categories as $category) { ?>
   <li><a href="<?php if($category['name']=='DESIGNERS'){ 
     echo "http://coterique.com/index.php?route=information/listing";
   }else{
     echo $category['href'];
   } ?>"><?php echo $category['name']; ?></a>
   <?php if ($category['children']) { ?>
   <div>
    <?php 
    $count_category = count($category['children']);
    if($category['name'] == 'DESIGNERS'){ $count_category = 9; }
    for ($i = 0; $i < $count_category;)
    { 
     ?>
     <ul>
      <?php if ($category['name'] == 'DESIGNERS') { ?>
      <li><a href="http://coterique.com/index.php?route=information/listing" >All Designers</a></li>
      <?php }?>

      <?php $j = $i + ceil(count($category['children']) / $category['column']); ?>
      <!-- <?php if ($category['name'] == 'DESIGNERS') { $j = 9; } ?> -->
      <?php for (; $i < $j; $i++) { ?>
      <?php if (isset($category['children'][$i])) { ?>
      <li><a href="<?php echo $category['children'][$i]['href']; ?>"><?php echo $category['children'][$i]['name']; ?></a></li>
      <?php } ?>
      <?php } ?>
    </ul>
    <?php } ?>
  </div>
  <?php } ?>
</li>
<?php } ?>
<?php /*
<li>
  <a href="#">ACCESSORIES</a>
  <div style>
    <ul>
      <li>
        <a href="http://coterique.com/index.php?route=product/category&path=25_72">Bags</a>
        <a href="#">Jewelry</a>
        <a href="#">Belts</a>
        <a href="#">Sunglasses</a>
        <a href="#">Shoes</a>
      </li>
    </ul>
  </div>
</li>
*/ ?>
<li><a href="http://coterique.com/index.php?route=information/mag" >FW13 CAMPAIGN</a></li>
<li><a href="http://coterique.com/index.php?route=information/blog" >BLOG</a></li>
<?/*
<li><a href="#">GIFTS</a>
	<div>
	<ul>
		<li><a href="#">For the Working Lady</a></li>
		<li><a href="#">For the Party Girl</a></li>
		<li><a href="#">For your Mother</a></li>
	</ul>
	</div>
</li> */?>
<li><a href="http://coterique.com/index.php?route=information/outfits">OUTFITS</a>
</ul>
</div>
<?php } ?>
<div id="notification"></div>
