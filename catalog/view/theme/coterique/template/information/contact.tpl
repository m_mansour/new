<?php echo $header; ?>

<section class="main-banner-section banner3">
    <div class="caption-contact">
        <div class="container">
            <div class="col-md-5">
                <div class="left-bar">
                    <div class="title">
                        <h2>Contact information</h2>
                        <p>Please use the contact information below to reach us. We’re open Sun–Friday 9:00AM–5:00PM.</p>
                    </div>
                    <div class="places">
                        <ul class="list-unstyled">
                            <li class="clearfix"><!--item-->
                                <div class="icon-wrap"><img src="catalog/view/theme/coterique/image/assets/phone.png" alt=""></div>
                                <div class="info">
                                    <h4>Phone Number</h4>
                                    <h5><?php echo $telephone; ?></h5>
                                </div>
                            </li>
                            <li class="clearfix"><!--item-->
                                <div class="icon-wrap"><img src="catalog/view/theme/coterique/image/assets/mail2.png" alt=""></div>
                                <div class="info">
                                    <h4>email address</h4>
                                    <h5>info@coterique.com</h5>
                                </div>
                            </li>
                            <li class="clearfix"><!--item-->
                                <div class="icon-wrap"><img src="catalog/view/theme/coterique/image/assets/location.png" alt=""></div>
                                <div class="info">
                                    <h4>office address</h4>
                                    <h5><?php echo $address; ?></h5>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-offset-1">
                <div class="righ-bar">
<!--                    <div class="title">
                        <h2>Contact inforamtion</h2>
                        <p>Please use the contact information below to reach us. We’re open Sun–Friday 9:00AM–5:00AM.</p>
                    </div>-->
                    <div class="form-content-wraping">
                        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <div class="form-group">
                                <div class="col-md-6 grid-l">
                                    <label for="input-name"><?php echo $entry_name; ?></label>

                                    <input type="text" name="name" value="<?php echo $name; ?>" id="input-name" placeholder="Full Name"  class="form-control">
                                    <?php if ($error_name) { ?>
                                    <div class="text-danger"><?php echo $error_name; ?></div>
                                    <?php } ?>
                                </div>
                                <div class="col-md-6 grid-r grid-l">
                                    <label for="input-email"><?php echo $entry_email; ?></label>
                                    <input type="text" name="email" id="input-email" value="<?php echo $email; ?>" placeholder="example@example.com"  class="form-control">
                                    <?php if ($error_email) { ?>
                                    <div class="text-danger"><?php echo $error_email; ?></div>
                                    <?php } ?>
                                </div>
                                <div class="col-md-12 grid-l">
                                    <label for="enquiry-title">type of inquiry</label>
                                    <input type="text" name="enquiryh" id="enquiry-title" placeholder="Apply as a designer" value="<?php echo $enquiryh; ?>"  class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12 grid-l">
                                <label for="input-enquiry"><?php echo $entry_enquiry; ?></label>
                                <textarea name="enquiry" type="text" id="input-enquiry" placeholder="Your message…" required class="form-control"><?php echo $enquiry; ?></textarea>
                                <?php if ($error_enquiry) { ?>
                                <div class="text-danger"><?php echo $error_enquiry; ?></div>
                                <?php } ?>
                            </div>
                            <?php echo $captcha; ?>
                            <div class="col-md-12 grid-l">
                                <button type="submit" class="btn btn-danger" value="<?php echo $button_submit; ?>">SEND your message</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo $footer; ?>
