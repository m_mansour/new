<section style="background:url('imgs/designer-bg.jpg') 50% 50% no-repeat;background-size:cover; -webkit-background-size:cover;" class="sub-banners"><!--banner-section-->
    <div class="caption">
        <div class="container">
            <h1>Deborah Henning</h1>
            <button class="line">FOLLOW</button>
        </div>
    </div>
</section>
<div class="designer-story clearfix"><!--Designer Story-->
    <div class="grid col-md-5">
        <div class="country-image">
            <div class="caption">
                <div class="flag"><img src="imgs/flag0.jpg" alt=""></div>
                <div class="info">
                    <h2>London</h2>
                    <p>United Kingdom</p>
                </div>
            </div>
        </div>
    </div>
    <div class="grid col-md-5">
        <div class="content-info">
            <h3>story of the designer</h3>
            <p>The Deborah Henning label creates a beautiful mix of playful, androgynous womenswear, which is based on the concept of casual perfection. Harnessing the ‘just out of bed’ look in a subtle and sensual manner whilst keeping a refined and unforced styling attitude. Mixing the effortless air of Parisian style icons and inspiration from the undertones of the British Music scene, the label creates a delicate balance of casual, romantic clothing made for those who want to make a style statement without looking overdressed.</p><a href="#" class="line">read full interview</a>
        </div>
    </div>
    <div class="grid col-md-2">
        <div class="item-1"><img src="imgs/item21.png" alt="">
            <h4>featured product</h4><a href="#">
                <p>Floral-Print Cut-Out Crepe Blazer</p></a><b>   $250</b>
        </div>
    </div>
</div>
<div class="designer-profile-content">
    <div class="container">
        <div class="crumb clearfix hidden-xs"><!--breadcrumb-->
            <ul class="list-unstyled">
                <li><a href="#">Home</a></li>
                <li><a href="">DESIGNERS</a></li>
                <li><a href="">DEBORAH HENNING</a></li>
            </ul>
        </div>
        <button data-toggle="collapse" data-target="#toggle-filters-mob" class="btn btn-toogle-filters visible-xs">REFINE RESULTS</button>
        <div id="toggle-filters-mob" class="filter-options clearfix">
            <select class="selectpicker"><!--options1-->
                <option>Category</option>
                <option>option1</option>
                <option>option2</option>
            </select>
            <select class="selectpicker"><!--options1-->
                <option>SIZE</option>
                <option>option1</option>
                <option>option2</option>
            </select>
            <select disabled="disabled" class="selectpicker"><!--options1-->
                <option>Deborah…</option>
                <option>option1</option>
                <option>option2</option>
            </select>
            <select class="selectpicker"><!--options1-->
                <option>color</option>
                <option>option1</option>
                <option>option2</option>
            </select>
            <select class="selectpicker"><!--options1-->
                <option>sorty by</option>
                <option>option1</option>
                <option>option2</option>
            </select>
            <select class="selectpicker"><!--options1-->
                <option>view all</option>
                <option>option1</option>
                <option>option2</option>
            </select>
            <div class="visible-xs Apply btns">
                <button class="btn btn-1">Apply selections</button>
                <button class="btn btn-2">view all</button>
            </div>
        </div>
        <div class="container selected-tags hidden-xs"><!--tags-->
            <ul class="list-unstyled">
                <li>
                    <div class="tag">
                        <h4>deborah henning</h4><a class="remove"> <img src="imgs/colse.png" alt=""></a>
                    </div>
                    <div class="tag">
                        <h4>Dresses</h4><a class="remove"> <img src="imgs/colse.png" alt=""></a>
                    </div>
                    <div class="tag">
                        <h4>Clear all</h4><a class="remove all"><img src="imgs/colse.png" alt=""></a>
                    </div>
                </li>
            </ul>
        </div>
        <div class="row">
            <div class="items-wrap-designer-profile">
                <div class="col-md-3 col-sm-4 col-xs-6"><!-- item-->
                    <div class="item"><img src="imgs/desg1.jpg" alt="">
                        <div class="item-g">
                            <h4>ISABEL SANCHIS</h4><a href="#">
                                <p>Floral-Print Cut-Out Crepe Dress</p></a><b>   $250</b><a href="#" class="line">VIEW DETAILS</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-6"><!-- item-->
                    <div class="item"><img src="imgs/desg1.jpg" alt="">
                        <div class="item-g">
                            <h4>ISABEL SANCHIS</h4><a href="#">
                                <p>Floral-Print Cut-Out Crepe Dress</p></a><b>   $250</b><a href="#" class="line">VIEW DETAILS</a>
                        </div>
                        <div class="like"><img src="imgs/like.png" alt=""></div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-6"><!-- item-->
                    <div class="item"><img src="imgs/desg1.jpg" alt="">
                        <div class="item-g">
                            <h4>ISABEL SANCHIS</h4><a href="#">
                                <p>Floral-Print Cut-Out Crepe Dress</p></a><b>   $250</b><a href="#" class="line">VIEW DETAILS</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-6"><!-- item-->
                    <div class="item"><img src="imgs/desg1.jpg" alt="">
                        <div class="item-g">
                            <h4>ISABEL SANCHIS</h4><a href="#">
                                <p>Floral-Print Cut-Out Crepe Dress</p></a><b>   $250</b><a href="#" class="line">VIEW DETAILS</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-6"><!-- item-->
                    <div class="item"><img src="imgs/desg1.jpg" alt="">
                        <div class="item-g">
                            <h4>ISABEL SANCHIS</h4><a href="#">
                                <p>Floral-Print Cut-Out Crepe Dress</p></a><b>   $250</b><a href="#" class="line">VIEW DETAILS</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-6"><!-- item-->
                    <div class="item"><img src="imgs/desg1.jpg" alt="">
                        <div class="item-g">
                            <h4>ISABEL SANCHIS</h4><a href="#">
                                <p>Floral-Print Cut-Out Crepe Dress</p></a><b>   $250</b><a href="#" class="line">VIEW DETAILS</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-6"><!-- item-->
                    <div class="item"><img src="imgs/desg1.jpg" alt="">
                        <div class="item-g">
                            <h4>ISABEL SANCHIS</h4><a href="#">
                                <p>Floral-Print Cut-Out Crepe Dress</p></a><b>   $250</b><a href="#" class="line">VIEW DETAILS</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-6"><!-- item-->
                    <div class="item"><img src="imgs/desg1.jpg" alt="">
                        <div class="item-g">
                            <h4>ISABEL SANCHIS</h4><a href="#">
                                <p>Floral-Print Cut-Out Crepe Dress</p></a><b>   $250</b><a href="#" class="line">VIEW DETAILS</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-6"><!-- item-->
                    <div class="item"><img src="imgs/desg1.jpg" alt="">
                        <div class="item-g">
                            <h4>ISABEL SANCHIS</h4><a href="#">
                                <p>Floral-Print Cut-Out Crepe Dress</p></a><b>   $250</b><a href="#" class="line">VIEW DETAILS</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-6"><!-- item-->
                    <div class="item"><img src="imgs/desg1.jpg" alt="">
                        <div class="item-g">
                            <h4>ISABEL SANCHIS</h4><a href="#">
                                <p>Floral-Print Cut-Out Crepe Dress</p></a><b>   $250</b><a href="#" class="line">VIEW DETAILS</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-6"><!-- item-->
                    <div class="item"><img src="imgs/desg1.jpg" alt="">
                        <div class="item-g">
                            <h4>ISABEL SANCHIS</h4><a href="#">
                                <p>Floral-Print Cut-Out Crepe Dress</p></a><b>   $250</b><a href="#" class="line">VIEW DETAILS</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-6"><!-- item-->
                    <div class="item"><img src="imgs/desg1.jpg" alt="">
                        <div class="item-g">
                            <h4>ISABEL SANCHIS</h4><a href="#">
                                <p>Floral-Print Cut-Out Crepe Dress</p></a><b>   $250</b><a href="#" class="line">VIEW DETAILS</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container hidden-xs">
            <div class="paging"><!--paging--><a href="#" class="arrow"><img src="imgs/paging2.png" alt=""></a>
                <h3>Page <span>1 </span>of <span>10</span></h3><a href="#" class="arrow"><img src="imgs/paging1.png" alt=""></a>
            </div>
        </div>
    </div>
</div>