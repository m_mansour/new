<div class="col-md-12 designer-items">
  <?php foreach ($products as $product) { ?>
    <div class="col-md-3 col-sm-6 clearfix"><!--item-->
      <div class="item">
        <div class="img-wrap"><img height="" src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>">
          <div class="caption active">
            <h3> <?php echo $product['model']; ?> <span><?php echo $product['name']; ?>
                <span><?php echo $product['price']; ?></span></span>
            </h3>
            <div class="unactive">
              <p><?php echo $product['description']; ?></p><a href="<?php echo $product['href']; ?>">SHOP NOW</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php } ?>
</div>
