<?php echo $header; ?>

<?php
global $config;
$theme_path = $config->get('config_template');

?>

<div class="grid-coterique-discover clearfix">
    <div class="col-md-8 clearfix">
        <div class="grid-large">
            <div style="background:url('catalog/view/theme/<?php echo $theme_path; ?>/image/discover/1.jpg') no-repeat; background-size:cover;-webkit-background-size:cover;" class="right-col">
                <div class="col-md-offset-2">
                    <div class="content">
                        <div class="owl-slider-2">
                            <div class="item">
                                <h4>introducing new label</h4>
                                <h3>DEBORAH HENNING</h3>
                                <p>A label mixing the effortless air of Parisian style icons and inspiration from the undertones of the British Music scene.</p><a href="#">SHOP NOW<img src="catalog/view/theme/<?php echo $theme_path; ?>/image/assets/arrow-cit.png" alt=""></a>
                            </div>
                            <div class="item">
                                <h4>introducing new label</h4>
                                <h3>DEBORAH HENNING</h3>
                                <p>A label mixing the effortless air of Parisian style icons and inspiration from the undertones of the British Music scene.</p><a href="#">SHOP NOW<img src="catalog/view/theme/<?php echo $theme_path; ?>/image/assets/arrow-cit.png" alt=""></a>
                            </div>
                            <div class="item">
                                <h4>introducing new label</h4>
                                <h3>DEBORAH HENNING</h3>
                                <p>A label mixing the effortless air of Parisian style icons and inspiration from the undertones of the British Music scene.</p><a href="#">SHOP NOW<img src="catalog/view/theme/<?php echo $theme_path; ?>/image/assets/arrow-cit.png" alt=""></a>
                            </div>
                            <div class="item">
                                <h4>introducing new label</h4>
                                <h3>DEBORAH HENNING</h3>
                                <p>A label mixing the effortless air of Parisian style icons and inspiration from the undertones of the British Music scene.</p><a href="#">SHOP NOW<img src="catalog/view/theme/<?php echo $theme_path; ?>/image/assets/arrow-cit.png" alt=""></a>
                            </div>
                            <div class="item">
                                <h4>introducing new label</h4>
                                <h3>DEBORAH HENNING</h3>
                                <p>A label mixing the effortless air of Parisian style icons and inspiration from the undertones of the British Music scene.</p><a href="#">SHOP NOW<img src="catalog/view/theme/<?php echo $theme_path; ?>/image/assets/arrow-cit.png" alt=""></a>
                            </div>
                            <div class="item">
                                <h4>introducing new label</h4>
                                <h3>DEBORAH HENNING</h3>
                                <p>A label mixing the effortless air of Parisian style icons and inspiration from the undertones of the British Music scene.</p><a href="#">SHOP NOW<img src="catalog/view/theme/<?php echo $theme_path; ?>/image/assets/arrow-cit.png" alt=""></a>
                            </div>
                        </div>
                        <div class="btns-slide">
                            <button class="btn next"> <img src="catalog/view/theme/<?php echo $theme_path; ?>/image/assets/slider-top.png" alt=""></button>
                            <button class="btn prev"> <img src="catalog/view/theme/<?php echo $theme_path; ?>/image/assets/slider-top.png" alt=""></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4 clearfix">
        <div class="right-colm">
            <div class="nos-col">
                <div class="img-wrap"><img src="catalog/view/theme/<?php echo $theme_path; ?>/image/img23.jpg" alt=""></div>
            </div>
            <div class="nos-col">
                <div class="caption">
                    <h3>MSGM Pre-fall collection now available</h3><a href="#" class="line">SHOP NOW </a>
                </div>
            </div>
        </div>
    </div>
    <div class="grid-coterique-discover clearfix">
        <div class="col-md-8 clearfix">
            <div class="grid-large">
                <div style="background:url('catalog/view/theme/<?php echo $theme_path; ?>/image/discover/2.jpg') no-repeat; background-size:cover;-webkit-background-size:cover;" class="right-col v2">
                    <div class="col-md-offset-2">
                        <div class="content">
                            <div class="item">
                                <h2>JUST IN Gucci By Gucci Pour Femme</h2><a href="#"> <img src="catalog/view/theme/<?php echo $theme_path; ?>/image/assets/arrow4.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 clearfix v2">
            <div class="right-colm">
                <div class="nos-col">
                    <div class="caption">
                        <h6>sale<span>50%</span></h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 clearfix v2">
            <div style="background:url('catalog/view/theme/<?php echo $theme_path; ?>/image/discover/3.jpg') no-repeat; background-size:cover;-webkit-background-size:cover;" class="right-colm">
                <div class="nos-col">
                    <div class="caption2">
                        <h4>recommend DESIGNER</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 clearfix v2">
            <div class="right-colm">
                <div class="nos-col">
                    <div class="caption3"><a href="#">
                            <h4>Nafsika Skourti</h4></a>
                        <h5>Amman, Jordan</h5>
                        <p>Nafsika Skourti is half greek, half jordanian and half internet. In 2012 she graduated from Central Saint Martins where she was trained, educated and sleep deprived. Before all this she worked at Marchesa in New York and also trained in haute couture embroidery and passementerie at Ecole Lesage in Paris.</p><a href="#" class="line">FOLLOW</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 clearfix v2">
            <div class="right-colm">
                <div class="nos-col">
                    <div class="caption4"><img src="catalog/view/theme/<?php echo $theme_path; ?>/image/discover/4.png" alt=""><a href="#">
                            <h4>NAFSIKA SKOURTI</h4></a><a href="#">
                            <p>Floral-Print Cut-Out Crepe Blazer</p></a>
                        <h6>$250</h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 clearfix v2">
            <div style="background:url('catalog/view/theme/<?php echo $theme_path; ?>/image/discover/5.jpg') no-repeat; background-size:cover;-webkit-background-size:cover;" class="right-colm">
                <div class="nos-col">
                    <div class="caption2"></div>
                </div>
            </div>
        </div>
        <div class="col-md-4 clearfix v2">
            <div class="right-colm">
                <div class="nos-col">
                    <div class="caption3 caption6"><a href="#">
                            <h4>The Business Behind the Billy Farrell Agency Lenses</h4></a>
                        <h5>Sep 10, 2015 ∙ <a href="">BY DANA KHATER</a></h5>
                        <p>It remains to be seen whether an agency renowned for its skill at event photography will be supported in so wide an expansion of its expertise and business-activities. That said, BFA has long-term relationships with its clients and its four founders have already stolen the New York…</p><a href="#" class="line">continue reading</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 clearfix v2">
            <div style="background:url('catalog/view/theme/<?php echo $theme_path; ?>/image/discover/6.jpg') no-repeat; background-size:cover;-webkit-background-size:cover;" class="right-colm">
                <div class="nos-col">
                    <div class="caption2"></div>
                </div>
            </div>
        </div>
        <div class="col-md-4 clearfix v2">
            <div style="background:url('catalog/view/theme/<?php echo $theme_path; ?>/image/discover/8.jpg') no-repeat; background-size:cover;-webkit-background-size:cover;" class="right-colm">
                <div class="nos-col">
                    <div class="caption7">
                        <h3>recommend ambassador</h3><a href="#">
                            <h4>Chiara Ferragni</h4></a>
                        <h5>MILAN, italy</h5><a href="#" class="line">FOLLOW</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 clearfix v2">
            <div class="right-colm">
                <div class="nos-col">
                    <div class="caption3 caption6"><a href="#">
                            <h4>The Business Behind the Billy Farrell Agency Lenses</h4></a>
                        <h5>Sep 10, 2015 ∙ <a href="">BY DANA KHATER</a></h5>
                        <p>It remains to be seen whether an agency renowned for its skill at event photography will be supported in so wide an expansion of its expertise and business-activities. That said, BFA has long-term relationships with its clients and its four founders have already stolen the New York…</p><a href="#" class="line">continue reading</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 clearfix v2">
            <div style="background:url('catalog/view/theme/<?php echo $theme_path; ?>/image/discover/7.jpg') no-repeat; background-size:cover;-webkit-background-size:cover;" class="right-colm">
                <div class="nos-col">
                    <div class="caption2"></div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php echo $footer; ?>
