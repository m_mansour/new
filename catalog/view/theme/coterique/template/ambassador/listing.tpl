<?php echo $header; ?>

<?php
global $config;
$theme_path = $config->get('config_template');

?>
<section class="section">	<!--section -->
    <div class="container founder-section version2">
        <div class="heading">
            <h3>Our amazing<span>Brand ambassadors</span></h3>
        </div>
        <div class="div ambassadors-list">
            <?php if($categories) { ?>
            <?php foreach  ($categories as $category) { ?>
            <?php
                 $cat_name = $category['name']  ;
                 $cat_limit = substr($cat_name, 0, 17);
             ?>
            <div class="col-md-3 col-sm-4 col-xs-6"><!-- item-->
                <div class="item"><img src="image/<?php echo $category['p_thumb']; ?>" alt="">
                    <div class="caption">
                        <div class="info">
                            <div class="info-wrap">
                                <a href="#<?php //echo $category['href']; ?>"><h3><?php echo $cat_limit ?></h3></a>
                                <h4><?php echo $category['job_title']; ?></h4>
                                <p><?php foreach ($zones as $zone) { ?>
                                    <?php if ($zone['zone_id'] == $category['zone_id']) { ?>
                                    <?php echo $zone['name']; ?>,
                                    <?php } ?>
                                    <?php } ?>

                                    <?php foreach ($countries as $country) { ?>
                                    <?php if ($country['country_id'] == $category['country_id']) { ?>
                                    <?php echo $country['name']; ?>
                                    <?php } ?>
                                    <?php } ?>

                                </p>
                            </div>
                            <div class="social-media1 clearfix">
                                <ul class="list-unstyled">
                                    <?php if($category['insta_url']){ ?>
                                    <li><a target="_blank" href="<?php echo $category['insta_url']; ?>"><img src="catalog/view/theme/coterique/image/insta.png" alt=""></a></li>
                                    <?php }?>
                                    <?php if($category['t_url']){ ?>
                                    <li><a target="_blank" href="<?php echo $category['t_url']; ?>"><img src="catalog/view/theme/coterique/image/tw.png" alt=""></a></li>
                                    <?php }?>
                                   <?php if($category['p_url']){ ?>
                                    <li><a target="_blank" href="<?php echo $category['p_url']; ?>"><img src="catalog/view/theme/coterique/image/pinterest.png" alt=""></a></li>
                                    <?php }?>
                                     <?php if($category['w_url']){ ?>
                                    <li><a target="_blank" href="<?php echo $category['w_url']; ?>"><img src="catalog/view/theme/coterique/image/site.png" alt=""></a></li>
                                     <?php }?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <?php } ?>
            <div class="div ambassadors-list"></div>

        </div>
    </div>
</section>
<?php echo $footer; ?>