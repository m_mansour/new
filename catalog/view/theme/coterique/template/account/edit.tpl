<?php echo $header; ?>
<section class="payofort-page account-mangment">
    <div class="caption-contact">
        <div class="container">
            <div class="col-md-12 crumb crumb-2 clearfix"><!--breadcrumb-->
                <ul class="list-unstyled">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
              <?php if ($error_warning) { ?>
              <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
              <?php } ?>


            <div class="col-md-3">
                <div  data-offset-top="10" data-offset-bottom="20" class="left-bar">
                    <h2>YOUR ACCOUNT</h2>
                    <ul class="list-unstyled">
                        <li><a href="index.php?route=account/edit" class="active">Profile information</a></li>
                        <li><a href="index.php?route=account/password">Change your password</a></li>
                        <li><a href="index.php?route=account/address">shipping information</a></li>
                        <li><a href="#order_history">order history</a></li>
                        <li><a href="#email_preferences">email preferences</a></li>
                        <li><a href="#people_you_follow">people you follow</a></li>
                    </ul><a href="index.php?route=account/logout" class="btn btn-primary btn-logout">Sign out</a>
                </div>
            </div>




            <div class="col-md-9">
                <div id="Profile_information" class="form-content clearfix"><!-- form-1-->
                    <div class="header clearfix">
                        <h3>Profile Information</h3>
                    </div>
                    <div class="col-md-3 content-profile">
                        <h3>Profile photo</h3>
                        <div class="img-wrap"><img src="catalog/view/theme/coterique/image/profile.jpg" alt=""></div>
                        <button class="btn upload">upload photo</button><a href="#" class="delete">Delete profile photo?</a>
                    </div>
                    <div class="col-md-9 col-left">
                        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label for="input-firstname"><?php echo $entry_firstname; ?></label>
                                    <input name="firstname" value="<?php echo $firstname; ?>" type="text" id="name" placeholder="<?php echo $entry_firstname; ?>" required class="form-control">
                                    <?php if ($error_firstname) { ?>
                                    <div class="text-danger"><?php echo $error_firstname; ?></div>
                                    <?php } ?>
                                </div>
                                <div class="col-md-6 col-left">
                                    <label for="input-lastname"><?php echo $entry_lastname; ?></label>
                                    <input type="text" id="last" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>" required class="form-control">
                                    <?php if ($error_lastname) { ?>
                                    <div class="text-danger"><?php echo $error_lastname; ?></div>
                                    <?php } ?>
                                </div>
                                <div class="col-md-6">
                                    <label for="input-email"><?php echo $entry_email; ?></label>
                                    <input type="text" id="email" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" required class="form-control">
                                    <?php if ($error_email) { ?>
                                    <div class="text-danger"><?php echo $error_email; ?></div>
                                    <?php } ?>
                                </div>
                                <div class="col-md-6 col-left">
                                    <label for="input-telephone"><?php echo $entry_telephone; ?></label>
                                    <input type="text" id="number" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" required class="form-control">
                                    <?php if ($error_telephone) { ?>
                                    <div class="text-danger"><?php echo $error_telephone; ?></div>
                                    <?php } ?>
                                </div>

                               <!-- <div class="col-md-6">
                                    <label for="password">password</label>
                                    <input type="text" id="password" placeholder="********" required class="form-control">
                                </div>
                                <div class="col-md-6 label-wrap"><a href="">
                                        <h4>Change your password?</h4></a></div> -->
                                <div class="col-md-12 col-left">
                                    <label for="name">prices currency </label>
                                    <div class="filter-options">
                                        <select class="selectpicker"><!--options1-->
                                            <option>United States Dollar</option>
                                            <option>currency1</option>
                                            <option>currency2</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12 col-left">
                                    <button class="btn btn-submit">save information</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>



        </div>
    </div>
</section>
<script type="text/javascript"><!--
// Sort the custom fields
$('.form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('.form-group').length) {
		$('.form-group').eq($(this).attr('data-sort')).before(this);
	}

	if ($(this).attr('data-sort') > $('.form-group').length) {
		$('.form-group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('.form-group').length) {
		$('.form-group:first').before(this);
	}
});
//--></script>
<script type="text/javascript"><!--
$('button[id^=\'button-custom-field\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$(node).parent().find('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});
//--></script>
<?php echo $footer; ?>