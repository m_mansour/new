<?php
echo $header;

global $config;
$theme_path = $config->get('config_template')
?>

<section class="main-banner-section banner3 sign-in">
    <div class="container">
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
        <?php } ?>
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
        <?php } ?>
        <div class="col-md-6 col-md-offset-3">
            <div class="tabs-content clearfix">
                <ul role="tablist" class="list-unstyled tabs-items">
<!--                    <li role="presentation"><a href="#signIn" aria-controls="signIn" role="tab" data-toggle="tab">SIGN UP</a></li>-->
                    <li role="presentation" class="active"><a href="#signUp" aria-controls="signUp" role="tab" data-toggle="tab">SIGN IN</a></li>
                </ul>
            </div>
            <div class="tab-content wrap-content clearfix">
                <div role="tabpanel" id="signUp" class="tab-pane active">
                    <h3> Already have an account? Sign In</h3>
                    <div class="col-md-10 col-md-offset-1"><!--form SignIn-->
                        <div class="form-content">
                            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                                <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-text" />
                                <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-text" />


                                <div class="forget"><a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a></div>


                                <?php if ($redirect) { ?>
                                <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
                                <?php } ?>


                                <button class="btn btn-block email" type="submit">
                                   <img class="icons" src="catalog/view/theme/coterique/image/assets/mail.png" alt="">
                                    SIGN IN 
                                </button>

                                <!--<h4>OR alternatively YOU CAN</h4><a href="#" class="btn btn-block fb">
                                    <div class="icon"><img src="catalog/view/theme/<?php /*echo $theme_path; */?>/image/assets/fb.png" alt=""></div>SIGN IN WITH Facebook</a>-->
                            </form>

                        </div>
                    </div>
                </div>
                <div role="tabpanel" id="signIn" class="tab-pane">
                    <h3> Sign Up and get access to offers, follow designers, build your wishlist and much more. It only takes one minute. Lorem ipsum dolor sit amet is text.</h3>
                    <div class="col-md-10 col-md-offset-1"><!--form SignIn-->
                        <div class="form-content">
                            <form action="">
                                <input type="text" placeholder="YOUR NAME…" class="form-text">
                                <input type="email" placeholder="YOUR email…" class="form-text">
                            </form><a href="#" class="btn btn-block email">
                                <div class="icon"><img src="imgs/assets/mail.png" alt=""></div>SIGN UP WITH EMAIL</a>
                            <h4>OR alternatively YOU CAN</h4><a href="#" class="btn btn-block fb">
                                <div class="icon"><img src="imgs/assets/fb.png" alt=""></div>SIGN UP WITH Facebook</a>
                            <div class="rights">
                                <h3>By Signing Up, you agree to our Terms of Service and Privacy Policy</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo $footer; ?>