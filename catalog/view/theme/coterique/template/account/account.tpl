<?php echo $header; ?>

<section class="payofort-page account-mangment">
    <div class="caption-contact">
        <div class="container">
            <div class="col-md-12 crumb crumb-2 clearfix"><!--breadcrumb-->
                <ul class="list-unstyled">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
            <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
            <?php } ?>

            <div class="col-md-3">
                <div  data-offset-top="10" data-offset-bottom="20" class="left-bar">
                    <h2>YOUR ACCOUNT</h2>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo $edit; ?>" class="active">Profile information</a></li>
                        <li><a href="<?php echo $password; ?>" class="active"><?php echo $text_password; ?></a></li>
                        <li><a href="<?php echo $text_address; ?>">shipping information</a></li>
                        <li><a href="#order_history">order history</a></li>
                        <li><a href="#email_preferences">email preferences</a></li>
                        <li><a href="#people_you_follow">people you follow</a></li>
                    </ul><a href="index.php?route=account/logout" class="btn btn-primary btn-logout">Sign out</a>
                </div>
            </div>

        </div>
    </div>
</section>

<?php echo $footer; ?>
