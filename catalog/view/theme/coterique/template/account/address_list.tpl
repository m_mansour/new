<?php echo $header; ?>
<section class="payofort-page account-mangment">
    <div class="caption-contact">
        <div class="container">
            <div class="col-md-12 crumb crumb-2 clearfix"><!--breadcrumb-->
                <ul class="list-unstyled">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
          <?php if ($success) { ?>
          <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
          <?php } ?>
          <?php if ($error_warning) { ?>
          <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
          <?php } ?>

            <div class="col-md-3">
                <div  data-offset-top="10" data-offset-bottom="20" class="left-bar">
                    <h2>YOUR ACCOUNT</h2>
                    <ul class="list-unstyled">
                        <li><a href="index.php?route=account/edit" >Profile information</a></li>
                        <li><a href="index.php?route=account/password">Change your password</a></li>
                        <li><a href="index.php?route=account/address" class="active">shipping information</a></li>
                        <li><a href="#order_history">order history</a></li>
                        <li><a href="#email_preferences">email preferences</a></li>
                        <li><a href="#people_you_follow">people you follow</a></li>
                    </ul><a href="index.php?route=account/logout" class="btn btn-primary btn-logout">Sign out</a>
                </div>
            </div>

            <div class="col-md-9">
            <div id="content" class="<?php echo $class; ?>"><?php // echo $content_top; ?>
              <h2><?php echo $text_address_book; ?></h2>
              <?php if ($addresses) { ?>
              <table class="table table-bordered table-hover">
                <?php foreach ($addresses as $result) { ?>
                <tr>
                  <td class="text-left"><?php echo $result['address']; ?></td>
                  <td class="text-right"><a href="<?php echo $result['update']; ?>" class="btn btn-info"><?php echo $button_edit; ?></a> &nbsp; <a href="<?php echo $result['delete']; ?>" class="btn btn-info"><?php echo $button_delete; ?></a></td>
                </tr>
                <?php } ?>
              </table>
              <?php } else { ?>
              <p><?php echo $text_empty; ?></p>
              <?php } ?>
              <div class="buttons clearfix">
                <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>
                <div class="pull-right"><a href="<?php echo $add; ?>" class="btn btn-primary"><?php echo $button_new_address; ?></a></div>
              </div>
              <?php // echo $content_bottom; ?></div>
            <?php //echo $column_right; ?></div>
        </div>
    </div>
</section>
<?php echo $footer; ?>