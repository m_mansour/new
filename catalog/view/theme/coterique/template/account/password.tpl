<?php echo $header; ?>
<section class="payofort-page account-mangment">
    <div class="caption-contact">
        <div class="container">

            <div class="col-md-12 crumb crumb-2 clearfix"><!--breadcrumb-->
                <ul class="list-unstyled">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                    <?php } ?>
                </ul>
            </div>

            <div class="col-md-3">
                <div  data-offset-top="10" data-offset-bottom="20" class="left-bar">
                    <h2>YOUR ACCOUNT</h2>
                    <ul class="list-unstyled">
                        <li><a href="index.php?route=account/edit" >Profile information</a></li>
                        <li><a href="index.php?route=account/password" class="active">Change your password</a></li>
                        <li><a href="index.php?route=account/address">shipping information</a></li>
                        <li><a href="#order_history">order history</a></li>
                        <li><a href="#email_preferences">email preferences</a></li>
                        <li><a href="#people_you_follow">people you follow</a></li>
                    </ul><a href="index.php?route=account/logout" class="btn btn-primary btn-logout">Sign out</a>
                </div>
            </div>
          <div class="col-md-9">
            <div id="content" class="<?php echo $class; ?>"><?php // echo $content_top; ?>
              <h1><?php echo $heading_title; ?></h1>
              <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                <fieldset>
                  <legend><?php echo $text_password; ?></legend>
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-password"><?php echo $entry_password; ?></label>
                    <div class="col-sm-10">
                      <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
                      <?php if ($error_password) { ?>
                      <div class="text-danger"><?php echo $error_password; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-confirm"><?php echo $entry_confirm; ?></label>
                    <div class="col-sm-10">
                      <input type="password" name="confirm" value="<?php echo $confirm; ?>" placeholder="<?php echo $entry_confirm; ?>" id="input-confirm" class="form-control" />
                      <?php if ($error_confirm) { ?>
                      <div class="text-danger"><?php echo $error_confirm; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                </fieldset>
                <div class="buttons clearfix">
                  <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-submit back"><?php echo $button_back; ?></a></div>
                  <div class="pull-right">
                      <button class="btn btn-submit" value="<?php echo $button_continue; ?>">save information</button>
                  </div>
                </div>
              </form>
              <?php //echo $content_bottom; ?>
            </div>
            <?php //echo  $column_right; ?></div>

        </div>
    </div>
</section>
<?php echo $footer; ?>