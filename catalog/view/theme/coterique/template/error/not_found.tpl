<?php echo $header; ?>


  <section class=" main-banner-section banner2 banner-tab banner-33">
    <div class="container">
      <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="header">
          <h3><?php echo $heading_title; ?></h3>
          <h1>Oh, Oops!</h1>
          <p>Unfortunately this page you’re looking for no longer exists, please click below to go to our homepage.</p><a href="<?php echo $continue; ?>"">GO TO HOMEPAGE</a>
        </div>
      </div>
    </div>
  </section>
  <section class="section search-404 clearfix"> <!--section -->
    <div class="container story">
      <div class="heading">
        <h3>Looking for <span>specific item?</span></h3>
      </div>
      <section class="container actions-content"><!--banner-section-->
        <div class="col-md-12 search-adesigner">
          <form action="">
            <input type="text" placeholder="Enter search criteria here…" required class="form-control">
            <div class="search btn">
              <button type="submit"><img src="catalog/view/theme/coterique/image/assets/search.png" alt=""></button>
            </div>
          </form>
        </div>
      </section>
    </div>
  </section>
  <section class="section search-404 clearfix"> <!--section -->
    <div class="container story">
      <div class="heading">
        <h3>or Continue <span>shopping</span></h3>
      </div>
      <section class="container grid-items-wraping"><!--banner-section-->
        <div class="col-md-12">
          <div class="col-md-4 clearfix hide-mob"> <!-- item-->
            <div class="img-wrap large1">
              <img src="catalog/view/theme/coterique/image/search1.jpg" alt="">
              <div class="caption">
                <h3><a href="">Shop TOPS ›</a></h3>
              </div>
            </div>

          </div>
          <div class="col-md-8 clearfix item22s">
            <div class="col-md-12"> <!--item-->
              <div class="img-wrap large2">
                <img src="catalog/view/theme/coterique/image/search2.jpg" alt="">
                <div class="caption">
                  <h3><a href="">Shop outerwear ›</a></h3>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-xs-6 left"> <!--item-->
              <div class="img-wrap large3">
                <img src="catalog/view/theme/coterique/image/search3.jpg" alt="">
                <div class="caption">
                  <h3><a href="">Shop jackets ›</a></h3>
                </div>
              </div>
            </div>
            <div class="col-md-6  col-xs-6 right"><!--item-->
              <div class="img-wrap large3">
                <img src="catalog/view/theme/coterique/image/search4.jpg" alt="">
                <div class="caption">
                  <h3><a href="">Shop Dresses ›</a></h3>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12 full-width">
          <div class="col-md-12">
            <div class="img-wrap large4">
              <img src="catalog/view/theme/coterique/image/search6.jpg" alt="">
              <div class="caption">
                <h3><a href="">Shop leather accessories ›</a></h3>
              </div>
            </div>
          </div>

        </div>
      </section>
    </div>
  </section>



<?php echo $footer; ?>