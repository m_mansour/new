<?php echo $header; ?>


<?php

global $config;
$theme_path = $config->get('config_template');

?>

<section class="container actions-content"><!--banner-section-->
    <div class="col-md-9 col-sm-9 col-xs-9 search-adesigner" id="search-d">

            <input name="search-d" type="text" placeholder="Search designers…" required class="form-control">
            <div class="search btn">
                <button type="button"><img src="catalog/view/theme/coterique/image/assets/search.png" alt=""></button>
            </div>

    </div>
    <div role="tablist" class="col-md-3 col-sm-3 col-xs-3 toggle-grid-style">
        <a href="#grid1" aria-controls="grid1" role="tab" data-toggle="tab" class="btn"><img src="catalog/view/theme/coterique/image/assets/grid.png" alt=""><span class="hide-mob">GRID</span></a>
        <a href="#list1" aria-controls="list1" role="tab" data-toggle="tab" class="btn"><img src="catalog/view/theme/coterique/image/assets/list.png" alt=""><span class="hide-mob">List		</span></a>
    </div>

</section>
<section class="wrap-list-designer-content">
    <div class="container tab-content">
        <div role="tabpanel" id="grid1" class="tab-pane active">

            <?php if($categories) { ?>
            <?php foreach  ($categories as $category) { ?>
            <div class="col-md-3 col-sm-4 col-xs-6 same-height"><!--item-->
                <a href="<?php echo $category['href']; ?>">
                  <div class="item">
                      <?php // echo $category['p_thumb']; ?>
                    <div class="img-wrap">
                        <?php if($category['p_thumb']){ ?>
                         <img src="image/<?php echo $category['p_thumb']; ?>" alt="">
                        <?php }else{ ?>
                            <img src="image/default-thumb-des.jpg" alt="">
                       <?php } ?>

                    </div>
                    <div class="content">
                  <?php if($category['country_id']){ ?>
                        <?php foreach ($countries as $country) { ?>
                            <?php if ($country['country_id'] == $category['country_id']) { ?>
                                <div class="sprite sprite-<?php echo $country['country_id']; ?> extra"></div>
                            <?php }
                          }
                    }else{echo "<div class='flags'></div>" ;} ?>

                        <a href="<?php echo $category['href']; ?>"><h4><?php echo $category['name']  ?></h4></a>
                        <?php if($category['country_id']){ ?>
                        <h5>
                            <?php foreach ($zones as $zone) { ?>
                                <?php if ($zone['zone_id'] == $category['zone_id']) { ?>
                                    <?php echo $zone['name']; ?>,
                                <?php } ?>
                            <?php } ?>

                            <?php foreach ($countries as $country) { ?>
                                <?php if ($country['country_id'] == $category['country_id']) { ?>
                                    <?php echo $country['name']; ?>
                                <?php } ?>
                                <?php } ?>

                        </h5>
                        <?php }else{echo "<h5>No Country</h5>";}?>
                        <a href="<?php echo $category['href']; ?>" class="line">View Details</a>
                    </div>
                </div>
                 </a>
            </div>
            <?php } ?>
            <?php } ?>
        </div>
        <div role="tabpanel" id="list1" class="list-view-content clearfix tab-pane ">

            <?php

                if($categories){
                    $catList = [];
                    foreach  ($categories as $category) {
                        $firstLetter = substr($category['name'],0,1);
                        if(!isset($catList[$firstLetter])){
                           $catList[$firstLetter] = [];
                        }
                        $catList[$firstLetter][]=$category ;
                    }

                }
            ?>

            <div class="col-md-3 clearfix"><!-- item-->
                <?php $i=0 ; foreach($catList as $k => $cats){ $i++ ?>
                    <div class="item11"><!--package-->
                        <div class="head">
                            <h3><?php echo $k ; ?></h3>
                        </div>
                        <div class="content">
                            <ul class="list-unstyled">
                            <?php foreach($cats as $cat){ ?>
                                <li><a href="<?php echo $cat['href']; ?> "><?php echo $cat['name']; ?> </a></li>
                            <?php } ?>
                            </ul>
                        </div>
                    </div>
                <?php if (in_array($i,[4,11,16]) ){ ?>
               <?php  echo "</div><div class='col-md-3 clearfix'>" ; ?>
                   <?php }  ?>

                <?php } ?>


            </div>

        </div>
    </div>
</section>

<?php echo $footer; ?>
