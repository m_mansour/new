<?php echo $header; ?>


    <section class="main-banner-section banner3">
      <div class="container">
        <div class="col-md-6 col-md-offset-3">
          <div class="header">
            <h3><?php echo $heading_title; ?></h3>
            <h1>Thank you!</h1>
            <p><?php echo $text_message; ?>.</p>
            <a href="<?php echo $continue; ?>">GO TO HOMEPAGE			</a>
          </div>
        </div>
      </div>
    </section>


  

<?php echo $footer; ?>