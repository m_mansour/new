<?php if (count($currencies) > 1) { ?>
<div class="pull-left curr">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="currency">
        <div class="btn-group">
            <button class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                <?php foreach ($currencies as $currency) { ?>


                <?php  if ($currency['symbol_right'] && $currency['code'] == $code) { ?>
                        <?php  if ($currency['code'] == "EGP") { ?>
                            <img id="cur" src="catalog/view/theme/coterique/image/egypt.png" class="flags <?php echo strtolower(substr($currency['code'], 0, 2)); ?>"/>
                        <?php }else{ ?>
                            <img id="cur" src="catalog/view/theme/coterique/image/usa.png" class="flags <?php echo strtolower(substr($currency['code'], 0, 2)); ?>"/>

                        <?php } ?>
                        &nbsp;
                <span class="hidden-xs hidden-sm "><?php echo $currency['title']; ?></span> <i class="fa fa-caret-down"></i>
                <?php } ?>
                <?php } ?>
            </button>
            <ul class="dropdown-menu">
                <?php foreach ($currencies as $currency) { ?>
                <?php if ($currency['symbol_left']) { ?>
                <li class="count-d">
                    <button class="currency-select btn btn-link btn-block" type="button" name="<?php echo $currency['code']; ?>">
                        <img id="cur" src="catalog/view/theme/coterique/image/usa.png" class="flags <?php echo strtolower(substr($currency['code'], 0, 2)); ?>"/>
                        <?php echo $currency['symbol_left']; ?> <?php echo $currency['title']; ?>
                    </button>
                </li>
                <?php } else { ?>
                <li class="count-d">
                    <button class="currency-select btn btn-link btn-block" type="button" name="<?php echo $currency['code']; ?>">
                        <?php //echo $currency['symbol_right']; ?>
                        <?php  if ($currency['code'] == "EGP") { ?>
                        <img id="cur" src="catalog/view/theme/coterique/image/egypt.png" class="flags <?php echo strtolower(substr($currency['code'], 0, 2)); ?>"/>
                         <?php }else{ ?>
                            <img id="cur" src="catalog/view/theme/coterique/image/usa.png" class="flags <?php echo strtolower(substr($currency['code'], 0, 2)); ?>"/>

                        <?php } ?>
                         <?php echo $currency['title']; ?>
                    </button>
                </li>
                <?php } ?>
                <?php } ?>
            </ul>
            <input type="hidden" name="code" value="" />
            <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
        </div>

    </form>
</div>
<?php } ?>