<?php
echo $header;
global $config;
$theme_path = $config->get('config_template')
?>

<section class="main-banner-section">

    <?php echo $slideshow ; ?>
<!--    <div class="container">-->
<!--        <div class="col-md-6 col-md-offset-3">-->
<!--            <div class="header">-->
<!--                <h3>JUST ARRIVED</h3>-->
<!--                <h1>Lama Jouni</h1>-->
<!--                <div class="hidden-xs">-->
<!--                    <p>An inspired blend of sleek lines and bold empty space, this collection fuses polished modernity with hints of sci-fi. </p>-->
<!--                </div><a href="index.php?route=product/category&path=_484">SHOP NOW			</a>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
</section>

<?php if ($featured){ ?>

<section class="bannerSecond clearfix" style="background:url('image/<?php echo $full_bg; ?>') 50% 50% no-repeat;background-size:cover; -webkit-background-size:cover;">	<!--section -->
    <div class="shadow-home">
        <div class="item  hiddex-sm hidden-xs"></div>
        <div class="item ">
           <?php //foreach ($categories as $category) { ?>
            <div class="content">
                <h2>Featured Designer of the week</h2>
                <div class="info">
                    <h3><?php echo $heading_title; ?></h3>
                    <p><?php echo $description ; ?>.</p>

                    <div class="place clearfix" style="width: 200px">
                        <div class="sprite sprite-<?php echo $country_id ?> flag-product"></div>
    <!--                    <div class="img-wrap"><img src="catalog/view/theme/--><?php //echo $theme_path; ?><!--/image/flag-place.jpg" alt=""></div>-->
                        <h4><span style="font-weight: bold"><?php echo $zone_name ?>,</span><span><?php echo $country_name ?></span></h4>
                    </div>
                    <div class="footer"><a href="<?php echo $url ?>" class="line">READ AND SHOP	</a></div>
                </div>
            </div>
            <?php //} ?>
        </div>
        <div class="item  hiddex-sm hidden-xs"></div>
    </div>
</section>

    <?php } ?>


<section class="section grid-section clearfix">	<!--section -->
    <div class="container">
        <div class="col-md-12 banner-1">
          <?php echo $content_top; ?>
        </div>
        <div class="col-md-4 banner-3">

            <?php echo $column_left; ?>
        </div>
        <div class="col-md-4">


            <?php echo $middel; ?>
        </div>
        <div class="col-md-4 banner-2">
            <?php echo $column_right; ?>

        </div>
    </div>
</section>
<section class="section">	<!--section -->
    <div class="container founder-section version2">
        <div class="heading">
            <h3>CotiGirl picks<span>of the week</span></h3>
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="founder-info clearfix">
                    <div class="img-wrap"><img src="image/<?php echo $p_image; ?>" alt=""></div>
                    <div class="info">
                        <h3>This week’s picks by</h3>
                        <h2>~<?= $cot_name ?></h2>
                        <h4><?= $job_title ?></h4>
                    </div>
                </div>
            </div>
        </div>
        <?php echo $content_bottom ; ?>

    </div>
</section>

<?php

$user_id="336723211";
$access_token = "336723211.4b6f0d9.28711270b5c84f8d9c17a8d4ba6054a0";

//$url = "https://api.instagram.com/v1/users/".$user_id."/media/recent?access_token=".$access_token;

$url = "https://www.instagram.com/coterique/media/";

$ch = curl_init($url);

curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

$json = curl_exec($ch);
curl_close($ch);
$result = json_decode($json);

?>
<section class="section clearfix instagram-api">	<!--section -->
    <div class="container">
        <div class="heading">
            <h3>FOLLOW US ON<span>Instagram</span></h3>
        </div>
    </div>
    <div class="wrap-insta">
        <ul class="list-unstyled">
            <?php $i=0 ; foreach ($result->items as $post) { ?>
            <li class="item">
                <a href="<?= $post->link ?>"  target="_blank">

                    <img src="<?php echo $post->images->low_resolution->url;?>" alt="<?php echo $post->caption->text;?>">
                    <div class="caption"></div>
                </a>
            </li>
            <?php  if ($i++ == 13) break; ?>
            <?php } ?>
        </ul>
    </div>
</section>


<?php echo $footer; ?>