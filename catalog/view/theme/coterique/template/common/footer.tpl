<footer class="main-footer">
    <div class="container">
        <div class="col-md-2 hide-tab">
            <?php if ($informations) { ?>
            <div class="head">
                <h3>COTERIQUE</h3>
                <ul class="list-unstyled">
                    <li><a href="index.php?route=information/information&information_id=4">About Us</a></li>
                    <li><a href="index.php?route=ambassador/listing">Our CotiGirls</a></li>

                  <!--  <li><a href="#">About Us</a></li>

                    <li><a href="#">Careers</a></li>
                    <li><a href="#">Terms & Conditions</a></li>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Privacy Policy</a></li> -->

                </ul>
            </div>
            <?php } ?>
        </div>
        <div class="col-md-2 col-md-offset-1 hide-tab">
            <div class="head">
                <h3>FAQ</h3>
                <ul class="list-unstyled">
                    <li><a href="index.php?route=information/information&information_id=10#shipping">Shipping Information</a></li>
                    <li><a href="index.php?route=information/information&information_id=10#payment">Payment Info</a></li>
                    <li><a href="index.php?route=information/information&information_id=10#account">Account Info</a></li>
<!--                    <li><a href="#">Gift Cards</a></li>-->
<!--                    <li><a href="index.php?route=information/information&information_id=10">FAQs</a></li>-->
                </ul>
            </div>
        </div>
        <div class="col-md-2 col-md-offset-1 footer-tab">
            <div class="head">
                <h3 class="hide-tab">contact us</h3>
                <ul class="list-unstyled">
                    <li style="color: #fff">+201007808080</li>
                    <li><a href="mailto:info@coterique.com">info@coterique.com</a></li>
<!--                    <li class="hide-tab"><a href="#">Live Support</a></li>
                    <li class="hide-tab"><a href="#">Apply as a designer</a></li>
                    <li class="hide-tab"><a href="#">Apply as a boutique</a></li>-->
                    <li class="hide-tab"><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>

                </ul>
            </div>
        </div>
        <div class="col-md-3 col-md-offset-1 form-wrap footer-tab col-sm-6 col-sm-offset-2">
            <div class="head">
                <h3>Stay in touch</h3>
                <form action="//coterique.us3.list-manage.com/subscribe/post?u=82141deca0008eda692248fb6&amp;id=91bc4cfd54" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate form-inline" target="_blank" novalidate>
                    <div class="form-group">
                        <input type="email" name="EMAIL" id="mce-EMAIL" placeholder="Your email…" required class="form-control">
                        <button type="submit" id="mc-embedded-subscribe" class="btn btn-deafult">SIGN UP</button>
                    </div>
                </form>
                <div class="social-media clearfix">
                    <ul class="list-unstyled">
                        <li><a href="https://www.instagram.com/coterique/"><img src="catalog/view/theme/coterique/image/insta.png" alt=""></a></li>
                        <li><a href="https://www.facebook.com/Coterique/"><img src="catalog/view/theme/coterique/image/fb.png" alt=""></a></li>
                        <li><a target="_blank" href="https://twitter.com/Coterique"><img src="catalog/view/theme/coterique/image/tw.png" alt=""></a></li>

<!--                        <li><a href="#"><img src="catalog/view/theme/coterique/image/pinterest.png" alt=""></a></li>-->
<!--                        <li><a href="#"><img src="catalog/view/theme/coterique/image/gplus.png" alt=""></a></li>-->
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="rights">
        <h4>Copyright © 2016. Coterique, LLC. All rights reserved.Crafted with love in Cairo.</h4><img src="catalog/view/theme/coterique/image/rights.jpg" alt="">
    </div>
</footer>
<!--<script src="catalog/view/theme/coterique/js/bootstrap.min.js"></script> -->
<script src="catalog/view/theme/coterique/js/owl.js"></script>
<script src="catalog/view/theme/coterique/js/select.js"></script>
<script src="catalog/view/theme/coterique/js/classie.js"></script>
<script src="catalog/view/theme/coterique/js/script.js"></script>


<!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
    window.__lc = window.__lc || {};
    window.__lc.license = 7267351;
    (function() {
        var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
        lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
    })();
</script>
<!-- End of LiveChat code -->

</body>
</html>
