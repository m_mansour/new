<div id="cart" class="btn-group btn-block">
  <button type="button" data-toggle="dropdown" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-inverse btn-block btn-lg dropdown-toggle">
    <img src="catalog/view/theme/coterique/image/cart.png" alt="" /> <span id="cart-total"><?php echo $text_items; ?></span>
  </button>
  <ul class="dropdown-menu pull-right cart-holder">
    <?php if ($products || $vouchers) { ?>
      <li>
        <div class="items ">
          <div class="head">
            <h4>Shopping Bag</h4>
          </div>

          <?php foreach ($products as $product) { ?>
            <div  class="item clearfix"> <!--item-->
              <?php if ($product['thumb']) { ?>
                <div class="img-wrap">
                  <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>">
                </div>
              <?php } ?>
              <div class="info">
                <!--  <a href="#"><h6>MARC JACOBS</h6> </a> -->
                <a href="<?php echo $product['href']; ?>"><p><?php echo $product['name']; ?></p></a>

                <h5><?php echo $product['total']; ?></h5>
                <button type="button" onclick="cart.remove('<?php echo $product['cart_id']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs removee line">REMOVE</button>

              </div>
            </div>
          <?php } ?>
        </div>
      </li>

      <li>
        <div>
          <div class="check_out_action ">
            <?php foreach ($totals as $total) { ?>
              <h4><?php echo $total['title']; ?> <span><?php echo $total['text']; ?></span></h4>
            <?php } ?>
            <a href="<?php echo $cart; ?>" class="btn btn-block btn1"><?php echo $text_cart; ?></a>
            <a href="<?php echo $checkout; ?>" class="btn btn-block btn2"><?php echo $text_checkout; ?></a>
          </div>
      </li>
    <?php } else { ?>
      <li>
        <p class="text-center"><?php echo $text_empty; ?></p>
      </li>
    <?php } ?>
  </ul>
</div>
