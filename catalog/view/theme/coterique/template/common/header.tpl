<?php
global $config;
$theme_path = $config->get('config_template')

?>
<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
    <script src="catalog/view/theme/coterique/js/modernizr.js"></script>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/coterique/stylesheet/stylesheet.css" rel="stylesheet">
    <link href="catalog/view/theme/coterique/stylesheet/edits.css" rel="stylesheet">
  <link href="catalog/view/theme/coterique/stylesheet/flags.css" rel="stylesheet">
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>

<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>

    <meta property="og:image" content="<?php echo HTTP_SERVER ;?>catalog/view/theme/coterique/image/banner.jpg" />
    <meta property="og:url"   content="<?php echo HTTP_SERVER ;?>" />
    <meta property="og:type"  content="website" />
    <meta property="og:title"  content="<?php echo $title; ?>" />
    <meta property="og:description"   content="<?php echo $description; ?>" />

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','https://connect.facebook.net/en_US/fbevents.js');

        fbq('init', '537862976372426');
        fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=537862976372426&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body class="cbp-spmenu-push">

    <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
    <ul class="list-unstyled">
        <li class="dropdown country">

            <?php echo $currency; ?>
        </li>

        <li>
            <a href="https://coterique.com">Home</a>
        </li>



        <li class="search">
            <?php echo $search ?>
        </li>
        <div id="cssmenu">
            <li class='has-sub '>
                <a >Sign In / Sign Up<span><img src="catalog/view/theme/coterique/image/assets/dr.png" alt=""></span>

                </a>
                <ul>
                    <li><a href='index.php?route=account/login'>Sign In</a></li>
                    <li><a href='index.php?route=account/register'>Sign Up</a></li>
                </ul>
            </li>

        </div>
        <?php foreach ($categories as $category) { ?>
        <?php if ($category['children']) { ?>
            <div id="cssmenu">
                <li class='has-sub '>
                    <a href='<?php echo $category['href']; ?>'><?php echo $category['name']; ?><span><img src="catalog/view/theme/coterique/image/assets/dr.png" alt=""></span>

                    </a>
            <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>

                <ul>
                <?php $i = 0 ; foreach ($children as $child) {$i++; ?>
                        <li><a href='<?php echo $child['href']; ?>'><?php echo $child['name']; ?></a></li>
                    <?php  if($i == 4) {
                        break;
                    }?>
                 <?php } ?>

                    <?php if ($category['name'] == "DESIGNERS"){ ?>
                        <li><a href="index.php?route=designer/listing">ALL DESIGNERS </a></li>
                    <?php }elseif($category['name'] == "CLOTHING"){ ?>
                        <li><a href="clothing">ALL Clothing </a></li>


                    <?php } ?>

                </ul>
            <?php }  ?>

                </li>
            </div>
            <?php }  ?>
        <?php } ?>

        <li>
            <a  href="cotibeach">Cotibeach</a>
        </li>
        <li>
            <a href="index.php?route=information/contact">Contact Us</a>
        </li>


    </ul>
</nav>
<!--<header>-->
<section class="mainbanner" data-spy="affix" data-offset-top="60" data-offset-bottom="200"><!-- banner-1-->
    <div class="container">
        <button class="navbar-toggle tog collapsed" id="showLeftPush"></button>
        <div class="left-side">

            <?php if ($logo) { ?>
            <div class="logo-wrap">
                <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" alt="" title="<?php echo $name; ?>"></a>
            </div>
            <?php } else { ?>
            <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
            <?php } ?>
        </div>
        <div class="right-side">
            <ul class="list-unstyled">
                <li class="dropdown search"><?php echo $search ?></li>

                <?php if ($logged) { ?>
                <li class="dropdown country">
                    <a href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
<!--                        <img src="catalog/view/theme/--><?php //echo $theme_path; ?><!--/image/person.jpg" alt="">-->
                        <?php echo $customer_firstname; ?><span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo $account; ?>"> <?php echo $text_account; ?></a></li>
                        <li><a href="<?php echo $order; ?>"> <?php echo $text_order; ?></a></li>
                        <li><a href="<?php echo $logout; ?>"> <?php echo $text_logout; ?></a></li>

                    </ul>
                </li>
                <?php } else { ?>
                <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
                <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
                <?php } ?>
                <?php echo $currency; ?>
                <li id="Target_Cart" class="cart-wrap"><?php echo $cart; ?></li>

               </ul>
           </div>
       </div>
   </section>
<script>
 /*   $(document).ready(function () {
        $('#text_items').html(text_items);

    });*/
</script>

<!--       </header>-->
   <section class="menu2">
       <nav class="navbar navbar-default"><!-- Mainmenu-->
        <div class="container">
            <div class="navbar-header">
<!--                <button type="button" data-toggle="collapse" data-target="#nav-toogle" aria-expanded="false" class="navbar-toggle collapsed"></button>-->
            </div>
            <div id="nav-toogle" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                  <li><a href="index.php?route=product/special">Just In</a></li>


                    <?php foreach ($categories as $category) { ?>
                    <?php if ($category['children']) { ?>

                    <li class="dropdown">
                        <a href="<?php if($category['name'] == 'DESIGNERS'){echo 'index.php?route=designer/listing'; }else{ echo $category['href'];} ?>" class="dropdown-toggle"  role="button" aria-haspopup="true" aria-expanded="false"><?php echo $category['name']; ?></a>
                        <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                        <ul class="dropdown-menu" style=" width: 100%;   box-sizing: border-box;">
                            <div class="fullscreen-dropmenu clearfix" id="">
                                <div class="content">

                            <?php $x = 0 ; foreach ($children as $child) { ?>

                                    <?php if($child['thumb']){ ?>
                                    <div class="col-md-2">
                                        <div class="img-wrap">
                                            <?php  if($child['thumb']){ ?>
                                    <!--
                                                <?php echo"--test--".$child['thumb']; ?>
                                        -->        <img src="<?php echo  $child['thumb'];?>" alt="">
                                            <?php }else{ ?>
                                                <img src="image/menu.jpg" alt="">
                                            <?php } ?>
                                        </div>
                                        <a href="<?php echo $child['href']; ?>"><h3><?php echo $child['name']; ?></h3></a>
                                        <a class="line" href="<?php echo $child['href']; ?>">SHOP NOW</a>
                                    </div>
                                    <?php
                                     $x++;
                                     if($x == 4)
                                        break;
                                    ?>

                                <?php }?>

                                <?php } ?>
                                    <div class="col-md-2">
                                        <ul class="list-unstyled">
                                            <h4>By Name</h4>
                                        <?php $i = 0 ;  foreach ($children as $child) { $i++;



                                            ?>


                                            <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>

                                        <?php  if($i == 6) {
                                            break;
                                        }?>
                                        <?php }?>
                                            <?php if ($category['name'] == "DESIGNERS"){ ?>
                                                 <li><a href="index.php?route=designer/listing">ALL DESIGNERS </a></li>
                                             <?php }elseif($category['name'] == "CLOTHING"){ ?>
                                                <li><a href="clothing">ALL Clothing </a></li>


                                            <?php } ?>
                                        </ul>
                                    </div>



                                </div>
                            </div>
                        </ul>
                        <?php } ?>
                    </li>
                    <?php }  ?>
                    <?php } ?>
                    <li><a href="sale">Sale</a></li>

                </ul>
            </div>
        </div>
    </nav>
</section>

<!-- gogo -->
