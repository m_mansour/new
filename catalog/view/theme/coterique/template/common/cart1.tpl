
<script>
    text_items = "<?php echo $text_items; ?>";
</script>
<!-- mine -->
<div class="container" id="cart" >
    <!--CART DROPDOWN MENU -->
    <div id="homsa"  class="col-md-4 col-sm-12 ContenT_Menu">

             <div  class="cart_dropdown_menu clearfix ok">


                    <div class="items ">
                        <div class="head">
                            <h4>Shopping Bag</h4>
                        </div>
                        <?php if ($products || $vouchers) { ?>
                        <?php foreach ($products as $product) { ?>
                        <div  class="item clearfix"> <!--item-->
                            <?php if ($product['thumb']) { ?>
                            <div class="img-wrap">
                                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>">
                            </div>
                            <?php } ?>
                            <div class="info">
                              <!--  <a href="#"><h6>MARC JACOBS</h6> </a> -->
                                <a href="<?php echo $product['href']; ?>"><p><?php echo $product['name']; ?></p></a>

                                <h5><?php echo $product['total']; ?></h5>
                                <button type="button" onclick="carts.remove('<?php echo $product['cart_id']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs removee line">REMOVE</button>

                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    

                    <div class="check_out_action ">
                        <?php foreach ($totals as $total) { ?>
                        <h4><?php echo $total['title']; ?> <span><?php echo $total['text']; ?></span></h4>
                        <?php } ?>
                        <a href="<?php echo $cart; ?>" class="btn btn-block btn1"><?php echo $text_cart; ?></a>
                        <a href="<?php echo $checkout; ?>" class="btn btn-block btn2"><?php echo $text_checkout; ?></a>
                    </div>


                    <?php }else{ ?>

                    <p class="text-center"><?php echo $text_empty; ?></p>

                    <?php } ?>

        </div>

    </div>
</div>
