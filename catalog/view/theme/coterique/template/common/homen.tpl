<?php
echo $header;
global $config;
$theme_path = $config->get('config_template')
?>

<section class="main-banner-section">
    <div class="container">
        <div class="col-md-6 col-md-offset-3">
            <div class="header">
                <h3>JUST ARRIVED</h3>
                <h1>Isabel Sanchis</h1>
                <div class="hidden-xs">
                    <p>Dresses featuring white ruffles tailored to perfection, with an optional belt to break in and show your waist.</p>
                </div><a href="#">SHOP NOW			</a>
            </div>
        </div>
    </div>
</section>



<section class="bannerSecond clearfix">	<!--section -->
    <div class="item item1 hiddex-sm hidden-xs"></div>
    <div class="item item2">
       <?php //foreach ($categories as $category) { ?>
        <div class="content">
            <h2>Featured Designer of the week</h2>
            <div class="info">
                <h3><?php echo $heading_title; ?></h3>
                <p><?php echo $description; ?></p>
                <div class="place clearfix">
                    <div class="img-wrap"><img src="catalog/view/theme/<?php echo $theme_path; ?>/image/flag-place.jpg" alt=""></div>
                    <h4><?php echo $zone_name ?>,<span><?php echo $country_name ?></span></h4>
                </div>
                <div class="footer"><a href="<?php echo $url ?>" class="line">READ AND SHOP	</a></div>
            </div>
        </div>
        <?php //} ?>
    </div>
    <div class="item item3 hiddex-sm hidden-xs"></div>
</section>


<section class="section grid-section clearfix">	<!--section -->
    <div class="container">
        <div class="col-md-12">
            <div style="background:url(catalog/view/theme/<?php echo $theme_path; ?>/image/grid1.jpg) no-repeat;width:100%; height:500px;" class="item item-lg">
                <h2>#COTIBEACH SALE UPto 50% on swimwear</h2>
            </div>
        </div>
        <div class="col-md-4">
            <div style="background:url(catalog/view/theme/<?php echo $theme_path; ?>/image/grid2.jpg) no-repeat;width:100%; height:170px;" class="item item-sm1">
                <div class="caption">
                    <h3>Designer name</h3><a href="#">SHOP NOW ›</a>
                </div>
            </div>
            <div style="background:url(catalog/view/theme/<?php echo $theme_path; ?>/image/grid3.jpg) no-repeat;width:100%; height:300px;" class="item item-sm1">
                <div class="caption caption-2">
                    <h2>#COTIBEACH SALE UPto 50% on swimwear</h2>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div style="position:relative;background:url(catalog/view/theme/<?php echo $theme_path; ?>/image/grid4.jpg) no-repeat;width:100%; height:500px;" class="item item-sm1">
                <div class="caption3">
                    <h2>Lorem ipsum dolor is some title here</h2>
                    <p>Dresses featuring white ruffles tailored to perfection, with an optional belt to break in.</p><a href="#">SHOP NOW ›</a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div style="background:url(catalog/view/theme/<?php echo $theme_path; ?>/image/grid5.jpg) no-repeat; width:100%; height:300px;color:#000;" class="item item-sm1">
                <div class="caption caption-2">
                    <h2>#COTIBEACH SALE UPto 50% on swimwear</h2>
                </div>
            </div>
            <div style="background:url(imgs/grid6.jpg) no-repeat; width:100%; height:170px;" class="item item-sm1">
                <div class="caption">
                    <h3>Designer name</h3><a href="#">SHOP NOW ›</a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section">	<!--section -->
    <div class="container founder-section version2">
        <div class="heading">
            <h3>ambassador picks<span>of the week</span></h3>
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="founder-info clearfix">
                    <div class="img-wrap"><img src="catalog/view/theme/<?php echo $theme_path; ?>/image/per1.jpg" alt=""></div>
                    <div class="info">
                        <h3>This week’s picks by</h3>
                        <h2>~Alexander Wang</h2>
                        <h4>Creative Director, Balenciaga</h4>
                    </div>
                </div>
            </div>
        </div>
        <?php echo $content_bottom ; ?>
    </div>
</section>

<?php

$user_id="1282056625";
$access_token = "1282056625.1654d0c.08362e29f37f49b3a5036e12afd7a06f";

$url = "https://api.instagram.com/v1/users/".$user_id."/media/recent?access_token=".$access_token;

$ch = curl_init($url);

curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

$json = curl_exec($ch);
curl_close($ch);
$result = json_decode($json);

?>
<section class="section clearfix instagram-api">	<!--section -->
    <div class="container">
        <div class="heading">
            <h3>FOLLOW US ON<span>Instagram</span></h3>
        </div>
    </div>
    <div class="wrap-insta">
        <ul class="list-unstyled">
            <?php  foreach ($result->data as $post) { ?>
            <li class="item"><img src="<?php echo $post->images->low_resolution->url;?>" alt="<?php echo $post->caption->text;?>">
                <div class="caption"></div>
            </li>
            <?php if ($i++ == 13) break; ?>
            <?php } ?>
        </ul>
    </div>
</section>


<?php echo $footer; ?>