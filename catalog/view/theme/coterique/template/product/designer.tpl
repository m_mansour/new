<?php
echo $header;

?>
<?php if($fulls){ ?>
<section style="background:url('image/<?php echo $fulls; ?>') 50% 50% no-repeat;background-size:cover; -webkit-background-size:cover;" class="sub-banners"><!--banner-section-->
<?php }else{ ?>
 <section  style="background:url('image/default-cover.jpg') 50% 50% no-repeat;background-size:cover; -webkit-background-size:cover;" class="sub-banners"><!--banner-section-->

   <?php } ?>
    <div class="caption">
        <div class="container">
            <h1><?php echo $heading_title; ?></h1>


            <button class="line">FOLLOW</button>
        </div>


    </div>


</section>
<div id="content"  class="designer-story clearfix"><!--Designer Story-->
    <div class="grid col-md-4">
        <div class="country-image bg-<?php echo $country_id; ?>">
            <div class="caption">

                <div class="sprite sprite-<?php echo $country_id; ?> flag"></div>
                <div class="info">
                    <h2><?php echo $zone_name; ?></h2>
                    <p><?php echo $country_name; ?></p>
                </div>
            </div>
        </div>
    </div>


    <div class="grid col-md-5">
        <div class="content-info">
            <h3>story of the designer</h3>
            <p><?php echo substr($description,0,600) ; ?></p>
<!--            <a href="#" class="line">read full interview</a>-->
        </div>
    </div>


    <div class="grid col-md-3">
        <?php
                foreach ($products as $product) {
                    if ($product['tag'] == "featured") {
                        ?>

                        <div class="item-1"><img src="<?php echo $product['featured_image']; ?>" alt="">
                            <h4>featured product</h4>
                            <a href="<?php echo $product['href'] ?>">
                                <p><?php echo $product['name'] ?></p>
                            </a>
                            <?php if ($product['price']) { ?>
                                <?php if (!$product['special']) { ?>
                                    <b style="display: block">  <?php echo $product['price']; ?></b>
                                <?php } else { ?>
                                    <b class="price-new" style="display: block"><?php echo $product['special']; ?></b> <b class="price-old"><?php echo $product['price']; ?></b>
                                <?php } ?>
                                <?php if ($product['tax']) { ?>
                                    <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    <?php
                    }
                }
          ?>
    </div>

</div>


<div class="container">
    <button data-toggle="collapse" data-target="#toggle-filters-mob" class="btn btn-toogle-filters visible-xs">REFINE RESULTS</button>


<!--  <div id="content" class="<?php /*echo $class; */?>">
            <h2><?php /*echo $heading_title; */?></h2>
            <?php /*if ($thumb || $description) { */?>
            <div class="row">
                <?php /*if ($thumb) { */?>
                <div class="col-sm-2"><img src="<?php /*echo $thumb; */?>" alt="<?php /*echo $heading_title; */?>" title="<?php /*echo $heading_title; */?>" class="img-thumbnail" /></div>
                <?php /*} */?>
                <?php /*if ($description) { */?>
                <div class="col-sm-10"><?php /*echo $description; */?></div>
                <?php /*} */?>
            </div>
            <hr>
            --><?php /*} */?>
    <br>
    <?php if ($products) { ?>

        <div id="toggle-filters-mob" class="filter-options clearfix">

            <select id="input-sort" class="selectpicker" onchange="location = this.value;">
                <option value="<?php echo $text_sort; ?>"><?php echo $text_sort; ?></option>
                <?php foreach ($sorts as $sorts) { ?>
                    <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                        <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                    <?php } else { ?>
                        <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                    <?php } ?>
                <?php } ?>
            </select>

            <select id="input-limit" class="selectpicker" onchange="location = this.value;">
                <option value="<?php echo $text_limit; ?>" selected="selected"><?php echo $text_limit; ?></option>
                <?php foreach ($limits as $limits) { ?>
                    <?php if ($limits['value'] == $limit) { ?>
                        <option value="<?php echo $limits['href']; ?>" ><?php echo $limits['text']; ?></option>
                    <?php } else { ?>
                        <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                    <?php } ?>
                <?php } ?>
            </select>
            <?php echo $column_left; ?>

        </div>
        

        <br />

        <div class="row">
            <div class="items-wrap-designer-profile">
                <?php foreach ($products as $product) {  ?>

                    <div class="col-md-3 col-sm-4 clearfix"><!-- item-->
                        <div class="items">
                            <a href="<?php echo $product['href']; ?>" >
                                <img height="" src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>">
                             </a>
                            <div class="item-g">
                                <h4><?php echo $heading_title ; ?></h4>

                                <a href="<?php echo $product['href']; ?>"><p><?php echo $product['name']; ?></p></a>

                                <?php if ($product['price']) { ?>
                                    <?php if (!$product['special']) { ?>
                                        <b>  <?php echo $product['price']; ?></b>
                                    <?php } else { ?>
                                        <b class="price-new"><?php echo $product['special']; ?></b> <b class="price-old"><?php echo $product['price']; ?></b>
                                    <?php } ?>
                                    <?php if ($product['tax']) { ?>
                                        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                    <?php } ?>
                                <?php } ?>
                                <a href="<?php echo $product['href']; ?>" class="line">VIEW DETAILS</a>
                            </div>
                            <button class="like"  id="#like-wish" type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');">

                            </button>

                        </div>
                    </div>

                <?php } ?>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
            <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
    <?php } ?>
    <?php if (!$categories && !$products) { ?>
        <p><?php echo $text_empty; ?></p>
        <div class="buttons">
            <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
        </div>
    <?php } ?>
    <?php echo $content_bottom; ?></div>
<?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
