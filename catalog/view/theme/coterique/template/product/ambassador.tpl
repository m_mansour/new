<?php
echo $header;
global $config;
$theme_path = $config->get('config_template')

?>
<?php
//$x=$parent_cat_info['category_id'];
//var_dump($x);exit;
 ?>
<section class="main-banner-section ambassdor" style="background: url('image/<?php echo $fulls; ?>') no-repeat fixed;">
    <div class="container">
        <div class="col-md-4 col-md-offset-4">
            <div class="header2">
                <h3>Meet our ambassador</h3>
                <h1><?php echo $heading_title; ?></h1>
                <p><?php echo  $job_title; ?></p>
                <div class="place clearfix">
                    <div class="img-wrap"><img src="catalog/view/theme/coterique/image/flag-place.jpg" alt=""></div>
                    <h4><?php echo $zone_name; ?>,<span><?php echo $country_name; ?></span></h4>
                </div><a href="#" class="line">follow	</a>
            </div>
        </div>
    </div>
</section>
<?php echo $description; ?>
<section>
    <div class="ammmmb">

        <?php if ($images) { ?>
        <?php foreach ($images as $image) { ?>
        <div class="item"><img src="image/<?php echo $image['full_image']; ?>" alt="">
            <h4>“That's the kind of life I want. I feel happier when I exist around other people.”</h4>
        </div>
        <?php } ?>
        <?php } ?>

    </div>
</section>
<section class="section top-picks">	<!--section -->
    <div class="container founder-section">
        <div class="heading">
            <h3><?php echo $heading_title; ?><span>top picks</span></h3>
        </div>
    </div>
    <div class="container">
        <?php foreach ($products as $product) {  ?>
        <div class="col-md-2 col-sm-3 col-xs-6">
            <div class="item"><img style="height: 99px;width: 75px"  src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>">
                <div class="caption">
                  <!--  <h4>DEBORAH HENNING</h4> -->
                    <p><?php echo $product['name']; ?></p><a href="<?php echo $product['href']; ?>" class="line">SHOP NOW</a>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</section>

<?php echo $footer; ?>
