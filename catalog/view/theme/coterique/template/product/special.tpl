<?php
echo $header;
global $config;
$theme_path = $config->get('config_template')

?>
<?php
//$x=$parent_id_des['category_id'];
//var_dump($heading_id);exit;
?>



<section class="container ">
  <div class="crumb crumb-2 clearfix ">
    <ul class="breadcrumb list-unstyled ">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ul>
  </div>

</section>


<?php //echo $column_left; ?>
<div class="designer-profile-content clearfix">
  <div class="container">
  <button data-toggle="collapse" data-target="#toggle-filters-mob" class="btn btn-toogle-filters visible-xs">REFINE RESULTS</button>

  <br>
  <?php if ($products) { ?>

    <div id="toggle-filters-mob" class="filter-options clearfix">

      <select id="input-sort" class="selectpicker" onchange="location = this.value;">
        <option value="<?php echo $text_sort; ?>"><?php echo $text_sort; ?></option>
        <?php foreach ($sorts as $sorts) { ?>
          <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
            <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
          <?php } else { ?>
            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
          <?php } ?>
        <?php } ?>
      </select>

      <select id="input-limit" class="selectpicker" onchange="location = this.value;">
        <option value="<?php echo $text_limit; ?>" selected="selected"><?php echo $text_limit; ?></option>
        <?php foreach ($limits as $limits) { ?>
          <?php if ($limits['value'] == $limit) { ?>
            <option value="<?php echo $limits['href']; ?>" ><?php echo $limits['text']; ?></option>
          <?php } else { ?>
            <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
          <?php } ?>
        <?php } ?>
      </select>
      <?php //echo $column_left; ?>
<!--      <select id="input-cat" class="selectpicker" onchange="location = this.value;">
        <option value="<?php /*echo $heading_title;*/?>" selected="selected"><?php /*echo $heading_title;*/?></option>
        <?php /*foreach ($categories as $category) { */?>
          <?php /*if (count($categories) <= 5) { */?>
            <option value="<?php /*echo $category['href']; */?>" ><?php /*echo $category['name']; */?></option>
          <?php /*} else { */?>
            <option value="<?php /*echo $category['href']; */?>"><?php /*echo $category['name'] ; */?></option>
          <?php /*} */?>
        <?php /*} */?>
      </select>-->



    </div>

    <br />

    <div class="row">
      <div class="items-wrap-designer-profile">
        <?php foreach ($products as $product) {  ?>

          <div class="col-md-3 col-sm-4  clearfix"><!-- item-->
            <div class="items">
              <a href="<?php echo $product['href']; ?>">
                <img height="" src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>">
              </a>
              <div class="item-g">

                <h4><?php echo $product['model'] ; ?></h4>

                <a href="<?php echo $product['href']; ?>"><p><?php echo $product['name']; ?></p></a>

                <?php if ($product['price']) { ?>
                  <?php if (!$product['special']) { ?>
                    <b>  <?php echo $product['price']; ?></b>
                  <?php } else { ?>
                    <b class="price-new"><?php echo $product['special']; ?></b> <b class="price-old"><?php echo $product['price']; ?></b>
                  <?php } ?>
                  <?php if ($product['tax']) { ?>
                    <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                  <?php } ?>
                <?php } ?>
                <a href="<?php echo $product['href']; ?>" class="line">VIEW DETAILS</a>
              </div>
              <button class="like"  id="#like-wish" type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');">

              </button>

            </div>
          </div>

        <?php } ?>

      </div>
    </div>
    <div class="row">
      <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
      <div class="col-sm-6 text-right"><?php echo $results; ?></div>
    </div>
  <?php } ?>
  <?php if (!$categories && !$products) { ?>
    <p><?php echo $text_empty; ?></p>
    <div class="buttons">
      <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
    </div>
  <?php } ?>
  <?php //echo $content_bottom; ?>
</div>
</div>
<?php //echo $column_right; ?>

<?php echo $footer; ?>
