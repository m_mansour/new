<?php echo $header; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <!--<h3>
    &nbsp;
  </h3>-->
  <div class="product-info">
    <div class="left">
      <?php if ($thumb) { ?>

      <div class="image"><a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" class="colorbox" rel="colorbox"><img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" id="image" /></a></div>
      <?php } ?>
      <?php if ($images) { ?>
      <div class="image-additional">
        <?php foreach ($images as $image) { ?>
        <a href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>" class="colorbox" rel="colorbox"><img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a>
        <?php } ?>
      </div>
      <?php } ?>
    </div>
    <div class="center">

      <div class="description">
        <b><?php echo $model; ?></b><br />
        <?php echo $heading_title; ?>
      </div>
      <?php if ($price) { ?>
      <div class="price">
        <?php if (!$special) { ?>
        <?php echo $price; ?>
        <?php } else { ?>
        <span class="price-old"><?php echo $price; ?></span> <span class="price-new"><?php echo $special; ?></span>
        <?php } ?>
        <br />
        <?php if ($discounts) { ?>
        <br />
        <div class="discount">
          <?php foreach ($discounts as $discount) { ?>
          <?php echo sprintf($text_discount, $discount['quantity'], $discount['price']); ?><br />
          <?php } ?>
        </div>
        <?php } ?>
      </div>
      <?php } ?>

      <div class="info">
        <?php echo $description; ?>
      </div>
      <div class="size-guide">
        <div id="size-guide-toggle"  ><a onclick='javascript:$("#size-guide-table").toggle("slow");'><b>Size Guide</b></a></div>
        <div id="size-guide-table" style="display:none;">
          <TABLE WIDTH=280 CELLPADDING=0 CELLSPACING=0 STYLE="page-break-before: always">
            <COL >
              <COL>
                <COL >
                  <COL >
                    <COL >
                      <COL >
                        <COL >
                          <TR>
                            <TD  >
                              <P><BR>
                              </P>
                            </TD>
                            <TD  >
                              <P>Denim</P>
                            </TD>
                            <TD  >
                              <P>US</P>
                            </TD>
                            <TD  >
                              <P>UK</P>
                            </TD>
                            <TD  >
                              <P>AU</P>
                            </TD>
                            <TD >
                              <P>FR</P>
                            </TD>
                            <TD >
                              <P>IT</P>
                            </TD>
                          </TR>
                          <TR>
                            <TD >
                              <P>XS</P>
                            </TD>
                            <TD  >
                              <P>25,26</P>
                            </TD>
                            <TD  >
                              <P>2</P>
                            </TD>
                            <TD  >
                              <P>6</P>
                            </TD>
                            <TD  >
                              <P>6</P>
                            </TD>
                            <TD  >
                              <P>34</P>
                            </TD>
                            <TD >
                              <P>38</P>
                            </TD>
                          </TR>
                          <TR>
                            <TD  >
                              <P>S</P>
                            </TD>
                            <TD >
                              <P>27.28</P>
                            </TD>
                            <TD  >
                              <P>4</P>
                            </TD>
                            <TD  >
                              <P>8</P>
                            </TD>
                            <TD  >
                              <P>8</P>
                            </TD>
                            <TD  >
                              <P>36</P>
                            </TD>
                            <TD >
                              <P>40</P>
                            </TD>
                          </TR>
                          <TR>
                            <TD  >
                              <P>M</P>
                            </TD>
                            <TD  >
                              <P>29,30</P>
                            </TD>
                            <TD >
                              <P>6</P>
                            </TD>
                            <TD  >
                              <P>10</P>
                            </TD>
                            <TD >
                              <P>10</P>
                            </TD>
                            <TD>
                              <P>38</P>
                            </TD>
                            <TD>
                              <P>42</P>
                            </TD>
                          </TR>
                          <TR>
                            <TD >
                              <P>L</P>
                            </TD>
                            <TD>
                              <P>30</P>
                            </TD>
                            <TD >
                              <P>8</P>
                            </TD>
                            <TD  >
                              <P>12</P>
                            </TD>
                            <TD >
                              <P>12</P>
                            </TD>
                            <TD  >
                              <P>40</P>
                            </TD>
                            <TD >
                              <P>44</P>
                            </TD>
                          </TR>
                          <TR>
                            <TD>
                              <P>XL</P>
                            </TD>
                            <TD>
                              <P>31,32</P>
                            </TD>
                            <TD >
                              <P>10</P>
                            </TD>
                            <TD>
                              <P>14</P>
                            </TD>
                            <TD >
                              <P>14</P>
                            </TD>
                            <TD>
                              <P>42</P>
                            </TD>
                            <TD >
                              <P>46</P>
                            </TD>
                          </TR>

                        </TABLE>

                      </div>
                    </div>
                    <?php if ($options) { ?>
                    <div class="options">
                      <br />
                      <?php foreach ($options as $option) { ?>
                      <?php if ($option['type'] == 'select') { ?>
                      <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                        <?php if ($option['required']) { ?>
                        <span class="required">*</span>
                        <?php } ?>
                        <b><?php echo $option['name']; ?>:</b><br />
                        <select name="option[<?php echo $option['product_option_id']; ?>]">
                          <option value=""><?php echo $text_select; ?></option>
                          <?php foreach ($option['option_value'] as $option_value) { ?>
                          <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                            <?php if ($option_value['price']) { ?>
                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                            <?php } ?>
                          </option>
                          <?php } ?>
                        </select>
                      </div>
                      <br />
                      <?php } ?>
                      <?php if ($option['type'] == 'radio') { ?>
                      <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                        <?php if ($option['required']) { ?>
                        <span class="required">*</span>
                        <?php } ?>
                        <b><?php echo $option['name']; ?>:</b><br />
                        <?php foreach ($option['option_value'] as $option_value) { ?>
                        <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" />
                        <label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                          <?php if ($option_value['price']) { ?>
                          (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                          <?php } ?>
                        </label>
                        <br />
                        <?php } ?>
                      </div>
                      <br />
                      <?php } ?>
                      <?php if ($option['type'] == 'checkbox') { ?>
                      <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                        <?php if ($option['required']) { ?>
                        <span class="required">*</span>
                        <?php } ?>
                        <b><?php echo $option['name']; ?>:</b><br />
                        <?php foreach ($option['option_value'] as $option_value) { ?>
                        <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" />
                        <label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                          <?php if ($option_value['price']) { ?>
                          (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                          <?php } ?>
                        </label>
                        <br />
                        <?php } ?>
                      </div>
                      <br />
                      <?php } ?>
                      <?php if ($option['type'] == 'image') { ?>
                      <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                        <?php if ($option['required']) { ?>
                        <span class="required">*</span>
                        <?php } ?>
                        <b><?php echo $option['name']; ?>:</b><br />
                        <table class="option-image">
                          <?php foreach ($option['option_value'] as $option_value) { ?>
                          <tr>
                            <td style="width: 1px;"><input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" /></td>
                            <td><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" /></label></td>
                            <td><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                              <?php if ($option_value['price']) { ?>
                              (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                              <?php } ?>
                            </label></td>
                          </tr>
                          <?php } ?>
                        </table>
                      </div>
                      <br />
                      <?php } ?>
                      <?php if ($option['type'] == 'text') { ?>
                      <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                        <?php if ($option['required']) { ?>
                        <span class="required">*</span>
                        <?php } ?>
                        <b><?php echo $option['name']; ?>:</b><br />
                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" />
                      </div>
                      <br />
                      <?php } ?>
                      <?php if ($option['type'] == 'textarea') { ?>
                      <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                        <?php if ($option['required']) { ?>
                        <span class="required">*</span>
                        <?php } ?>
                        <b><?php echo $option['name']; ?>:</b><br />
                        <textarea name="option[<?php echo $option['product_option_id']; ?>]" cols="40" rows="5"><?php echo $option['option_value']; ?></textarea>
                      </div>
                      <br />
                      <?php } ?>
                      <?php if ($option['type'] == 'file') { ?>
                      <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                        <?php if ($option['required']) { ?>
                        <span class="required">*</span>
                        <?php } ?>
                        <b><?php echo $option['name']; ?>:</b><br />
                        <input type="button" value="<?php echo $button_upload; ?>" id="button-option-<?php echo $option['product_option_id']; ?>" class="button">
                        <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" />
                      </div>
                      <br />
                      <?php } ?>
                      <?php if ($option['type'] == 'date') { ?>
                      <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                        <?php if ($option['required']) { ?>
                        <span class="required">*</span>
                        <?php } ?>
                        <b><?php echo $option['name']; ?>:</b><br />
                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="date" />
                      </div>
                      <br />
                      <?php } ?>
                      <?php if ($option['type'] == 'datetime') { ?>
                      <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                        <?php if ($option['required']) { ?>
                        <span class="required">*</span>
                        <?php } ?>
                        <b><?php echo $option['name']; ?>:</b><br />
                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="datetime" />
                      </div>
                      <br />
                      <?php } ?>
                      <?php if ($option['type'] == 'time') { ?>
                      <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
                        <?php if ($option['required']) { ?>
                        <span class="required">*</span>
                        <?php } ?>
                        <b><?php echo $option['name']; ?>:</b><br />
                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="time" />
                      </div>
                      <br />
                      <?php } ?>
                      <?php } ?>
                    </div>
                    <?php } ?>
                    <div class="cart">
                      <div>
                        <input type="hidden" name="quantity" size="2" value="<?php echo $minimum; ?>" />
                        <input type="hidden" name="product_id" size="2" value="<?php echo $product_id; ?>" />
                        <?php if($quantity>0) {?>
                        <input type="button" value="<?php echo $button_cart; ?>" id="button-cart" class="button" />
                        <?php } else { ?>
                        <input type="button" value="OUT OF STOCK" class="button" />

                        <?php } ?>
                      </div>
                    </div>
                  </div>
                  <div class="right">
                   <?php if ($products) { ?>
                   <div style="float:left; width:300px; margin-bottom:30px;">
                    <b style='font-family: "futura_bold", sans-serif;'>WEAR IT WITH:</b>
                  </div>
                  <?php foreach ($products as $product) { ?>
                  <div class="related_holder">
                    <?php if ($product['thumb']) { ?>
                    <div style="float:left; width:100px;">
                      <div class="image-related"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a></div></div>
                      <?php } ?>
                      <div style="float:left; width:200px;">
                        <div class="brand"><a href="<?php echo $product['href']; ?>"><?php echo $product['model']; ?></a></div>
                        <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                        <?php if ($product['price']) { ?>
                        <div class="price">
                          <?php if (!$product['special']) { ?>
                          <?php echo $product['price']; ?>
                          <?php } else { ?>
                          <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
                          <?php } ?>
                        </div>
                      </div>
                      <?php } ?>
                      <?php if ($product['rating']) { ?>
                      <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
                      <?php } ?>
                    </div>
                    <?php } ?>
                    <?php } ?>
                  </div>
                </div>
<!--
<div class="modal fade" id="modal_addedCart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>--><!-- /.modal -->
<?php echo $content_bottom; ?></div>
<!--<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
  <script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>-->
  <script type="text/javascript"><!--
  $('.colorbox').colorbox({
    overlayClose: true,
    opacity: 0.5
  });
  //--></script> 
  <script type="text/javascript"><!--
  $('#button-cart').bind('click', function() {
    $.ajax({
      url: 'index.php?route=checkout/cart/add',
      type: 'post',
      data: $('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea'),
      dataType: 'json',
      success: function(json) {
        $('.success, .warning, .attention, information, .error').remove();

        if (json['error']) {
          if (json['error']['option']) {
            for (i in json['error']['option']) {
              $('#option-' + i).after('<span class="error">' + json['error']['option'][i] + '</span>');
            }
          }
        } 

        if (json['success']) {
          //$("#modal_addedCart").modal();
          $('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');

          $('.success').fadeIn('slow');

          $('#cart-total').html(json['total']);

          $('html, body').animate({ scrollTop: 0 }, 'slow'); 
        } 
      }
    });
});
//--></script>
<?php if ($options) { ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/ajaxupload.js"></script>
<?php foreach ($options as $option) { ?>
<?php if ($option['type'] == 'file') { ?>
<script type="text/javascript"><!--
new AjaxUpload('#button-option-<?php echo $option['product_option_id']; ?>', {
  action: 'index.php?route=product/product/upload',
  name: 'file',
  autoSubmit: true,
  responseType: 'json',
  onSubmit: function(file, extension) {
    $('#button-option-<?php echo $option['product_option_id']; ?>').after('<img src="catalog/view/theme/default/image/loading.gif" class="loading" style="padding-left: 5px;" />');
    $('#button-option-<?php echo $option['product_option_id']; ?>').attr('disabled', true);
  },
  onComplete: function(file, json) {
    $('#button-option-<?php echo $option['product_option_id']; ?>').attr('disabled', false);
    
    $('.error').remove();
    
    if (json['success']) {
      alert(json['success']);
      
      $('input[name=\'option[<?php echo $option['product_option_id']; ?>]\']').attr('value', json['file']);
    }
    
    if (json['error']) {
      $('#option-<?php echo $option['product_option_id']; ?>').after('<span class="error">' + json['error'] + '</span>');
    }
    
    $('.loading').remove(); 
  }
});
//--></script>
<?php } ?>
<?php } ?>
<?php } ?>
<script type="text/javascript"><!--
$('#review .pagination a').live('click', function() {
  $('#review').fadeOut('slow');

  $('#review').load(this.href);
  
  $('#review').fadeIn('slow');
  
  return false;
});     

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').bind('click', function() {
  $.ajax({
    url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
    type: 'post',
    dataType: 'json',
    data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
    beforeSend: function() {
      $('.success, .warning').remove();
      $('#button-review').attr('disabled', true);
      $('#review-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
    },
    complete: function() {
      $('#button-review').attr('disabled', false);
      $('.attention').remove();
    },
    success: function(data) {
      if (data['error']) {
        $('#review-title').after('<div class="warning">' + data['error'] + '</div>');
      }
      
      if (data['success']) {
        $('#review-title').after('<div class="success">' + data['success'] + '</div>');

        $('input[name=\'name\']').val('');
        $('textarea[name=\'text\']').val('');
        $('input[name=\'rating\']:checked').attr('checked', '');
        $('input[name=\'captcha\']').val('');
      }
    }
  });
});
//--></script> 
<script type="text/javascript"><!--
$('#tabs a').tabs();
//--></script> 
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script> 
<script type="text/javascript"><!--
if ($.browser.msie && $.browser.version == 6) {
  $('.date, .datetime, .time').bgIframe();
}

$('.date').datepicker({dateFormat: 'yy-mm-dd'});
$('.datetime').datetimepicker({
  dateFormat: 'yy-mm-dd',
  timeFormat: 'h:m'
});
$('.time').timepicker({timeFormat: 'h:m'});
//--></script> 
<?php echo $footer; ?>