<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content">
	<?php echo $content_top; ?>

	<br/>
	<p>
	Our Return Policy is simple, convenient and hassle-free. While we would like you to keep and enjoy the item you've bought on Coterique.com, we equally want you to have the peace of mind knowing that an item bought can be easily returned within certain time-frames and conditions. After all, keeping our Customers happy is the whole point of Coterique.com and we'd love to hear from you at any time regarding any of your purchases made.</p>
<p>
	Simply,if you wish to return an item for any reason or if you've had a change of mind, you could do so for all items bought on our website within&nbsp;<strong>Two (2) Days</strong>&nbsp;of the date of purchase, so long as the item(s), any related accessories and/or tools and components - along with the Original Packaging - is in the original state and condition when purchased and received.<br />
	&nbsp;</p>
<p>
	<strong>Any exceptions to our Returns Policy?</strong></p>
<p>
	<strong>Only for Bags you can only check them and return them at the time of delivery</strong></p>
<p>
	</p>
<p>
	Conveniently, we will arrange the return for you. You could contact our Customer Services Team 7-Days a week, 365-Days a year, to arrange a courier pick-up for your item and a quick Return. To ensure you are within the Policy time-frame for returning your purchase, please make sure that you contact us as soon as you have decided on returning an item. To do so, please visit our CONTACT US page on our website for the different methods you could contact our Customer Services team.</p>
<p>
	Just remember- Check your item(s) as soon as you receive it in order to ensure your full satisfaction with your purchase(s) so that you could take advantage of our Simple, No-questions asked Return policy within the conditional time-frames for returning an item (see above).</p>
<p>
	<strong>Hassle-free and Peace of Mind go hand in hand, this is why we also offer you certain guarantees to ensure your happiness and satisfaction:</strong></p>
<ul>
	<li>
		If an item received is not the same one ordered, doesn't work or function as advertised, is missing any component or accessory, faulty, damaged or not in the advertised condition, you have the right to return it and we will arrange for it.</li> <br/>
	<li>
		Your Right to Return starts only from the day you take delivery-of (receive) the item(s) and sign-off on the Courier Company's delivery note and can be used through-out the time-frames mentioned above, which are - Two (2) Days for all items except Bags.</li> <br/>
	<li>
		Refunds on your purchase are done seamlessly once a Return is requested. However it might take a few days to be credited back to you, whether on your Electronic Payment Card or Cash.</li> <br/>
</ul>
<p>
</p>

</div>
<?php echo $footer; ?>