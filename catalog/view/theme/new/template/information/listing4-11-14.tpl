<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content">
	<?php echo $content_top; ?>
	<div id="topletters" >
		<?php
			//$letters = array('#');
			$letters =  range('A', 'Z');
			array_push($letters, '#');
			$lettersArray = array(array());
			foreach ($letters as $letter) { ?>
				<a href="<?php echo $_SERVER['REQUEST_URI'];?>#letter_id_<?php echo $letter;?>"><?php echo $letter; ?></a>
		<?php } ?>
	</div>
	<style>
		.letter_container{
			border-top: 1px dotted #000000;
			padding: 15px 30px 35px 30px;
			overflow: auto;

		}

		.letter_big {
			float: left;
			position: absolute;
			text-align: left;
		}

		.letter_designers{
			float: right;
			padding-left: 50px; 
			width: 500px;
		}
		
		.designers_table{
    			table-layout: fixed;
			/*width: 500px;*/

		}
	
		.designers_table td{
			break-word: word-wrap;
			width: 160px;		
		}
		
		.letter_designers a{
			padding: 0px 5px 0px 5px; 
			margin: 5px 0px 5px 0px;
			text-decoration: none;
			border-right: 2px solid #FFFFFF;
			border-left: 2px solid #FFFFFF;

		}
		
		.letter_designers a:hover{
			color: #FF843D;
			border-right: 2px solid #000000;
			border-left: 2px solid #000000;
		}
	</style>
	<div id="left-column" >
		<div id="listing">
			<?php if ($categories) { 
				foreach ($categories as $category) { 
					if ($category['name'] == 'DESIGNERS') {
						if ($category['children']) { 
							for ($i = 0; $i < count($category['children']);) { 
								$j = $i + ceil(count($category['children']) / $category['column']); 
								for (; $i < $j; $i++) { 
									if (isset($category['children'][$i])) {
										foreach ($letters as $letter) {
											if ( $category['children'][$i]['name'][0] === $letter) {
												$k = 0;
												if (isset($lettersArray[$letter])) { 
													$k = count($lettersArray[$letter]);
												}
												$lettersArray[$letter][$k]['name'] = $category['children'][$i]['name'];
												$lettersArray[$letter][$k]['href'] = $category['children'][$i]['href'];
											}
										}
									}
								}
							}
						}
					}
				}
			}
				foreach ($categories as $category) { 
					if ($category['name'] == 'DESIGNERS') {
						if ($category['children']) { 
							for ($i = 0; $i < count($category['children']);) { 
								$j = $i + ceil(count($category['children']) / $category['column']); 
								for (; $i < $j; $i++) { 
									if (isset($category['children'][$i])) {
											if ( preg_match('/^\d/', $category['children'][$i]['name']) ) {
												$k = 0;
												if (isset($lettersArray['#'])) { 
													$k = count($lettersArray['#']);
												}
												$lettersArray['#'][$k]['name'] = $category['children'][$i]['name'];
												$lettersArray['#'][$k]['href'] = $category['children'][$i]['href'];
											}
									}
								}
							}
						}
					}
				}




			
			foreach ($letters as $letter) {
				if (isset($lettersArray[$letter])) { ?>
					<div class="letter_container" id="<?php echo 'letter_id_'.$letter;?>">
						<div class="letter_big"><h1><?php echo $letter; ?></h1></div>
						<div class="letter_designers">
							<table class="designers_table"><tr>
						<?php for ($i = 0; $i < count($lettersArray[$letter]); $i++) {
								if($i>0 && ($i%3==0)){ echo "</tr><tr>"; }?>
							<td><a href="<?php echo $lettersArray[$letter][$i]['href']; ?>"><?php echo $lettersArray[$letter][$i]['name']; ?></a></td>
						<?php } ?>
							</tr></table>
						</div>
					</div>
				<?php }
			} ?>
		</div>
	</div>
	<div id="right-column">
		<img src='http://demo.coterique.com/catalog/view/theme/shop/image/right.png' alt="MAG">
	</div>
	</br>
</div>
<?php echo $footer; ?>
