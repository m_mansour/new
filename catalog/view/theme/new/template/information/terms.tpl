<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content">
	<?php echo $content_top; ?>
	<br/>
	<p>
	INTRODUCTION AND ACCEPTANCE OF TERMS</p>
<p>
	This Website is operated by:<br />
	<br />
	COTERIQUE for Software Development and E-commerce (Electronic Trade)<br />
	1 Mohamed Sobhy Street, 4<sup>th</sup> floor, Flat 9, Giza, Egypt<br />
	<br />
	Company Registry: 65449</p>
<p>
	Tax No.: 445 - 137 - 142&nbsp;</p>
<p>
	<br />
	Access to and use of this website (hereinafter, the "Website") and the products and services available through this Website ("Services") are subject to the following terms, conditions and notices (the "Terms of Service") as well as to the documents referred to below.&nbsp;<br />
	<br />
	When you access to or use this Web Site and/or buy via this Web Site you are agreeing to all of the Terms of Service, as may be updated by us from time to time. Therefore please read these Terms and Conditions carefully because by accessing and ordering a product, you agree to be bound by same Terms and Conditions.<br />
	<br />
	Access to this Website is permitted on a temporary basis and it does not include any possible commercial use of this Website and of its contents, as well as any reproduction, duplication, copy and/or exploitation for any commercial purposes without express written consent of COTERIQUE for Software Development and E-commerce (Electronic Trade). We reserve the right to withdraw or amend the Services without notice. We will not be liable if for any reason this Website is unavailable at any time or for any period. From time to time, we may restrict access to some parts or all of this Website.&nbsp;<br />
	<br />
	When you visit this Website and/or submit an order, you are communicating with us electronically and you agree that all agreements, notices, disclosures and other communications that we send to you electronically satisfy any legal requirement that same communications be in writing.<br />
	<br />
	This Website accepts no responsibility for 3rd party's websites to which it may link to. Moreover these Terms and Conditions do not regulate the supply of services and/or products performed by third parties that appear on this Website through links, banners or other hypertext links, therefore under no circumstance COTERIQUE for Software Development and E-commerce (Electronic Trade) shall be deemed liable for services and/or products directly supplied by such third parties.<br />
	<br />
	&nbsp;</p>
<p>
	PRIVACY POLICY</p>
<p>
	By using this Website, you consent to the processing described therein and warrant that all data provided by you is accurate.&nbsp;<br />
	<br />
	</p>
<p>
	OUR SERVICES AND BUSINESS POLICY</p>
<p>
	Through this Website you are able to search and buy products of a number of brands and retailers.<br />
	<br />
	COTERIQUE for Software Development and E-commerce (Electronic Trade) is not directly a party of the transaction between you and sellers (i.e. the retailer and/or the brand of the ordered product), but only act to facilitate the contact between the buyer and the seller and to perform some ancillary service such as receiving payments on behalf of the seller and providing customer service. As a consequence COTERIQUE for Software Development and E-commerce (Electronic Trade) has no control over the quality, safety, morality or legality of the products. In any case, on this Website we do not offer for sale flawed products or product of lower quality than the corresponding market standards. In case the quality of the product do not correspond to such standards, you can return it to us according to the relevant provisions of the&nbsp;<a href="http://www.farfetch.com/pag91.aspx#Returns1">Returns Policy</a>, as described in the Returns and Refunds section.&nbsp;<br />
	<br />
	COTERIQUE for Software Development and E-commerce (Electronic Trade) attempts to be as accurate as possible in the description of the products. However COTERIQUE for Software Development and E-commerce (Electronic Trade) does not warrant that such a description is totally accurate, complete, reliable and error-free. In case the product ordered by you is not as described, you can return it to us according to the relevant provisions of the&nbsp;<a href="http://www.farfetch.com/pag91.aspx#Returns1">Returns Policy</a>, as described in the Returns and Refunds section.&nbsp;<br />
	<br />
	COTERIQUE for Software Development and E-commerce (Electronic Trade) requests all sellers using this Website to create reasonable business policies which comply with COTERIQUE for Software Development and E-commerce (Electronic Trade)'s business policies.&nbsp;<br />
	<br />
	</p>
<p>
	YOUR CONDUCT</p>
<p>
	In order to use the Services made available by the Website you must be over 18 years of age.<br />
	<br />
	You must not use the Website in any way that causes, or is likely to cause, the Website or access to it to be interrupted, damaged or impaired in any way.<br />
	You understand that you, and not our Website, are responsible for all electronic communications and content sent from your computer to us and you must use the Website for lawful purposes only.<br />
	<br />
	You must not use the Website for any of the following:<br />
	<br />
	<ul>
	<li>for fraudulent purposes, or in connection with a criminal offence or other unlawful activity; </li><br />
	<li>to send, use or reuse any material that is (i) illegal, offensive, abusive, indecent, defamatory, obscene or menacing, and/or (ii) in breach of copyright, trademark, confidence, privacy or any other right, and/or (iii) otherwise injurious to third parties, and/or (iv) objectionable, and/or (v) which consists of or contains software viruses, political campaigning, commercial solicitation, chain letters, mass mailings or any "spam";&nbsp;</li><br />
	<li>to cause annoyance, inconvenience or needless anxiety.&nbsp; </li><br />
	</ul>
	<br />
	Breaching this provision would constitute a criminal offence under the Egyptian law. COTERIQUE for Software Development and E-commerce (Electronic Trade), in compliance with any enforceable law or public order, will report any such breach to the relevant law enforcement authorities and disclose your identity to them.&nbsp;<br />
	<br />
	We will not be liable for any loss or damage caused by a distributed denial-of-service attack, viruses or other technologically harmful material that may infect your computer equipment, computer programs, data or other proprietary material due to your use of this Website or to your downloading of any material posted on it, or on any website linked to it.&nbsp;<br />
	<br />
	</p>
<p>
	INTELLECTUAL PROPERTY, SOFTWARE AND CONTENT</p>
<p>
	All content included on the Website, such as text, graphics, logos, button icons, images, audio clips, digital downloads, data compilations and software is the exclusive property of COTERIQUE for Software Development and E-commerce (Electronic Trade), its affiliates or its content suppliers and is protected by international copyright laws as well as by any relevant national law concerning copyright, authors rights and database right laws. The compilation of all content on this website is the exclusive property of COTERIQUE for Software Development and E-commerce (Electronic Trade) and its affiliates and is protected by Egypt and international copyright and database right laws. All software used on this website is the property of COTERIQUE for Software Development and E-commerce (Electronic Trade), our affiliates or our software suppliers and is protected by international copyright and authors' rights laws.<br />
	<br />
	It is therefore understood that all intellectual property rights - including, without limitation, all copyrights, database rights, rights in trademarks (except for what specifically provided hereinafter), rights in designs, rights in know-how and, in case, rights in patents and inventions as well as all other intellectual or industrial property rights - concerning any information, content, materials, data or processes contained in the Website belong to COTERIQUE for Software Development and E-commerce (Electronic Trade), its affiliates and/or its content suppliers. All such intellectual property rights of COTERIQUE for Software Development and E-commerce (Electronic Trade), its affiliates and/or its content suppliers are hereby reserved.<br />
	<br />
	You may not systematically extract and/or re-utilise parts of the contents of the website without COTERIQUE for Software Development and E-commerce (Electronic Trade)'s express written consent. In particular, you may not utilise any data mining, robots, or similar data gathering and extraction tools to extract (whether once or many times) for re-utilisation of any substantial parts of this website, without COTERIQUE for Software Development and E-commerce (Electronic Trade)'s express written consent. You also may not create and/or publish your own database that features substantial (eg our prices and product listings) parts of this website without COTERIQUE for Software Development and E-commerce (Electronic Trade)'s express written consent.<br />
	<br />
	Except where expressly stated to the contrary, all persons (including their names and images), third party trademarks and images of third party products, services and/or locations featured on this Website are in no way associated, linked or affiliated with COTERIQUE for Software Development and E-commerce (Electronic Trade) and you should not rely on the existence of such a connection or affiliation. Any trademarks/names featured on this Website are owned by the respective trade mark owners. Where a trade mark or brand name is referred to, it is used solely to describe or identify the products and/or services and it is in no way an assertion that such products or services are provided with the endorsement of the respective brand owner.<br />
	<br />
	</p>
<p>
	ORDERS, PRICES AND PAYMENT</p>
<p>
	By placing an order you are offering to purchase a product from the respective seller (and not directly from COTERIQUE for Software Development and E-commerce (Electronic Trade). which only act as a market where sellers and buyers meet), on and subject to the following terms and conditions. All orders are subject to availability and confirmation of the order price.<br />
	<br />
	Dispatch times may vary according to availability and there are no guarantees as to delivery times. The ordered products will be delivered to you directly by the seller, so that there can be multiple deliveries if different sellers are involved in your order. In order to use the Services made available by the Website you must be over 18 years of age and possess a valid credit or debit card issued by a bank acceptable to us or pay cash on delivery. We retain the right to refuse any request made by you.&nbsp;<br />
	<br />
	Our Website permits you to check any errors before sending your order to us. If your order is accepted we will inform you by email and we will confirm the identity of the seller, i.e. the party which you have contracted with and the one which will directly ship and invoice the ordered goods to you.<br />
	<br />
	When placing an order you undertake that all details you provide to us are true and accurate, that you are over 18 years and are an authorised user of the credit or debit card used to place your order and that there are sufficient funds to cover the cost of the goods. The cost of foreign products and services may fluctuate. All prices advertised are subject to such changes.<br />
	<br />
	<br />
	Pricing and Availability<br />
	Whilst we try and ensure that all details, descriptions and prices which appear on this Website are accurate, errors may occur. If we discover an error in the price of any goods which you have ordered we will inform you of this as soon as possible and give you the option of reconfirming your order at the correct price or cancelling it. If we are unable to contact you we will treat the order as cancelled. If you cancel and you have already paid for the goods, you will receive a full refund.&nbsp;<br />
	<br />
	All prices are inclusive of Sales Tax. Delivery costs will be charged in addition; such additional charges are clearly displayed where applicable and included in the 'Total Cost'.&nbsp;<br />
	<br />
	Currently, the prices you see on COTERIQUE for Software Development and E-commerce (Electronic Trade) exclude the cost of shipping and import duties except for customers shopping in the Egypt where prices are inclusive of import duties. Depending on your delivery address there may be different taxation rules. If you are shipping items from a boutique outside of your territory, you may need to pay import duties upon receipt of your item. More information is available through this link to&nbsp;<a href="http://www.farfetch.com/pag91.aspx#Shipping1">"Duties and Taxes"</a>&nbsp;section.<br />
	<br />
	<br />
	(c) Payment&nbsp;<br />
	Payment for items on the Site may be made online or partly online through the Coterique.com payment facilities called "Wallety" or Cash On Delivery ("COD") (or other payment methods which Coterique.com may make available on the Site from time to time)</p>
<p>
	By providing the Wallety and COD payment facilities, Coterique.com is merely facilitating the making of online payments by buyers possible but Coterique.com is not involved in the process of buying and selling items on the Site. All sales and purchases on the Site continue to be bipartite contracts between the buyer and the seller of an item(s) and Coterique.com is not responsible for any non-performance, breach or any other claim relating to or arising out of any contract entered into between any buyers and sellers, nor does Coterique.com have any fiduciary duty to any user. You acknowledge and agree that Coterique.com will hold your funds at your own risk.</p>
<p>
	<br />
	(d) Archival&nbsp;<br />
	Coterique.com will archive a copy of your contract with the seller in its files, and you will able to retrieve a copy by communicating your data and those of the order.<br />
	<br />
	</p>
<p>
	RETURNS POLICY</p>
<p>
	To view our returns policy please read our&nbsp;<a href="http://www.farfetch.com/pag91.aspx#Returns1">Returns and Refunds</a>.&nbsp;<br />
	<br />
	</p>
<p>
	DISCLAIMER OF LIABILITY</p>
<p>
	The material displayed on this Website is provided without any guarantees, conditions or warranties as to its accuracy. Unless expressly stated to the contrary to the fullest extent permitted by law, COTERIQUE for Software Development and E-commerce (Electronic Trade) and its suppliers, content providers and advertisers hereby expressly exclude all conditions, warranties and other terms which might otherwise be implied by statute, common law or the law of equity and shall not be liable for any damages whatsoever, including but without limitation to any direct, indirect, special, consequential, punitive or incidental damages, or damages for loss of use, profits, data or other intangibles, damage to goodwill or reputation, or the cost of procurement of substitute goods and services, arising out of or related to the use, inability to use, performance or failures of this Website or the Linked Sites and any materials posted thereon, irrespective of whether such damages were foreseeable or arise in contract, tort, equity, restitution, by statute, at common law or otherwise. This does not affect COTERIQUE for Software Development and E-commerce (Electronic Trade)'s liability for death or personal injury arising from its negligence, nor for fraudulent misrepresentation, misrepresentation as to a fundamental matter or any other liability which cannot be excluded or limited under applicable law.&nbsp;<br />
	<br />
	</p>
<p>
	INDEMNITY</p>
<p>
	You agree to indemnify, defend and hold harmless COTERIQUE for Software Development and E-commerce (Electronic Trade), its directors, officers, employees, consultants, agents, and affiliates, from any and all third party claims, liability, damages and/or costs (including, but not limited to, legal fees) arising from your use of this Website or your breach of the Terms of Service.<br />
	<br />
	</p>
<p>
	VARIATION</p>
<p>
	COTERIQUE for Software Development and E-commerce (Electronic Trade) shall have the right in its absolute discretion at any time and without notice to amend, remove or vary the Services and/or any page of this Website.<br />
	<br />
	</p>
<p>
	SEVERABILITY</p>
<p>
	If any part of the Terms of Service is unenforceable (including any provision in which we exclude our liability to you) the enforceability of any other part of the Terms of Service will not be affected, all other clauses remaining in full force and effect. So far as possible where any clause/sub-clause or part of a clause/sub-clause can be severed to render the remaining part valid, the clause shall be interpreted accordingly. Alternatively, you agree that the clause shall be rectified and interpreted in such a way that closely resembles the original meaning of the clause /sub-clause as is permitted by law.<br />
	<br />
	</p>
<p>
	COMPLAINTS</p>
<p>
	We operate a complaints handling procedure which we will use to try to resolve disputes when they first arise, please let us know if you have any complaints or comments.<br />
	<br />
	</p>
<p>
	WAIVER</p>
<p>
	If you breach these conditions and we take no action, we will still be entitled to use our rights and remedies in any other situation where you breach these conditions.&nbsp;<br />
	It is therefore understood that our lack of action in case of your breach of the present conditions, may not be construed in any case as a waiver of our right of action.<br />
	<br />
	</p>
<p>
	GOVERNING LAW AND JURISDICTION</p>
<p>
	These Terms and Conditions are to be construed in accordance with the laws of Egypt and in the event of any dispute or claim associated with these Terms and Conditions, that dispute or claim shall be subject to the exclusive jurisdiction of the Egyptian courts.<br />
	<br />
	</p>
<p>
	ENTIRE AGREEMENT</p>
<p>
	The above Terms of Service constitute the entire agreement of the parties and supersede any and all preceding and contemporaneous agreements between you and COTERIQUE for Software Development and E-commerce (Electronic Trade). Any waiver of any provision of the Terms of Service will be effective only if in writing and signed by a CEO of COTERIQUE for Software Development and E-commerce (Electronic Trade).</p>
<p>
	FORCE MAJEURE</p>
<p>
	If COTERIQUE for Software Development and E-commerce (Electronic Trade) is prevented or delayed (directly or indirectly) from making delivery of the Products or any portion of the Products on the agreed date of delivery or from otherwise performing this or any part of this agreement by reason of act(s) of God, war, embargo, riot(s), strike(s), lock-out(s), trade dispute(s), fire(s), break-down, inclement weather, interruption of transport, Government action, delay in delivery to COTERIQUE for Software Development and E-commerce (Electronic Trade) of any goods or materials or by any cause whatsoever (whether or not of like nature to those specified above) in each case outside such COTERIQUE for Software Development and E-commerce (Electronic Trade)'s control (force majeure), neither party hereto shall be under any liability whatsoever to the other party by reason of such event.</p>


</div>
<?php echo $footer; ?>