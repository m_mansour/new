array(24) {
["cart_id"]=>
string(2) "21"
["product_id"]=>
string(4) "1236"
["name"]=>
string(29) "Asymmetric Long Sleeve Jacket"
["model"]=>
string(15) "Nafsika Skourti"
["shipping"]=>
string(1) "1"
["image"]=>
string(25) "catalog/nafsika_top_3.jpg"
["option"]=>
array(1) {
[0]=>
array(15) {
["product_option_id"]=>
string(4) "1057"
["product_option_value_id"]=>
string(4) "1378"
["option_id"]=>
string(2) "11"
["option_value_id"]=>
string(2) "82"
["name"]=>
string(4) "Size"
["value"]=>
string(5) "IT 38"
["type"]=>
string(6) "select"
["quantity"]=>
string(1) "2"
["subtract"]=>
string(1) "1"
["price"]=>
string(6) "0.0000"
["price_prefix"]=>
string(1) "+"
["points"]=>
string(1) "0"
["points_prefix"]=>
string(1) "+"
["weight"]=>
string(10) "0.00000000"
["weight_prefix"]=>
string(1) "+"
}
}
["download"]=>
array(0) {
}
["quantity"]=>
string(1) "1"
["minimum"]=>
string(1) "1"
["subtract"]=>
string(1) "1"
["stock"]=>
bool(true)
["price"]=>
float(4902)
["total"]=>
float(4902)
["reward"]=>
int(0)
["points"]=>
int(0)
["tax_class_id"]=>
string(1) "0"
["weight"]=>
float(0)
["weight_class_id"]=>
string(1) "1"
["length"]=>
string(10) "0.00000000"
["width"]=>
string(10) "0.00000000"
["height"]=>
string(10) "0.00000000"
["length_class_id"]=>
string(1) "1"
["recurring"]=>
bool(false)
}
{"shipping_method":{"cotship":{"title":"Cotship","quote":{"cotship_5":{"code":"cotship.cotship_5","title":"In Egypt","cost":275,"tax_class_id":"0","text":"275EGP"}},"sort_order":"2","error":false}}}