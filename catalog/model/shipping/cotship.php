<?php

class ModelShippingCotship extends Model
{
    public function getQuote($address)
    {
        $this->load->language('shipping/weight');
        //echo "LOLOLOL";
        $quote_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "geo_zone where status = 1 ORDER BY name ");

        foreach ($query->rows as $result) {
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$result['geo_zone_id'] . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

            if ($query->num_rows) {
                $status = true;
            } else {
                $status = false;
            }


            if ($status) {
                //var_dump($result);

                //print_r($result['geo_zone_id']);
                //echo "SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$result['geo_zone_id'] . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')";
                //echo "SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$result['geo_zone_id'] . "' AND country_id = '" . (int)$address['country_id'] . "' AND zone_id = '" . (int)$address['zone_id'] . "'" ;
                //print_r($address);
                $cost = 0;
                $vendors_in_deal = array();
                foreach ($this->cart->getProducts() as $product) {
                    //var_dump($product);
                    //print_r($product);
                    //echo "Product from: " . $product['vendor_location'] . " to zone_id: " . $result['geo_zone_id'] . "<br />";
                    $shipment_text = "";
                    if (!in_array($product['vendor_id'], array_keys($vendors_in_deal))) {
                        $vendors_in_deal[strval($product['vendor_id'])] = array('location' => $product['vendor_location']);
                    }
                }


                //var_dump($vendors_in_deal);exit;
                foreach ($vendors_in_deal as $vendor_id => $vendor_data) {

                    $src = $vendor_data['location']; //location_id
                    $dest = $result['geo_zone_id'];// geo_zone_id
                    if ($src != 63 ) {
//                        $src = $result['geo_zone_id'];// geo_zone_id ===> location_id  // 64
                        $src = 63;
                        $queryDest = $this->db->query("SELECT geo_zone_id FROM " . DB_PREFIX . "zone_to_geo_zone WHERE country_id = ".$vendor_data['location']);
                        //$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "geo_zone where status = 1 ORDER BY name ")
                        $dest = $queryDest->row['geo_zone_id']; //location_id ==> get_zone
                    }

//
//                    var_dump($vendor_data['location']);
//                    var_dump($dest);



                    // from vendor egypt
                    if ((int)$src == 63) //domestic vendor
                    {

                        //Domestic shipping

                        if ((int)$dest == 23) {
                            //Zone 1 inside egypt
                            $cost += 2;
                        } elseif ((int)$dest == 24) {
                            //Zone 2 inside egypt
                            $cost += 2.5;
                        } elseif ((int)$dest == 25) {
                            //Zone 3 inside egypt
                            $cost += 3;
                        } elseif ((int)$dest == 26) {
                            //Zone 3 inside egypt
                            $cost += 3.5;
                        } elseif ((int)$dest == 27) {
                            //Zone 3 inside egypt
                            $cost += 5.5;
                        } elseif ((int)$dest == 28) {
                            //Zone 3 inside egypt
                            $cost += 8;
                        } //						}// zone 4 outside egypt
                        elseif ((int)$dest == 11) {
                            $cost += 38;
                        } elseif ((int)$dest == 12) {
                            $cost += 38;
                        } elseif ((int)$dest == 13) {
                            $cost += 39;
                        } elseif ((int)$dest == 14) {
                            $cost += 43;
                        } elseif ((int)$dest == 15) {
                            $cost += 53;
                        } elseif ((int)$dest == 16) {
                            $cost += 62;
                        } elseif ((int)$dest == 17) {
                            $cost += 56;
                        } elseif ((int)$dest == 18) {
                            $cost += 57;
                        } elseif ((int)$dest == 19) {
                            $cost += 65;
                        } elseif ((int)$dest == 20) {
                            $cost += 191;
                        } elseif ((int)$dest == 21) {
                            $cost += 42;
                        } elseif ((int)$dest == 22) {
                            $cost += 38;
                        } else {
                            $cost += 38;
                        }
                        // zone one
                    } else {
                        $cost += 38;
                    }
                }
                /*
                $weight = $this->cart->getWeight();

                $rates = explode(',', $this->config->get('weight_' . $result['geo_zone_id'] . '_rate'));

                foreach ($rates as $rate) {
                    $data = explode(':', $rate);

                    if ($data[0] >= $weight) {
                        if (isset($data[1])) {
                            $cost = $data[1];
                        }

                        break;
                    }
                }*/

                if ((string)$cost != '') {
                    $quote_data['cotship_' . $result['geo_zone_id']] = array(
                        'code' => 'cotship.cotship_' . $result['geo_zone_id'],
                        'title' => $result['name'],
                        'cost' => $cost,
                        'tax_class_id' => $this->config->get('weight_tax_class_id'),
                        'text' => $this->currency->format($this->tax->calculate($cost, $this->config->get('weight_tax_class_id'), $this->config->get('config_tax')))
                    );

                    //break;
                }
            }
        }
        //print_r($quote_data);
        $method_data = array();

        if ($quote_data) {
            $method_data = array(
                'code' => 'cotship',
                'title' => 'Cotship',
                'quote' => $quote_data,
                'sort_order' => $this->config->get('weight_sort_order'),
                'error' => false
            );
        }

        return $method_data;
    }
}

?>