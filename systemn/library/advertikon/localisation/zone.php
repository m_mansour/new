<?php
/**
 * Advertikon Extended Geo Zones Model
 *
 * @author Advertikon
 * @package Advertikon
 * @version
 */

namespace Advertikon\Localisation;

class Zone extends \ModelLocalisationZone{ 

	/**
	 * Get Geo Zones by ZoneID
	 *
	 * @param Integer $country_id Country ID
	 * @param Integer $zone_id Geo-zone ID
	 * @return Array
	 */
	public function getGeoZonesIdByZoneId( $country_id, $zone_id ) {

			$query = $this->db->query(
				"SELECT `geo_zone_id` FROM `" . DB_PREFIX . "zone_to_geo_zone` WHERE `country_id` = " .
					(int)$country_id . " AND ( `zone_id` = '" . (int)$zone_id . "' OR `zone_id` = 0 ) GROUP BY `geo_zone_id`" );

			$zone_data = array();
			foreach( $query->rows as $row ) {
				$zone_data[] = $row[ 'geo_zone_id' ];
			}

		return $zone_data;
	}
}
