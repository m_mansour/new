<?php echo $header; ?>
  <div id="content">
    <div class="container-fluid"><br />
      <br />
      <div class="row">
        <div class="col-sm-offset-4 col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h1 class="panel-title"><i class="fa fa-lock"></i> <?php echo $text_login; ?></h1>
            </div>
            <div class="panel-body">
              <?php if ($success) { ?>
                <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>
              <?php } ?>
              <?php if ($error_warning) { ?>
                <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>
              <?php } ?>
              <form action="?route=common/home/login" method="post" enctype="multipart/form-data">
                <div class="form-group">
                  <label for="input-username">User Name</label>
                  <div class="input-group"><span class="input-group-addon"><i class="fa fa-user"></i></span>
                    <input type="text" name="username" value="Username" placeholder="User Name" id="input-username" class="form-control" />
                  </div>
                </div>
                <div class="form-group">
                  <label for="input-password">Password</label>
                  <div class="input-group"><span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    <input type="password" name="password" value="password" placeholder="Password" id="input-password" class="form-control" />
                  </div>
               
                </div>
                <div class="text-right">
                  <button type="submit" class="btn btn-primary"><i class="fa fa-key"></i> <?php echo $button_login; ?></button>
                </div>
                <?php if ($redirect) { ?>
                  <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
                <?php } ?>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php echo $footer; ?>