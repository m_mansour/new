<?php echo $header; ?>
<div id="content">
	
	<?php if ($success) { ?>
	<div class="success"><?php echo $success; ?></div>
	<?php } ?>
	<div class="box">
		<div class="panel-heading">
			<h1>Vendor Products</h1>
			<div class="buttons">
<!--				<a onclick="$('#form').submit();" class="button">Save</a>-->
				<button type="submit" form="form" data-toggle="tooltip"  class="btn btn-primary"><i class="fa fa-save"></i></button>

<!--				<button type="submit" class="btn btn-primary"><i class="fa fa-key"></i>Save</button>-->
			</div>
		</div>
		<div class="table-responsive">
			<form action="index.php?route=common/home/modify_quantity" method="post" id="form">

				<table class="table table-bordered table-hover">
					<thead>
						<tr>

							<td class="center">Product Image</td>
							<td class="left">Product ID</td>
							<td class="left">Product Name</td>
							<td class="left">Product Option</td>
							<td class="left">Product Quantity</td>
							<td class="left">Product Model</td>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($main_products as $main_product) { ?>
						<tr>
							<td><img src="<?php echo $main_product['product_image']; ?>" width="150px" height="150px"/></td>
							<td><?php echo $main_product['product_id'] ;?> </td>
							<td><?php echo $main_product['product_name'];?> </td>
							<td><b>Main</b></td>
							<td><input type="text" value="<?php echo $main_product['product_quantity'];?>" name="quantity_main_<?php echo $main_product['product_id'];?>"/></td>
							<td><?php echo $main_product['product_model']; ?> </td>
						</tr>
						<?php foreach ($products[$main_product['product_id']] as $product) { ?>

						<tr>
							<td><img src="<?php echo $main_product['product_image']; ?>" width="150px" height="150px"/></td>
							<td><?php echo $product['product_id'] ;?> </td>
							<td><?php echo $product['product_name'];?> </td>
							<td><?php echo $product['product_option'];?></td>
							<td><input type="text" value="<?php echo $product['product_quantity'];?>" name="quantity_option<?php echo $product['product_option_value_id'];?>_<?php echo $product['product_id'];?>"/></td>
							<td><?php echo $product['product_model']; ?> </td>

							<?php }} ?>
						</tr>
					</tbody>
				</table>
			</form>


		</div>
	</div>
</div> 
<?php echo $footer; ?>