<?php
class ControllerCommonLogout extends Controller {
	public function index() {
        unset($this->session->data['vendor_id']);
        unset($this->session->data['vendor_name']);
        unset($this->session->data['vendor_logged_in']);
		unset($this->session->data['token']);

		$this->response->redirect($this->url->link('common/login', '', 'SSL'));
	}
}