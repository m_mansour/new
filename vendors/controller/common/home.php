<?php
class ControllerCommonHome extends Controller {  
	public function index(){
		
		//logged in
		$query = $this->db->query('select * from product where vendor_id = ' . $this->session->data['vendor_id'] );
		//echo "here";
		//$data['main_products'] = array();
		foreach($query->rows as $row){

			$sql = 'select * from product_description where product_id = ' . $row['product_id'];
			$query = $this->db->query($sql);
			//print_r($query);
			$product_name = $query->row['name'];
			$product_image ='../image/'.$row['image'];
			$product_id = $row['product_id'];
			$model = $row['model'];

			$data['main_products'][$product_id] = array(
				'product_id' => $product_id,
				'product_name' => $product_name,
				'product_image' => $product_image,
				'product_quantity' => $row['quantity'],
				'product_model' => $model
				);
			$sql = 'select * from product_option_value where product_id = ' . $row['product_id'];
			$query = $this->db->query($sql);
			foreach($query->rows as $option_row){
				$sql = 'select * from option_value_description where option_value_id = ' . $option_row['option_value_id'];
				$query = $this->db->query($sql);
				$data['products'][$product_id][] = array(
					'product_id' => $row['product_id'],
					'product_name' => $product_name,
					'product_quantity' => $option_row['quantity'],
					'product_option' => $query->row['name'],
					'product_option_id' => $option_row['option_value_id'],
					'product_option_value_id' => $option_row['product_option_value_id'],
					'product_image' => $product_image,
					'product_model' => $model
					);
				//echo $option_row['quantity'] . " " . $query->row['name'] . " " . $product_name;
			}
		}

		$this->session->data['updated_vendor_quantity'] = 0;
		//print_r($data);
		if($this->session->data['updated_vendor_quantity'] == 1){
			$data['success'] = 'Successfully updated quantities';
			$this->session->data['updated_vendor_quantity'] = 0;
		}
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['header'] = $this->load->controller('common/header');
		//$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('common/home.tpl', $data));


	} 
	public function login() {
		$this->document->setTitle("Vendor Login");
		if (($this->request->server['REQUEST_METHOD'] == 'POST'  ) ){
			$email = $this->db->escape( $this->request->post['username'] );
			$password_hash = md5($this->db->escape( $this->request->post['password']));
			$sql = "select vendor_id, vendor_name from vendors where vendor_email = '". $email ."' and vendor_password_hash = '" . $password_hash ."'";
			$query = $this->db->query($sql);
			$url = '';
			if($query->num_rows >0){
				$this->session->data['token'] = token(32);
				$this->session->data['vendor_logged_in'] = 1;
				$this->session->data['vendor_id'] = (int) $query->row['vendor_id'];
				$this->session->data['vendor_name'] = (int) $query->row['vendor_name'];
				$this->response->redirect($this->url->link('common/home', 'token=' . $this->session->data['token'] . $url, 'SSL'));

			}
		}



		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if ($this->config->get('config_password')) {
			$data['forgotten'] = $this->url->link('common/forgotten', '', 'SSL');
		} else {
			$data['forgotten'] = '';
		}

		$data['button_login'] = "Login";

		if (isset($this->request->get['route'])) {
			$route = $this->request->get['route'];

			unset($this->request->get['route']);
			unset($this->request->get['token']);

			$url = '';

			if ($this->request->get) {
				$url .= http_build_query($this->request->get);
			}

			$data['redirect'] = $this->url->link($route, $url, 'SSL');
		} else {
			$data['redirect'] = '';
		}


		$data['text_login']= "Vendor Login";

		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$this->response->setOutput($this->load->view('common/login.tpl', $data));
		

		
		
	}
	public function modify_quantity(){
		$url = '';
		if ( !($this->request->server['REQUEST_METHOD'] == 'POST'  ) ){
			$this->response->redirect($this->url->link('common/home', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		foreach($this->request->post as $post_name=>$post_value){
			if(preg_match('#quantity_main_(\d+)#', $post_name, $match)){
				$main_products[] = array(
					'product_id' => (int)$match[1],
					'product_quantity' => (int)$post_value
					);
			}elseif(preg_match('#quantity_option(\d+)_(\d+)#', $post_name, $match))
			{
				$products[] = array(
					'product_id' => (int)$match[2],
					'product_quantity' => (int)$post_value,
					'product_option_value_id' => (int)$match[1]
					);
			}

		}
		$this->session->data['vendor_data'] = array(
				'changed_main_products' => array()
			);

		foreach($products as $product){
			$sql = "update product_option_value set quantity = " . $product['product_quantity'] . " where product_option_value_id = " . $product['product_option_value_id'];
			$query = $this->db->query($sql);
		}
		foreach($main_products as $product){
			$sql = "update product set quantity = " . $product['product_quantity'] . " where product_id = " . $product['product_id'];
			$query = $this->db->query($sql);
			
		}
		/* Update data tab quantity = 0 if all options = 0 */
		$query = $this->db->query('select * from product where vendor_id = ' . $this->session->data['vendor_id'] );
		foreach($query->rows as $row){
			$product_id = $row['product_id'];
			$sql = 'select * from product_option_value where product_id = ' . $row['product_id'];
			$query = $this->db->query($sql);
			$all_options_zero = true;
			$has_options = false;
			foreach($query->rows as $option_row){
				$has_options = true;
				if($option_row['quantity'] > 0)
				{
					$all_options_zero = false;
				}
			}
		}
		if($all_options_zero and $has_options)
		{
				$sql = 'update product set quantity = 0 where product_id = ' . $product_id;
				$query = $this->db->query($sql);
				// $sql;
		}

		/* Done updating tab quantity */
		$this->session->data['updated_vendor_quantity'] = 1;
		$this->response->redirect($this->url->link('common/home', 'token=' . $this->session->data['token'] . $url, 'SSL'));

	}
}


?>
