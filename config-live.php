<?php
// HTTP
define('HTTP_SERVER', 'http://coterique.fslabs.net/');
define('HTTP_IMAGE', 'http://coterique.fslabs.net/image/');
define('HTTP_ADMIN', 'http://coterique.fslabs.net/admin/');

// HTTPS
define('HTTPS_SERVER', 'http://coterique.fslabs.net/');
define('HTTPS_IMAGE', 'http://coterique.fslabs.net/image/');

// DIR
define('DIR_APPLICATION', '/home/coteriq/public_html/catalog/');
define('DIR_SYSTEM', '/home/coteriq/public_html/system/');
define('DIR_DATABASE', '/home/coteriq/public_html/system/database/');
define('DIR_LANGUAGE', '/home/coteriq/public_html/catalog/language/');
define('DIR_TEMPLATE', '/home/coteriq/public_html/catalog/view/theme/');
define('DIR_CONFIG', '/home/coteriq/public_html/system/config/');
define('DIR_IMAGE', '/home/coteriq/public_html/image/');
define('DIR_CACHE', '/home/coteriq/public_html/system/storage/cache/');
define('DIR_MODIFICATION', '/home/coteriq/public_html/system/storage/modification/');
define('DIR_DOWNLOAD', '/home/coteriq/public_html/download/');
define('DB_PORT', '3306'); // OC 2.0.3 +
define('DIR_LOGS', '/home/coteriq/public_html/system/storage/logs/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'coteriq_mansour');
define('DB_PASSWORD', 'cot@#$');
define('DB_DATABASE', 'coteriq_cot');
define('DB_PREFIX', '');
?>
